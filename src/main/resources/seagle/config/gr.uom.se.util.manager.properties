##################### Definition of MANAGERS #################################
# Managers are instances that are resolved when seagle starts its deployment
# and they should start when a request to seagleManager.getManager(class)
# is issued. A manager is a SINGLE instance that is cached by seagle and
# is served to clients. Therefore manager instances should have no state
# or in case they have state should be thread safe.
#
# In order to register a manager to seagle you must specify its fully qualified
# name with a prefix of manager. The specified manager SHOULD BE a concrete class.
# DO NOT move managers from their packages (usually refactoring) without making
# the changes to this file, otherwise the code will break and SeagleManager
# singleton bean will not start at all.
# 
# Managers can be injected to modules using modules API mechanism as follows:
#
# interface MyModule {
# }
# class MyModuleImp implements MyModule {
#	FilePolicy filePolicyManager;
#	
#	@ProvideModule
#	public MyModule(
#			@Properties(name = NULLVal.NO_PROP) FilePolicy policy) {
#		this.filePolicyManager = policy;
#	}
#	...
# }
# 
# // Somewhere where in your code
# MyModule instance = seagleManager.getManager(ModuleManager.class)
#							.getLoader(MyModule.class).load(MyModule.class);
# 
# @Properties(name = NULLVal.NO_PROP) annotation is used to ensure that
# no property will be injected by mistake from module loader, or it will
# not be created if it can not be injected, because we need an instance
# of the manager that seagle contains.
#
# In order for this to work you must adhere to modules API rules,
# even you should annotate your interface with @Module( provider = MyModuleImp.class)
# or you should define at module domain properties,
# usually found in file (gr.uom.se.util.module.properties), the following
# 
# com.myclass.MyModule.provider.class = com.myclass.MyModuleImp
#
# For managers it is unnecessary to define a module provider (as the above example)
# because you already define a manager implementation in this place.
# 
# There are 2 default managers that are preloaded with SeagleManager,
# ModuleManager (provides functionality to load modules using reflection)
# and ConfigManager *provides functionality to access configurations).
# 
# IMPORTANT NOTE: do not define two managers that implements the same interface.
# In such cases when you request from seagle manager, a manager instance by
# providing that interface, it is undefined which manager the seagle should
# return. That means that if there are two or more managers that implements
# the same interface at least try to not request from seagle those manager
# instances using that interface. 
#
# Managers are instances that are not bound to Java EE properties (session beans
# etc).
#
# The following managers will be available from seagle manager using the
# known mechanism seagleManager.getManager(class)
# 
# Define FilePolicy manager
manager.gr.uom.java.seagle.v2.policy.FilePolicyImp = filePolicyManager
# Define ActivatorManager 
manager.gr.uom.se.util.manager.DefaultActivatorManager = activatorManager
# Define TaskSchedulerManager
manager.gr.uom.java.seagle.v2.scheduler.SeagleTaskSchedulerManager = seagleTaskScheduler
# Define RepositoryManager
manager.gr.uom.java.seagle.v2.project.DefaultRepositoryManager = repositoryManager
# Define ProjectDBManager
manager.gr.uom.java.seagle.v2.project.DefaultProjectDBManager = projectDbManager
# Define context manager (which manages context providers)
manager.gr.uom.se.util.context.DefaultContextManager = defaultContextManager
# Define repository provider factory manager
manager.gr.uom.java.seagle.v2.project.RepositoryProviderFactory = repositoryProviderFactory
# Define a repository info resolver factory
manager.gr.uom.java.seagle.v2.project.nameResolver.RepositoryInfoResolverFactory = repositoryInfoResolverFactory

##################### Definition of ACTIVATORS #################################
# Activators are one shot instances that perform a single job in a given a time.
# They are throw away instances, which mean when they are created they perform
# a job (usually activating some important parts of the application). When an
# activator is executed once, he can not be executed any more.
# Activator instances are created by ActivatorManager and are executed using
# modules API. To create an activator the implementing class must adhere to
# modules API rules.
# An activator should have a public method annotated with:
# gr.uom.se.util.manager.annotation.Init. 
# If this method takes any parameters they will be injected using modules API,
# thus the parameters should be annotated (not necessary required) with 
# @Property annotations.
#
# To activate (create the instance and execute the annotated method) an activator
# some point in time, use the following:
# 
# ActivatorManager manager = seagleManager.getManager(ActivatorManager.class);
# manager.activate(MyActivator.class);
#
# In order to tell sealge to activate some activators by default when it is being deployed
# you should define the activator at this file, doings so like the following definitions.
# 
# Define activator to register the default languages in DB (if they are not registered)
# and register the default metric categories.
activator.gr.uom.java.seagle.v2.metric.LanguageAndMetricCategoryActivator = programmingLanguageAndMetricCategoryActivator
# Define activator to register listeners to project actions such as ProjectEventType.CLONE
# which should listen to those events and perform task
activator.gr.uom.java.seagle.v2.project.ProjectActionAndListenerActivator = projectActionActivator
# Define activator to register the project analysis event dispatcher. This activator
# will register itself as a listener to events ANALYSIS_REQUESTED and will dispatch
# this event to registered metrics.
activator.gr.uom.java.seagle.v2.metric.ProjectAnalysisDispatcher = projectAnalysisDispatcherActivator
# Define activator to register listener for events of ANALYSIS_REQUESTED which will
# perform project evolution analysis (metric SourceCodeEvolutionAnalysis).
activator.gr.uom.java.seagle.v2.analysis.AnalysisActivator = projectAnalysisActivator
# Define activator to register repo metrics
activator.gr.uom.java.seagle.v2.metric.repo.RepoMetricActivator = repoMetricsActivator
#Define activator to register listener to listen after analysis completed event
activator.gr.uom.java.seagle.v2.metric.AnalysisCompletedActivator = analysisCompletedActivator


activator.gr.uom.java.seagle.v2.analysis.smellDetection.events.SmellDetectionActivator = smellDetectionActivator

activator.gr.uom.java.seagle.v2.analysis.dpd.DPDActivator = patternDetectionActivator

activator.gr.uom.java.seagle.v2.analysis.findbugs.FindBugsActivator = bugDetectionActivator

##################### Definition of Context Providers #################################
# Context providers are abstract instances that are useful when a context is required
# for a specific job. Usually managers, activators and other modules of seagle are used
# through out the entire application. That means that if a client needs for example
# needs a to obtain specific modules that their creation differs based on the circumstances
# he needs to obtain a specific context. The following snippet is used to get a context
# for a VCSRepository object (this context is provided by seagle).
# 
# ContextManager cm = seagleManager.getManager(ContextManager.class);
# ModuleContext context = cm.lookupContext(repository, ModuleContext.class);
# MyModuleDependingOnRepo module = context.load(MyModuleDependingOnRepo.class);
# 
# The above code is used to retrieve a module context for vcs repository. The module context
# can be used to load that specific module, and because that module has a dependency on repository
# when constructed the repository will be resolved.
# 
# Although the above example shows a module context usage, however the context is very abstract
# and can be used for other functions. That is, we used ModuleContext because it can be retrieved
# by module manager using mm.newDynamicContext(...). And a context provider for repository is
# already implemented.
# The following declaration is for VCSRepository module context implementation.
contextProvider.gr.uom.java.seagle.v2.project.RepositoryContextProvider = repositoryContextProvider
