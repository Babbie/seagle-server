/**
 * 
 */
package gr.uom.java.seagle.v2.ws.rest.project;

import gr.uom.java.seagle.v2.ws.rest.project.model.RESTBatchAnalysis;

import javax.ejb.Local;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;

/**
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
@Local
public interface AnalysisRequestor {

   /**
    * Given a project remote url and a list of versions return a response either
    * that the analysis has been scheduled or that the analysis has already 
    * performed in the past.
    * <p>
    * If a response code 404 (not found) is return it means the server is unable
    * to get an analysis for the given project, so it should be requested an
    * analysis for that project. In order to request an analysis a client must
    * request a project to be cloned first.
    * 
    * @param purl
    *           the remote url of the project. Must not be null.
    * @return  If the analysis has been performed in the past returns response
    * code 200. If the analysis has been successfully scheduled in the server 
    * but is not yet ready, returns response code 202 (Accepted).
    */
   Response requestAnalysis(String email, String purl);
   
   /**
    * Request a specific analysis for a large set of projects.
    * <p>
    * This service will run in silent mode, in that it will download any
    * project that is not in seagle, and will perform an analysis for the project.
    * 
    * @param batchAnalysis an object view of the client request, that should
    * be marshaled by the JAX-RS services.
    * 
    * @return a response to the client.
    */
   Response doBatchAnalysis(RESTBatchAnalysis batchAnalysis);
   
   //void doBatchAnalysis(AsyncResponse asyncResponse, RESTBatchAnalysis batchAnalysis);
}
