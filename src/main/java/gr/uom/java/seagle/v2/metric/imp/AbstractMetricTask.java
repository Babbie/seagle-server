/**
 *
 */
package gr.uom.java.seagle.v2.metric.imp;

import gr.uom.java.seagle.v2.AbstractSeagleResolver;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.db.persistence.Property;
import gr.uom.java.seagle.v2.db.persistence.controllers.PropertyFacade;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.metric.MetricRunEventType;
import gr.uom.java.seagle.v2.metric.MetricRunInfo;
import gr.uom.java.seagle.v2.metric.imp.AbstractMetricHandler.MetricSuffixProperty;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.java.seagle.v2.scheduler.SeagleTaskSchedulerManager;
import gr.uom.se.util.concurrent.Task;
import gr.uom.se.util.concurrent.TaskType;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.validation.ArgsCheck;
import gr.uom.se.vcs.VCSRepository;

import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An abstract metric task that supports the implementation of
 * {@link AbstractMetricHandler}.
 * <p>
 * In order to preserve consistent state each metric after it finishes its work
 * should send a metric {@linkplain MetricRunEventType#END end} event. Also a
 * metric should run using the {@link SeagleTaskSchedulerManager}. Because all
 * metrics should at list be compatible with these, this abstract implementation
 * simplify things in order to execute a metric (or a set of them). Subclass of
 * this implementation should only take care of making their computations by
 * implementing {@link #compute()} and at the end, if the computation was
 * successful to update the db by implementing {@link #updateDB()}. The updating
 * of db is taken care by this implementation so it supports JTA managed
 * transactions therefore the metrics doesn't need to support manual
 * transactions using user transaction objects.
 * 
 * @author Elvis Ligu
 */
public abstract class AbstractMetricTask extends AbstractSeagleResolver
      implements Task {

   /**
    * Type of task to be executed in task scheduler.
    * <p>
    * The type must be supported by task scheduler, otherwise it can not be
    * executed.
    */
   protected final TaskType type;
   /**
    * The info that was related with metric START event that was received.
    * <p>
    */
   protected final MetricRunInfo info;

   /**
    * The metrics that this task will execute.
    * <p>
    */
   protected final Set<ExecutableMetric> metrics;

   protected static final Logger logger = Logger
         .getLogger(AbstractMetricTask.class.getName());

   /**
    * Create an instance of this executor providing a seagle manager, the metric
    * run info for which the metric START event was received, the type of the
    * task and a list of metrics that this will support.
    * <p>
    * The metrics listed will be just for information purposes.
    */
   public AbstractMetricTask(SeagleManager seagleManager, MetricRunInfo info,
         TaskType type, Set<ExecutableMetric> metrics) {
      super(seagleManager);

      ArgsCheck.notNull("info", info);
      ArgsCheck.notNull("type", type);
      ArgsCheck.containsNoNull("metrics", metrics);
      ArgsCheck.notEmpty("metrics", metrics);

      this.info = info;
      this.type = type;
      this.metrics = metrics;

   }

   /**
    * Get a task type with the given string id.
    * <p>
    * This method will try to resolve the task scheduler from seagle manager and
    * will query a task type from the tasks supported from scheduler, with the
    * given id. If the task is null it will throw an exception. If not it will
    * return the task type.
    * 
    * @param seagleManager
    *           the seagle manager, must not be null.
    * @param id
    *           the task id, must not be null.
    * @return a task type for the given task id.
    */
   protected static TaskType getType(SeagleManager seagleManager, String id) {
      ArgsCheck.notNull("seagleManager", seagleManager);
      ArgsCheck.notNull("id", id);
      // Check the defaults first
      SeagleTaskSchedulerManager scheduler = seagleManager
            .getManager(SeagleTaskSchedulerManager.class);
      if (scheduler == null) {
         throw new RuntimeException(
               "can not resolve task scheduler from seagle manager");
      }
      TaskType type = scheduler.getType(id);
      // The task type should be present
      // otherwise we have a problem
      if (type == null) {
         throw new RuntimeException("Task type was not found " + id);
      }
      return type;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void run() {
      try {
         compute();
         finishWork();
      } catch (Exception ex) {
         logger.severe("ERROR During metric calculation. Metric END. ");
         logger.log(Level.SEVERE, "Stack trace: " , ex);
         
         sendMetricFinishedWithError();
         throw new RuntimeException(ex);
      }
   }

   /**
    * Subclasses must implement this method in order to make their computations.
    */
   protected abstract void compute();

   /**
    * Subclasses should implement this method in order to update the db after
    * they finishes their computations.
    * <p>
    * The execution of this method will support JTA transactions thus there is
    * no need for a subclass to manually support transactions.
    */
   protected abstract void updateDB();

   /**
    * After the computation a metric should finishes its work by updating
    * accordingly the db and sending a metric END event. This method will do
    * exactly that, however it will support DB transaction in that the operation
    * will be atomic, that is if something goes wrong all the changes will be
    * rolled back.
    */
   private void finishWork() {

      DBUpdater updater = resolveComponentOrManager(DBUpdater.class);
      updater.doWork(new Updater());
   }

   /**
    * This is a special runnable that will be used by this task in order to
    * unify the operations of updating db and notifying metric END in one single
    * atomic operation that supports JTA transactions. This runnable will be
    * executed by a STATELESS bean.
    * 
    * @author Elvis Ligu
    */
   private class Updater implements Runnable {

      @Override
      public void run() {
         // Update db code of implementation of this class
         updateDB();
         // We should now set the metrics as computed for the given project
         // And send an end event
         sendMetricFinished();
      }
   }

   /**
    * This will be called after the update DB has been performed in order to
    * send a metric END event and to write to DB a property that the abstract
    * handler needs to define when was the last time the metric was executed. If
    * one of the metrics of this task will throw an exception then it will be
    * considered that all metrics didn't finished successfully and all changes
    * to DB will roll back, and only metric END will be sent.
    */
   protected final void sendMetricFinished() {
      Iterator<ExecutableMetric> it = metrics.iterator();
      while (it.hasNext()) {
         ExecutableMetric metric = null;
         try {
            metric = it.next();
            metricFinished(metric, info);
            logger.log(Level.INFO, "END was sent from {0}",
                  metric.getMnemonic());
            it.remove();
         } catch (Exception ex) {
            logger.log(Level.SEVERE, "Can not send END for "
                  + (metric != null ? metric.getMnemonic() : ""), ex);
         }
      }
   }

   /**
    * This will be called from {@link #finishWork()} method if there is a
    * problem with computation of metrics or update of db.
    * <p>
    */
   protected final void sendMetricFinishedWithError() {
      for (ExecutableMetric metric : metrics) {
         try {
            sendMetricFinish(metric.getMnemonic(), info);
            logger.log(Level.INFO, "END was sent from {0}",
                  metric.getMnemonic());
         } catch (Exception ex) {
            logger.log(Level.SEVERE,
                  "Can not send END for " + metric.getMnemonic(), ex);
         }
      }
   }

   /**
    * @return get the repository of the project that is contained within metric
    *         start info.
    */
   protected final VCSRepository getRepo() {
      ProjectManager projects = resolveComponentOrManager(ProjectManager.class);
      String url = getProjectUrl();
      if (url == null) {
         throw new RuntimeException(
               "remote repo url is not contained within request");
      }
      VCSRepository repo = projects.getRepository(url);
      return repo;
   }

   /**
    * @return extract the project url from metric run info.
    */
   protected final String getProjectUrl() {
      // Schedule now the task to be run
      AnalysisRequestInfo request = getRequest(info);
      if (request == null) {
         throw new RuntimeException("analysis request info is null");
      }
      return request.getRemoteProjectUrl();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public TaskType getType() {
      return type;
   }

   /**
    * Send a metric {@linkplain MetricRunEventType#END end} event to inform the
    * metric event dispatcher that the metric has been finished its computation.
    * <p>
    * Note that each metric that receives a metric
    * {@linkplain MetricRunEventType#START start} event, should always send a
    * metric end event, otherwise the metric dispatcher will keep in memory
    * forever this metric event. The metric event should be sent even if the
    * metric should not be executed (but received the event from dispatcher).
    *
    * @param mnemonic
    *           the repo metric for which we should send the event
    * @param startInfo
    *           the info that initiated the metric
    */
   private final void sendMetricFinish(String mnemonic, MetricRunInfo startInfo) {
      EventManager events = resolveComponentOrManager(EventManager.class);
      Event endEvent = MetricRunInfo.end(mnemonic, startInfo);
      events.trigger(endEvent);
   }

   /**
    * This will set this metric as updated (calling
    * {@linkplain #setComputed(PropertyFacade, RepoMetric, String, Date) set
    * computed}) and will send a {@linkplain MetricRunEventType#END metric end}
    * event by calling {@linkplain #sendMetricFinish(String, MetricRunInfo)
    * metric finish}.
    *
    * @param metric
    *           the metric that has finished
    * @param info
    *           the info of the metric that started it
    */
   private final void metricFinished(ExecutableMetric metric, MetricRunInfo info) {
      try {
         PropertyFacade properties = resolveComponentOrManager(PropertyFacade.class);
         // Set the computed value of this metric
         setComputed(properties, metric, getRequest(info).getRemoteProjectUrl(), new Date());
      } finally {
         // Send the event that this metric has finished
         sendMetricFinish(metric.getMnemonic(), info);
      }
   }

   /**
    * Extract the analysis request info from metric request.
    * <p>
    *
    * @param info
    * @return the extracted analysis request info or null if there is not a
    *         request.
    */
   private final AnalysisRequestInfo getRequest(MetricRunInfo info) {
      Object req = info.request();
      if (req == null) {
         return null;
      }
      if (!(req instanceof AnalysisRequestInfo)) {
         throw new RuntimeException(
               "metric info doesn't contain an analysis request info");
      }
      return (AnalysisRequestInfo) info.request();
   }

   /**
    * Set the computed property for the given metric and the given project url.
    * <p>
    * This method will create a property under domain 'purl' with name
    * 'mnemonic.COMPUTED'.
    *
    * @param metric
    *           the metric to get the property for
    * @param purl
    *           the remote project url
    * @param date
    *           the date of computing, must not be null
    * @param properties
    *           the facade to read properties from db
    */
   private final void setComputed(PropertyFacade properties,
         ExecutableMetric metric, String purl, Date date) {
      String domain = purl;
      String name = MetricSuffixProperty.COMPUTED.getPropertyName(metric);
      String value = PropertyFacade.toString(date);
      Property property = properties.getPropertyByName(domain, name);
      if (property == null) {
         property = new Property();
         property.setDomain(domain);
         property.setName(name);
         property.setValue(value);
         properties.create(property);
      } else {
         property.setValue(value);
      }
   }
}
