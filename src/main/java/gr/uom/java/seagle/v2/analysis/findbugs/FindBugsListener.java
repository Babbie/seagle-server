package gr.uom.java.seagle.v2.analysis.findbugs;

import gr.uom.java.seagle.v2.AbstractSeagleListener;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisEventType;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventType;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel Feitosa
 */
public class FindBugsListener extends AbstractSeagleListener {

    private static final Logger logger = Logger.getLogger(FindBugsListener.class.getName());

    public FindBugsListener(SeagleManager seagleManager, EventManager eventManager) {
        super(seagleManager, eventManager);
    }

    @Override
    protected void register() {
        super.register();
    }

    @Override
    protected Set<EventType> getTypes() {
      Set<EventType> types = new HashSet<>();
      types.add(AnalysisEventType.ANALYSIS_COMPLETED);
      types.add(FindBugsEventType.FB_ANALYSIS_REQUESTED);
      types.add(FindBugsEventType.FB_ANALYSIS_ENDED);
      types.add(FindBugsEventType.FB_CLEAN_REQUESTED);
      types.add(FindBugsEventType.FB_CLEAN_ENDED);
      return types;
   }

    @Override
    protected void acceptEvent(EventType type, EventInfo info) {
        
        if (type.equals(FindBugsEventType.FB_ANALYSIS_REQUESTED) || 
                type.equals(AnalysisEventType.ANALYSIS_COMPLETED)) {
            AnalysisRequestInfo arinfo = (AnalysisRequestInfo) info.getDescription();
            Object[] i = new Object[]{arinfo.getEmail(), arinfo.getRemoteProjectUrl()};
            
            logger.log(Level.INFO, "Received FinBugs analysis request from {0}, for project {1}", i);
            FindBugsTask bugDetector = new FindBugsTask(seagleManager, arinfo);
            bugDetector.compute();
            
        } else if (type.equals(FindBugsEventType.FB_ANALYSIS_ENDED)) {
            logger.log(Level.INFO, "Completed FinBugs analysis request. {0}", info.getDescription());
            
        } else if (type.equals(FindBugsEventType.FB_CLEAN_REQUESTED)) {
            AnalysisRequestInfo arinfo = (AnalysisRequestInfo) info.getDescription();
            Object[] i = new Object[]{arinfo.getEmail(), arinfo.getRemoteProjectUrl()};
            
            logger.log(Level.INFO, "Received FinBugs clean request from {0}, for project {1}", i);
            FindBugsTask bugDetector = new FindBugsTask(seagleManager, arinfo);
            bugDetector.cleanFindBugsData();
            
        } else if (type.equals(FindBugsEventType.FB_CLEAN_ENDED)) {
            logger.log(Level.INFO, "Completed FinBugs clean request from. {0}", info.getDescription());
        }
    }

}
