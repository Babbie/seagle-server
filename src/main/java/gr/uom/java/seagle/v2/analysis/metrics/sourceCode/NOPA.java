package gr.uom.java.seagle.v2.analysis.metrics.sourceCode;

import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.java.seagle.v2.analysis.project.evolution.JavaProject;
import java.util.LinkedHashMap;
import java.util.ListIterator;
import java.util.Map;

/**
 *
 * @author Thodoris Chaikalis
 */
public class NOPA extends AbstractJavaExecutableMetric {

    public static final String MNEMONIC = "NOPA";

    public NOPA(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(SystemObject systemObject, JavaProject javaProject) {
        ListIterator<ClassObject> classIterator = systemObject.getClassListIterator();
        Map<String, Double> valuePerClass = new LinkedHashMap<>();
        while (classIterator.hasNext()) {
            ClassObject classObject = classIterator.next();
            classNOPA(valuePerClass,classObject);
        }
        storeValuesForAllNodesInMemory(getMnemonic(), valuePerClass, javaProject);
         storeProjectLevelAggregationMetricInMemory(getMnemonic(), javaProject, valuePerClass, MetricAggregationStrategy.Sum);
    }

   private void classNOPA(Map<String, Double> valuePerClass, ClassObject classObject) {
        valuePerClass.put(classObject.getName(), (double)classObject.getNumberOfPublicAttributes());
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "NOPA is the number of non-inherited attributes that belong to the interface of a class";
    }

    @Override
    public String getName() {
        return "Number of public attributes";
    }

}
