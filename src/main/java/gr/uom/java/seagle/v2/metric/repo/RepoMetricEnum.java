/**
 * 
 */
package gr.uom.java.seagle.v2.metric.repo;

import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;
import gr.uom.java.seagle.v2.metric.Category;
import gr.uom.java.seagle.v2.metric.Language;

/**
 * @author Elvis Ligu
 */
public enum RepoMetricEnum implements ExecutableMetric {

   /**
    * Number of commits for a project.
    */
   COM_PROJ("Number of Commits of a Project",
         "Count the number of commits for the entire project"),
   /**
    * Number of commits per version.
    */
   COM_COUNT_VER("Number of Commits per Version",
         "Count the number of commits from a previous version.\n"
               + "This will walk back the current version until it encounter\n"
               + "the previous version. Therefore for a given project it\n"
               + "is important which versions are required for analysis.\n"
               + "If intermediate versions are skipped this metric will\n"
               + "result in a greater number of commits per version."),

   /**
    * Number of authors per version.
    */
   AUTHOR_COUNT_VER("Number of Authors per Version",
         "Count the number of authors from a previous version."
               + "This will walk back the current version until it encounter\n"
               + "the previous version. Therefore for a given project it\n"
               + "is important which versions are required for analysis.\n"
               + "If intermediate versions are skipped this metric will\n"
               + "result in a greater number of authors per version."),

   /**
    * Authors per version.
    */
//   AUTHOR_VER("Authors per Version",
//         "Resolve the authors from a previous version."
//               + "This will walk back the current version until it encounter\n"
//               + "the previous version. Therefore for a given project it\n"
//               + "is important which versions are required for analysis.\n"
//               + "If intermediate versions are skipped this metric will\n"
//               + "result in a different set of authors per version."),
   /**
    * Number of committers per version.
    */
   COMMITER_COUNT_VER("Number of Committers per Version",
         "Count the number of committers from a previous version."
               + "This will walk back the current version until it encounter\n"
               + "the previous version. Therefore for a given project it\n"
               + "is important which versions are required for analysis.\n"
               + "If intermediate versions are skipped this metric will\n"
               + "result in a greater number of committers per version."),

   /**
    * Committers per version.
    */
//   COMMITTER_VER("Committers per Version",
//         "Resolve the committers from a previous version."
//               + "This will walk back the current version until it encounter\n"
//               + "the previous version. Therefore for a given project it\n"
//               + "is important which versions are required for analysis.\n"
//               + "If intermediate versions are skipped this metric will\n"
//               + "result in a different set of committers per version."),

   /**
    * Added files per version.
    */
//   ADD_FILE_VER("Added Files per Version",
//         "Resolve the added files from a previous version."
//               + "This will walk back the current version until it encounter\n"
//               + "the previous version. Therefore for a given project it\n"
//               + "is important which versions are required for analysis.\n"
//               + "If intermediate versions are skipped this metric will\n"
//               + "result in a different set of added files per version."),

   /**
    * Number of added files per version
    */
   ADD_FILE_COUNT_VER("Added Files per Version",
         "Resolve the added files from a previous version."
               + "This will walk back the current version until it encounter\n"
               + "the previous version. Therefore for a given project it\n"
               + "is important which versions are required for analysis.\n"
               + "If intermediate versions are skipped this metric will\n"
               + "result in a different number of added files per version."),
   /**
    * Deleted files per version.
    */
//   DEL_FILE_VER("Deleted Files per Version",
//         "Resolve the deleted files from a previous version."
//               + "This will walk back the current version until it encounter\n"
//               + "the previous version. Therefore for a given project it\n"
//               + "is important which versions are required for analysis.\n"
//               + "If intermediate versions are skipped this metric will\n"
//               + "result in a different set of deleted files per version."),
   /**
    * Number of deleted files per version.
    */
   DEL_FILE_COUNT_VER("Deleted Files per Version",
         "Resolve the deleted files from a previous version."
               + "This will walk back the current version until it encounter\n"
               + "the previous version. Therefore for a given project it\n"
               + "is important which versions are required for analysis.\n"
               + "If intermediate versions are skipped this metric will\n"
               + "result in a different number of deleted files per version."),
   /**
    * Modified files per version.
    */
//   MOD_FILE_VER("Added Files per Version",
//         "Resolve the modified files from a previous version."
//               + "This will walk back the current version until it encounter\n"
//               + "the previous version. Therefore for a given project it\n"
//               + "is important which versions are required for analysis.\n"
//               + "If intermediate versions are skipped this metric will\n"
//               + "result in a different set of modified files per version."),
   /**
    * Number of modified files per version.
    */
   MOD_FILE_COUNT_VER("Modified Files per Version",
         "Resolve the modified files from a previous version."
               + "This will walk back the current version until it encounter\n"
               + "the previous version. Therefore for a given project it\n"
               + "is important which versions are required for analysis.\n"
               + "If intermediate versions are skipped this metric will\n"
               + "result in a different number of modified files per version."),
   /**
    * Number of added lines per version.
    */
   ADD_LINE_COUNT_VER("Added Lines per Version",
         "Resolve the number of added lines from a previous version."
               + "This will walk back the current version until it encounter\n"
               + "the previous version. Therefore for a given project it\n"
               + "is important which versions are required for analysis.\n"
               + "If intermediate versions are skipped this metric will\n"
               + "result in a different numbers of added lines per version."),
   /**
    * Number of deleted lines per version.
    */
   DEL_LINE_COUNT_VER("Deleted Lines per Version",
         "Resolve the number of deleted lines from a previous version."
               + "This will walk back the current version until it encounter\n"
               + "the previous version. Therefore for a given project it\n"
               + "is important which versions are required for analysis.\n"
               + "If intermediate versions are skipped this metric will\n"
               + "result in a different numbers of deleted lines per version."),
   /**
    * Number of java files that are within a test directory that are
    * added/modified per commit of a version.
    */
//   JAVA_TEST_FILE_ADD_MOD_COUNT_VER(
//         "Added/Modified test files per Version",
//         "Resolve the number of added/modified test files from a previous version."
//               + "This will walk back the current version until it encounter\n"
//               + "the previous version. Therefore for a given project it\n"
//               + "is important which versions are required for analysis.\n"
//               + "If intermediate versions are skipped this metric will\n"
//               + "result in a different numbers of files per version.\n"
//               + "This metric will check pairs of commits (a current with\n"
//               + "its previous) starting from a previous version until\n"
//               + "current version is reached. For each commit pair it will\n"
//               + "collect the differences of files added or modified that\n"
//               + "have in their path a /test/ directory and have a .java\n"
//               + "suffix. If such a file is added/modified it will increase the\n"
//               + "counter.", Language.UNSPECIFIED, Language.JAVA),
   /**
    * Number of commits that add/modify a java test file and in the same commit
    * and add/modify a java file within a test directory.
    */
//   JAVA_FILE_ADD_TEST_COUNT_VER(
//         "Added Java Sources Accompanied with Tests",
//         "Resolve the number of Java source files that are added in a commit\n"
//               + "along with test files. This will walk back the current\n"
//               + "version until it encounter the previous version. Therefore\n"
//               + "for a given project it is important which versions are required\n"
//               + "for analysis. If intermediate versions are skipped this metric\n"
//               + "will result in a different numbers of files per version.\n"
//               + "This metric will check pairs of commits (a current with\n"
//               + "its previous) starting from a previous version until\n"
//               + "current version is reached. For each commit pair it will\n"
//               + "collect the differences of files added that\n"
//               + "have a .java\n suffix. When such addition is encountered\n"
//               + "will check if an addition and/or modification is made to a test file.\n"
//               + "(a .java file within a /test/ directory). There is no such heuristic to\n"
//               + "find out if the test modified was related to the new file, however it is\n"
//               + "indicative to when a source file is added is there a corresponding test for\n"
//               + "this new functionaility. If such a file is encountered it will increase the\n"
//               + "counter.", Language.UNSPECIFIED, Language.JAVA),
   /**
    * Number of commits that add/modify a java test file and in the same commit
    * and do not add/modify a java file within a test directory.
    */
//   JAVA_FILE_ADD_NO_TEST_COUNT_VER(
//         "Added Java Sources not Accompanied with Tests",
//         "Resolve the number of Java source files that are added in a commit\n"
//               + "but not tests are added or modified. This will walk back the current\n"
//               + "version until it encounter the previous version. Therefore\n"
//               + "for a given project it is important which versions are required\n"
//               + "for analysis. If intermediate versions are skipped this metric\n"
//               + "will result in a different numbers of files per version.\n"
//               + "This metric will check pairs of commits (a current with\n"
//               + "its previous) starting from a previous version until\n"
//               + "current version is reached. For each commit pair it will\n"
//               + "collect the differences of files added that\n"
//               + "have a .java\n suffix. When such addition is encountered\n"
//               + "will check if an addition and/or modification is not made to a test file.\n"
//               + "(a .java file within a /test/ directory). There is no such heuristic to\n"
//               + "find out if the test modified was related to the new file, however it is\n"
//               + "indicative to when a source file is added is there a corresponding test for\n"
//               + "this new functionaility. If such a file is encountered it will increase the\n"
//               + "counter. This metric will generally show the number of commits that do add a\n"
//               + "a source file without adding or modifying a test file (new untested functionaility).",
//         Language.UNSPECIFIED, Language.JAVA)
               ;

   private RepoMetricEnum(String name, String desc, String... langs) {
      this.name = name;
      this.desc = desc;
      this.langs = (langs == null || langs.length == 0 ? new String[] { Language.UNSPECIFIED }
            : langs);
   }

   private final String desc;
   private final String name;
   private final String[] langs;

   @Override
   public String getDescription() {
      return desc;
   }

   @Override
   public String getName() {
      return name;
   }

   @Override
   public String getMnemonic() {
      return name();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getCategory() {
      return Category.REPO_METRICS;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String[] getLanguages() {
      return langs;
   }

   /**
    * Given a suffix get a string that has a prefix 'mnemonic'.suffix.
    * <p>
    * 
    * @param suffix
    *           to be appended to property name
    * @return a prefixed with metric mnemonic string
    */
   public String getPrefixedPropertyName(String suffix) {
      return this.name() + "." + suffix;
   }
}
