package gr.uom.java.seagle.v2.analysis.project.evolution;

import gr.uom.se.vcs.VCSFile;
import gr.uom.se.vcs.VCSResource;
import gr.uom.se.vcs.walker.ResourceVisitor;
import gr.uom.se.vcs.walker.filter.resource.ResourceFilterUtility;
import gr.uom.se.vcs.walker.filter.resource.VCSResourceFilter;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author Theodore Chaikalis
 */
public class JavaFileVisitor implements ResourceVisitor<VCSResource> {

    private final JavaProject javaProject;

    public JavaFileVisitor(JavaProject javaProject) {
        this.javaProject = javaProject;
    }

    @Override
    public VCSResourceFilter<VCSResource> getFilter() {
        return ResourceFilterUtility.suffix(".java");
    }

    @Override
    public boolean includeDirs() {
        return false;
    }

    @Override
    public boolean includeFiles() {
        return true;
    }

    @Override
    public boolean visit(VCSResource t) {
        if (t instanceof VCSFile) {
            Path filePath = Paths.get(javaProject.getSourceFolder(), t.getPath());
            javaProject.addJavaFilePath(filePath.toString());
        }
        return true;
    }

}
