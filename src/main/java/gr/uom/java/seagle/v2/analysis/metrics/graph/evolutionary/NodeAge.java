package gr.uom.java.seagle.v2.analysis.metrics.graph.evolutionary;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.graph.AbstractEdge;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.SoftwareGraph;
import gr.uom.java.seagle.v2.analysis.graph.evolution.GraphEvolution;
import gr.uom.java.seagle.v2.analysis.metrics.graph.AbstractGraphExecutableMetric;
import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.Version;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Theodore Chaikalis
 */
public class NodeAge extends AbstractGraphEvolutionaryMetric {

    public static final String MNEMONIC = "NODE_AGE";

    public NodeAge(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(GraphEvolution ge) {
        Map<Version, Map<AbstractNode, Double>> metricValues = new LinkedHashMap<>();
        Version[] versionList = ge.getVersionArray();
        for (int i = 1; i < versionList.length; i++) {
            Version currentVersion = versionList[i];
            Version previousVersion = versionList[i - 1];
            SoftwareGraph<AbstractNode, AbstractEdge> currentGraph = ge.getGraphForAVersion(currentVersion);
            SoftwareGraph<AbstractNode, AbstractEdge> previousGraph = ge.getGraphForAVersion(previousVersion);
            
            Map<AbstractNode, Double> valuePerClass = new LinkedHashMap<>();
            for (AbstractNode node : currentGraph.getVertices()) {
                try {
                    AbstractNode prevNode = previousGraph.getNode(node.getName());
                    if (prevNode != null) {
                        node.increaseAge(prevNode.getAge());
                        valuePerClass.put(node, (double)node.getAge());
                    }
                } catch (NullPointerException e) {
                    Logger.getLogger("UpdateNodeAges").log(Level.SEVERE, "Previous graph does not exist. Please check. Current version = " + currentGraph.getVersion(), e);
                }
            }
            metricValues.put(currentVersion, valuePerClass);
        }
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Age of each node";
    }

    @Override
    public String getName() {
        return "Node Age";
    }

}
