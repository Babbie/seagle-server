package gr.uom.java.seagle.v2.analysis.project.evolution;

import gr.uom.java.seagle.v2.analysis.metrics.SourceCodeEvolutionAnalysisMetricTask;
import gr.uom.java.ast.SystemObject;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.graph.GraphFactory;
import gr.uom.java.seagle.v2.analysis.graph.javaGraph.JavaGraphFactory;
import gr.uom.java.seagle.v2.analysis.graph.SoftwareGraph;
import gr.uom.java.seagle.v2.analysis.graph.javaGraph.JavaDemeterLawEdgeCreationStrategy;
import gr.uom.java.seagle.v2.analysis.graph.javaGraph.ASTReader;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.java.seagle.v2.analysis.graph.AbstractEdge;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.evolution.GraphEvolution;
import gr.uom.java.seagle.v2.analysis.metrics.AMetricsCollection;
import gr.uom.java.seagle.v2.analysis.metrics.graph.AbstractGraphExecutableMetric;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.AbstractJavaExecutableMetric;
import gr.uom.java.seagle.v2.db.persistence.Node;
import gr.uom.java.seagle.v2.db.persistence.controllers.NodeFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.VersionFacade;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;
import static gr.uom.java.seagle.v2.metric.imp.MetricEventListener.logger;
import gr.uom.java.seagle.v2.utilities.SeagleDataStructures;
import gr.uom.se.util.context.ContextManager;
import gr.uom.se.util.module.ModuleContext;
import gr.uom.se.util.validation.ArgsCheck;
import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.analysis.version.provider.ConnectedVersionProvider;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;

/**
 *
 * @author Theodore Chaikalis & Elvis Ligu
 */
public class SourceCodeEvolutionAnalysis {

    public static final String mnemonic = "SourceCodeEvolutionAnalysis";

    private ProjectManager projectManager;
    private SeagleManager seagleManager;
    private NodeFacade nodeFacade;
    private SourceCodeEvolutionAnalysisMetricTask grEvAnal;
    private Set<ExecutableMetric> metrics;

    @EJB
    ExecutorService executor;

    public SourceCodeEvolutionAnalysis(SeagleManager seagleManager, NodeFacade nodeFacade, Set<ExecutableMetric> metrics) {
        ArgsCheck.notNull("seagleManager", seagleManager);
        ArgsCheck.notNull("nodeFacade", nodeFacade);

        this.nodeFacade = nodeFacade;
        this.projectManager = seagleManager.resolveComponent(ProjectManager.class);
        this.seagleManager = seagleManager;
        this.metrics = metrics;
    }

    public List<JavaProject> run(Project project) {
        checkoutVersions(project.getRemoteRepoPath());
        List<JavaProject> javaProjects = createJavaProjectForEachVersion(project);
        project.setSoftwareProjects(javaProjects);
        List<SoftwareGraph> graphs = createGraphs(javaProjects);
        return javaProjects;
    }

    private void checkoutVersions(String repoPath) {
        ContextManager cm = seagleManager.getManager(ContextManager.class);
        try (VCSRepository repo = projectManager.getRepository(repoPath)) {
            ModuleContext context = cm.lookupContext(repo, ModuleContext.class);
            ConnectedVersionProvider versionProvider = context.load(ConnectedVersionProvider.class);
            Iterator<VCSCommit> versionIterator = versionProvider.iterator();
            while (versionIterator.hasNext()) {
                VCSCommit commitForAVersion = versionIterator.next();
                projectManager.checkout(repoPath, commitForAVersion.getID());
            }
        }
    }

    public List<JavaProject> createJavaProjectForEachVersion(Project project) {
        List<JavaProject> javaProjects = new ArrayList<>();
        try (VCSRepository repo = projectManager.getRepository(project.getRemoteRepoPath());) {
            for (Version version : project.getVersions()) {
                String checkoutFolder = projectManager.getCheckoutFolder(project.getRemoteRepoPath(), version.getCommitID());
                VCSCommit vcsCommit = repo.resolveCommit(version.getCommitID());
                JavaProject javaProject = new JavaProject(version, project.getName(), checkoutFolder, vcsCommit);
                javaProjects.add(javaProject);
            }
        } catch (Exception ex) {
            Logger.getLogger(SourceCodeEvolutionAnalysis.class.getName()).log(Level.SEVERE, null, ex);
        }
        return javaProjects;
    }

    private void calculateSourceCodeMetrics(final SystemObject systemObject, final JavaProject javaProject) throws InterruptedException, ExecutionException {
        for (final ExecutableMetric m : metrics) {
            if (m instanceof AbstractJavaExecutableMetric) {
                AbstractJavaExecutableMetric javaMetric = (AbstractJavaExecutableMetric) m;
                logger.log(Level.INFO, " | calculating Source Code Metric: {0} | Version : {1}", new Object[]{javaMetric.getMnemonic(), javaProject.getProjectVersion().getName()});
                javaMetric.calculate(systemObject, javaProject);
            }
        }
    }
    
    private void calculateGraphMetrics(SoftwareProject softwareProject) {
        logger.info("Graph metrics calculation started");
        for (final ExecutableMetric m : metrics) {
            if (m instanceof AbstractGraphExecutableMetric) {
                AbstractGraphExecutableMetric gem = (AbstractGraphExecutableMetric) m;
                logger.log(Level.INFO, "-------calculating metric: {0}", gem.getMnemonic());
                gem.calculate(softwareProject);
            }
        }
        logger.info("Graph metrics calculation ENDED. ");
    }

    private List<SoftwareGraph> createGraphs(List<JavaProject> javaProjects) {
        List<SoftwareGraph> createdGraphs = new ArrayList<>();
        for (JavaProject softwareProject : javaProjects) {
            try {
                SystemObject systemObject = ASTReader.parseASTAndCreateSystemObject(softwareProject);
                SoftwareGraph<AbstractNode, AbstractEdge> graph = createJavaGraph(systemObject, softwareProject);
                persistGraphNodes(graph);
                softwareProject.setGraph(graph);
                calculateSourceCodeMetrics(systemObject, softwareProject);
                calculateGraphMetrics(softwareProject);
                createdGraphs.add(graph);
                //  javaProject.persist();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
        return createdGraphs;
    }

    private SoftwareGraph<AbstractNode, AbstractEdge> createJavaGraph(SystemObject systemObject, JavaProject javaProject) throws Exception {
        GraphFactory graphFactory = new JavaGraphFactory(systemObject, javaProject);
        SoftwareGraph<AbstractNode, AbstractEdge> systemGraph = graphFactory.getSystemGraph(new JavaDemeterLawEdgeCreationStrategy());
        if (systemGraph == null) {
            throw new RuntimeException("Graph for version " + javaProject.getProjectVersion() + " is null");
        }
        systemGraph.setVersion(javaProject.getProjectVersion());
        return systemGraph;
    }

    private void persistGraphNodes(SoftwareGraph<AbstractNode, AbstractEdge> graph) {
        List<Node> persistedGraphNodes = nodeFacade.findByVersion(graph.getVersion());
        Map<String, Node> nodeMap = SeagleDataStructures.nodeListToTreeMap(persistedGraphNodes);
        VersionFacade versionFacade = seagleManager.resolveComponent(VersionFacade.class);
        Version version = versionFacade.findById(graph.getVersion().getId());
        for (AbstractNode node : graph.getVertices()) {
            Node dbNode = null;
            if (!nodeMap.containsKey(node.getName())) {
                dbNode = new Node();
                String name = node.getName();
                dbNode.setName(name);
                dbNode.setSimpleName(name.substring(name.lastIndexOf(".") + 1, name.length()));
                dbNode.setVersion(version);
                dbNode.setSourceFilePath(node.getSourceFilePath());
                node.setDbNode(dbNode);
                nodeFacade.create(dbNode);
                versionFacade.edit(version);
            }
        }
    }

}
