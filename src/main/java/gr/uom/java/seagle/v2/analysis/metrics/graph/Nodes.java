package gr.uom.java.seagle.v2.analysis.metrics.graph;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.graph.AbstractEdge;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.SoftwareGraph;
import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;

/**
 *
 * @author Theodore Chaikalis
 */
public class Nodes extends AbstractGraphExecutableMetric {

    public static final String MNEMONIC = "NODES";

    public Nodes(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(SoftwareProject softwareProject) {
         SoftwareGraph<AbstractNode, AbstractEdge> graph = softwareProject.getProjectGraph();
        int nodesCount = graph.getVertexCount();
        softwareProject.putProjectLevelMetric(getMnemonic(), nodesCount);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Number of nodes for a version of the graph";
    }

    @Override
    public String getName() {
        return "Number of Nodes";
    }

}
