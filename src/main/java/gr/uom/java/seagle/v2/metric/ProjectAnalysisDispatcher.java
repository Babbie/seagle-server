package gr.uom.java.seagle.v2.metric;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisEventType;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectFacade;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.se.util.event.DefaultEvent;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventListener;
import gr.uom.se.util.event.EventType;
import gr.uom.se.util.manager.annotations.Activator;
import gr.uom.se.util.manager.annotations.Init;
import gr.uom.se.util.module.annotations.NULLVal;
import gr.uom.se.util.module.annotations.Property;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.util.validation.ArgsCheck;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author elvis
 */
@Activator(
        dependencies = LanguageAndMetricCategoryActivator.class
)
public class ProjectAnalysisDispatcher implements EventListener {
    
   private static final Logger logger = Logger.getLogger(ProjectAnalysisDispatcher.class.getName());

   private final int MAX_SIZE = 10000;

   private final RequestQueue requests = new RequestQueue(MAX_SIZE, false);

   private final EventManager eventManager;
   private final SeagleManager seagleManager;

   @ProvideModule
   public ProjectAnalysisDispatcher(
           @Property(name = NULLVal.NO_PROP) EventManager eventManager,
           @Property(name = NULLVal.NO_PROP) SeagleManager seagleManager) {

      ArgsCheck.notNull("eventManager", eventManager);
      ArgsCheck.notNull("seagleManager", seagleManager);

      this.eventManager = eventManager;
      this.seagleManager = seagleManager;
   }

   @Init
   public void init() {
      eventManager.addListener(AnalysisEventType.ANALYSIS_REQUESTED, this);
      eventManager.addListener(MetricRunEventType.END, this);
   }

   @Override
   public void respondToEvent(Event event) {
      EventType type = event.getType();

      // If analysis request we start a new job for the analysis
      // by triggering an event for the metrics to run 
      if (AnalysisEventType.ANALYSIS_REQUESTED.equals(type)) {

         AnalysisRequestInfo request
                 = (AnalysisRequestInfo) event.getInfo().getDescription();
         scheduleRequest(request);

      } else if (MetricRunEventType.END.equals(type)) {
         // This is an event from a metric
         MetricRunInfo info
                 = (MetricRunInfo) event.getInfo().getDescription();
         gatherResult(info);
      }
   }

   private void scheduleRequest(AnalysisRequestInfo info) {
      // 1 - Look into the db all the registered metrics
      //     for the given project
      Project project = getProject(info);
      if (project == null) {
         // abort here because some other request
         // may have deleted the project
         logger.log(Level.SEVERE,"Cannot schedule Analysis, Project is not in the DataBase");
         return;
      }

      // Do nothing if there are no registered metrics to
      // run for the given project
      Collection<Metric> metrics = project.getRegisteredMetrics();
      if (metrics.isEmpty()) {
         fireEvent(info);
      }

      // Now collect all metric mnemonics
      String[] mnemonics = new String[metrics.size()];
      int i = 0;
      for (Metric m : metrics) {
         mnemonics[i++] = m.getMnemonic();
      }

      // Create the metric event
      Event event = requests.schedule(info, mnemonics);
      // If event is null that means there is already a request for this
      // project that hasn't finished yet, so we do not need to send this
      // event at this time, because it will be queued and sent after all other
      // requests are finished.
      try {
         if (event != null) {
            eventManager.trigger(event);
         }
      } catch (Exception e) {
      // Remove in case somthing goes wrong so we do not
         // have bad requests
         MetricRunInfo minfo = (MetricRunInfo)event.getInfo().getDescription();
         requests.scheduleNext(minfo.id());
         throw new RuntimeException(e);
      }
   }

   private Project getProject(AnalysisRequestInfo info) {
      ProjectFacade facade = getProjectFacade();
      List<Project> list = facade.findByUrl(info.getRemoteProjectUrl());
      if (list.isEmpty()) {
         return null;
      }
      Project project = list.get(0);
      project = facade.refresh(project, project.getId());
      return project;
   }

   private ProjectFacade getProjectFacade() {
      ProjectFacade facade = seagleManager.resolveComponent(ProjectFacade.class);
      if (facade == null) {
         throw new RuntimeException(
                 "project facade is not resolvable from seagle manager");
      }
      return facade;
   }

   /**
    * This method will gather all metric info that are send by metrics.
    * <p>
    * For each metric info we find the request id and the metric that finished
    * the job. We remove the metric with the given mnemonic from the list of
    * mnemonics with the given request id. When the list is empty that means all
    * the metrics have finished so we must trigger an event for the finished
    * analysis request.
    *
    * @param info
    */
   private void gatherResult(MetricRunInfo info) {
      // At this stage only one mnemonic is in the list
      // of info

      String mnemonic = info.mnemonics().iterator().next();
      // Get the request's list and remove the metric from this
      // list
      RequestQueue.Pair entry = requests.get(info.id());
      // We should check for entry to be null
      // in case we have previously returned from
      // a bad request
      if (entry == null) {
         return;
      }
      entry.mnemonics.remove(mnemonic);
      if (entry.mnemonics.isEmpty()) {
         // Remove the request from the map
         // it is considered done
         RequestQueue.NextEvent nextEvent = requests.scheduleNext(info.id());
         // The results are gathered so we must fire an event
         fireEvent(entry.info);
         // If we have a previous request for the same project
         // we fire an event to start the metrics
         if (nextEvent != null && nextEvent.event != null) {
            this.respondToEvent(nextEvent.event);
         }
      }
   }

   /**
    * Fire an ANALYSIS_COMPLETED event based on initial request
    * ANALYSIS_REQUESTED.
    *
    * @param info
    */
   private void fireEvent(AnalysisRequestInfo info) {
      EventInfo eInfo = new EventInfo(info, new Date());
      eventManager.trigger(
              new DefaultEvent(AnalysisEventType.ANALYSIS_COMPLETED, eInfo));
   }
}
