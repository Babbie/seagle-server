/**
 * 
 */
package gr.uom.java.seagle.v2.ws.rest;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

/**
 * @author Elvis Ligu
 */
@Provider
public class CORSResponseFilter implements ContainerResponseFilter{

   /**
    * {@inheritDoc}
    */
   @Override
   public void filter(ContainerRequestContext requestContext,
         ContainerResponseContext responseContext) throws IOException {
      
      // Get the headers from response
      MultivaluedMap<String, Object> headers = responseContext.getHeaders();
      // Make the REST public to all domains
      headers.add("Access-Control-Allow-Origin", "*");
      headers.add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");        
      headers.add("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
      if(!headers.containsKey("Content-Encoding")) {
         headers.add("Content-Encoding", "utf-8");
      }
      
   }

}
