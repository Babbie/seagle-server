/**
 *
 */
package gr.uom.java.seagle.v2.metric.annotation;

import gr.uom.java.seagle.v2.metric.Category;
import gr.uom.java.seagle.v2.metric.Language;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Metric {

   String mnemonic();

   String name();

   String description() default "";

   String category() default Category.UNSPECIFIED;

   String[] langs() default Language.UNSPECIFIED;
   
   String[] dependencies() default {};
}
