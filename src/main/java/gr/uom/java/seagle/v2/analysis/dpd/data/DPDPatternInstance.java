/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.analysis.dpd.data;

import java.util.ArrayList;

/**
 *
 * @author Samet
 */
public class DPDPatternInstance 
{
    private ArrayList<DPDPatternInstanceRole> instanceRoles;

    public DPDPatternInstance() 
    {
        this.instanceRoles = new ArrayList<DPDPatternInstanceRole>();
    }
    
    /**
     * Gets the instance roles list for the pattern instance.
     * @return Instance roles list for the pattern instance
     */
    public ArrayList<DPDPatternInstanceRole> getInstanceRoleList()
    {
        return this.instanceRoles;
    }
}
