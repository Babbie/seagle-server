/**
 *
 */
package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.se.util.module.ModuleConstants;
import gr.uom.se.util.module.ModuleContext;
import gr.uom.se.util.module.ModuleLoader;
import gr.uom.se.util.module.ModuleManager;
import gr.uom.se.util.module.ParameterProvider;
import gr.uom.se.util.module.PropertyInjector;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.util.property.DomainPropertyHandler;
import gr.uom.se.util.property.PropertyUtils;
import gr.uom.se.util.validation.ArgsCheck;
import gr.uom.se.vcs.VCSRepository;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The default implementation of project context.
 * <p>
 * This implementation is more of a wrapper for a module context which can
 * inject repository and project instances to.
 *
 * @author Elvis Ligu
 */
public class DefaultProjectContext implements ProjectContext {

    private final VCSRepository repo;
    private final Project project;
    private final ModuleContext context;
    private final DomainPropertyHandler propertyHandler;

    /**
     *
     */
    public DefaultProjectContext(SeagleManager seagleManager, Project project,
            VCSRepository repo) {

        ArgsCheck.notNull("seagleManager", seagleManager);
        ArgsCheck.notNull("project", project);
        ArgsCheck.notNull("repository", repo);

        ModuleManager mm = seagleManager.getManager(ModuleManager.class);
      // Create handler in order to specify new properties that are valid
        // for this context only
        this.propertyHandler = PropertyUtils.newHandler();
        this.context = mm.newDynamicContext(VCSRepository.class, propertyHandler);
        this.project = project;
        this.repo = repo;

        // Set this class as the provider of repo and project
        context.setProvider(this, VCSRepository.class);
        context.setProvider(this, Project.class);
    }

    public ModuleLoader getLoader(Class<?> type) {
        return context.getLoader(type);
    }

    public void setLoader(ModuleLoader loader, Class<?> type) {
        context.setLoader(loader, type);
    }

    @Override
    public Class<? extends ModuleLoader> getLoaderClass(Class<?> type) {
        return context.getLoaderClass(type);
    }

    @Override
    public void setLoaderClass(String loaderClass, Class<?> type) {
        context.setLoaderClass(loaderClass, type);
    }

    @Override
    public void setLoaderClass(Class<? extends ModuleLoader> loaderClass,
            Class<?> type) {
        context.setLoaderClass(loaderClass, type);
    }

    @Override
    public Object getProvider(Class<?> type) {
        return context.getProvider(type);
    }

    @Override
    public void setProvider(Object provider, Class<?> type) {
        context.setProvider(provider, type);
    }

    @Override
    public Class<?> getProviderClass(Class<?> type) {
        return context.getProviderClass(type);
    }

    @Override
    public void setProviderClass(String providerClass, Class<?> type) {
        context.setProviderClass(providerClass, type);
    }

    public Class<?> getType() {
        return context.getType();
    }

    public void setProviderClass(Class<?> providerClass, Class<?> type) {
        context.setProviderClass(providerClass, type);
    }

    public ParameterProvider getParameterProvider(Class<?> type) {
        return context.getParameterProvider(type);
    }

    public void setParameterProvider(ParameterProvider provider, Class<?> type) {
        context.setParameterProvider(provider, type);
    }

    public Class<? extends ParameterProvider> getParameterProviderClass(
            Class<?> type) {
        return context.getParameterProviderClass(type);
    }

    public void setParameterProviderClass(String providerClass, Class<?> type) {
        context.setParameterProviderClass(providerClass, type);
    }

    public void setParamterProviderClass(
            Class<? extends ParameterProvider> providerClass, Class<?> type) {
        context.setParamterProviderClass(providerClass, type);
    }

    public PropertyInjector getPropertyInjector(Class<?> type) {
        return context.getPropertyInjector(type);
    }

    public void setPropertyInjector(PropertyInjector provider, Class<?> type) {
        context.setPropertyInjector(provider, type);
    }

    public Class<? extends PropertyInjector> getPropertyInjectorClass(
            Class<?> type) {
        return context.getPropertyInjectorClass(type);
    }

    public void setPropertyInjectorClass(String injectorClass, Class<?> type) {
        context.setPropertyInjectorClass(injectorClass, type);
    }

    public void setPropertyInjectorClass(
            Class<? extends ParameterProvider> injectorClass, Class<?> type) {
        context.setPropertyInjectorClass(injectorClass, type);
    }

    public <T> T load(Class<T> type) {
        return context.load(type);
    }

    public void inject(Object bean) {
        context.inject(bean);
    }

    public <T> T getParameter(Class<T> parameterType, Annotation[] annotations,
            Map<String, Map<String, Object>> properties) {
        return context.getParameter(parameterType, annotations, properties);
    }

    private boolean closed = false;

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() throws IOException {
        if (!closed) {
            repo.close();
        }
    }

    /**
     * {@inheritDoc}
     */
    // Used by modules api to retrieve the repo
    @ProvideModule
    @Override
    public VCSRepository getRepository() {
        return repo;
    }

    /**
     * {@inheritDoc}
     */
    // Used by modules api to retrieve the project
    @ProvideModule
    @Override
    public Project getProject() {
        return project;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getProperty(String domain, String name) {
        return propertyHandler.getProperty(domain, name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T getProperty(String domain, String name, Class<T> type) {
        return propertyHandler.getProperty(domain, name, type);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setProperty(String domain, String name, Object value) {
        propertyHandler.setProperty(domain, name, value);
    }

    private String getDefaultDomain() {
        return ModuleConstants.DEFAULT_PROPERTY_ANNOTATION_DOMAIN;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setProperty(String name, Object value) {
        this.setProperty(getDefaultDomain(), name, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getProperty(String name) {
        return this.getProperty(getDefaultDomain(), name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T getProperty(String name, Class<T> type) {
        return this.getProperty(getDefaultDomain(), name, type);
    }

}
