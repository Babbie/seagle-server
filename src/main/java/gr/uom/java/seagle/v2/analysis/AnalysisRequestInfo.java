
package gr.uom.java.seagle.v2.analysis;


import java.util.Objects;

/**
 *
 * @author Elvis Ligu & Theodore Chaikalis
 */
public class AnalysisRequestInfo {
    
    private final String email;
    private final String purl;

    public AnalysisRequestInfo(String email, String purl) {
        this.email = email;
        this.purl = purl;
    }

    public String getEmail() {
        return email;
    }

    public String getRemoteProjectUrl() {
        return purl;
    }


   @Override
   public int hashCode() {
      int hash = 5;
      hash = 83 * hash + Objects.hashCode(this.purl);
      return hash;
   }

   @Override
   public boolean equals(Object obj) {
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      final AnalysisRequestInfo other = (AnalysisRequestInfo) obj;
      if (!Objects.equals(this.purl, other.purl)) {
         return false;
      }
      return true;
   }
    
    
}
