package gr.uom.java.seagle.v2.analysis.dpd;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.db.persistence.Node;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.controllers.NodeFacade;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Task for executing the Design Pattern Detection (DPD) tool for project.
 *
 * @author Daniel Feitosa
 */
public class DPDTask {

    private final EventManager eventManager;
    private final Project project;

    // Verify the necessity
    //private final DPDFacade dpdFacade;
    private final NodeFacade nodeFacade;

    private static final Logger logger = Logger.getLogger(DPDTask.class.getName());

    private final Collection<Node> nodesToUpdate;

    public DPDTask(SeagleManager seagleManager, AnalysisRequestInfo ainfo) {

        // Is this the bast way to handle the managers?
        // Other managers use the ArgsCheck
        ///this.badSmellFacade = seagleManager.resolveComponent(BadSmellFacade.class);
        this.nodeFacade = seagleManager.resolveComponent(NodeFacade.class);
        this.eventManager = seagleManager.resolveComponent(EventManager.class);
        ProjectManager projectManager = seagleManager.resolveComponent(ProjectManager.class);

        // What do I need to create here?
        //SmellManager.createSmells(badSmellFacade, seagleManager);
        this.project = projectManager.findByUrl(ainfo.getRemoteProjectUrl());

        // Why using the synchronizedCollection?
        this.nodesToUpdate = Collections.synchronizedCollection(new HashSet<Node>());
    }

    public void compute() {
        logger.log(Level.INFO, "Starting detection of patterns for project {0}", project.getName());
        //TODO:2016-08-29:Execute DPD for each version

        //TODO:2016-08-29:Create history of pattern instance

        //TODO:2016-08-29:Save results to database

        trigger(DPDEventType.DPD_ANALYSIS_ENDED);
        logger.info("Detection of patterns completed for "+project.getName());
    }

    public void cleanDPDData() {
        logger.log(Level.INFO, "Starting detection of patterns for project {0}", project.getName());
        //TODO:2016-08-29:Delete DPD output xml for each version

        //TODO:2016-08-29:Delete Delete all Db data for the project

        //TODO:2016-08-29:Save results to database

        trigger(DPDEventType.DPD_CLEAN_REQUESTED);
        logger.info("Cleaning of DPD data completed for "+project.getName());
    }
    
    protected void updateDB() {
        // What is necessary to update DB?
        nodeFacade.edit(new ArrayList<Node>(nodesToUpdate));
    }

    private void trigger(DPDEventType evn) {
        EventInfo eInfo = new EventInfo(project, new Date());
        Event dpdENDED = evn.newEvent(eInfo);
        eventManager.trigger(dpdENDED);
    }

}
