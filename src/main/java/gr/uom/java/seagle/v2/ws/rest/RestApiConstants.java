package gr.uom.java.seagle.v2.ws.rest;
public class RestApiConstants {
	
	public static final int GENERIC_APP_ERROR_CODE = 5001;		
	public static final String SEAGLE_UOM_URL = "http://java.uom.gr/seagle";

}