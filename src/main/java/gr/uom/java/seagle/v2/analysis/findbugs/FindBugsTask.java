package gr.uom.java.seagle.v2.analysis.findbugs;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.db.persistence.Node;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.java.seagle.v2.db.persistence.controllers.NodeFacade;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Task for executing the Design Pattern Detection (DPD) tool for project.
 *
 * @author Daniel Feitosa
 */
public class FindBugsTask {

    private final EventManager eventManager;
    private final Project project;
    private final ProjectManager projectManager;

    // Verify the necessity
    //private final FBFacade dpdFacade;
    private final NodeFacade nodeFacade;

    private static final Logger logger = Logger.getLogger(FindBugsTask.class.getName());

    private final Collection<Node> nodesToUpdate;

    public FindBugsTask(SeagleManager seagleManager, AnalysisRequestInfo ainfo) {

        // Is this the bast way to handle the managers?
        // Other managers use the ArgsCheck
        ///this.badSmellFacade = seagleManager.resolveComponent(BadSmellFacade.class);
        this.nodeFacade = seagleManager.resolveComponent(NodeFacade.class);
        this.eventManager = seagleManager.resolveComponent(EventManager.class);
        this.projectManager = seagleManager.resolveComponent(ProjectManager.class);

        // What do I need to create here?
        //SmellManager.createSmells(badSmellFacade, seagleManager);
        this.project = projectManager.findByUrl(ainfo.getRemoteProjectUrl());

        // Why using the synchronizedCollection?
        this.nodesToUpdate = Collections.synchronizedCollection(new HashSet<Node>());
    }

    public void compute() {
        logger.log(Level.INFO, "Starting detection of bugs for project {0}", project.getName());

        //TODO:2016-08-29:Execute Findbugs for each version
        for (Version version : project.getVersions()) {
            projectManager.buildProjectVersion(project.getRemoteRepoPath(), version.getCommitID(), true);
        }

        //TODO:2016-08-29:Create history of bugs using findbugs

        //TODO:2016-08-29:Save results to database

        trigger(FindBugsEventType.FB_ANALYSIS_ENDED);
        logger.log(Level.INFO, "Detection of bugs completed for {0}", project.getName());
    }

    public void cleanFindBugsData() {
        logger.log(Level.INFO, "Starting cleaning of FindBugs data for project {0}", project.getName());
        //TODO:2016-08-29:Delete FindBugs output xml for each version

        //TODO:2016-08-29:Delete all DB data for the project

        //TODO:2016-08-29:Save results to database

        trigger(FindBugsEventType.FB_CLEAN_REQUESTED);
        logger.log(Level.INFO, "Cleaning of FindBugs data completed for {0}", project.getName());
    }
    
    protected void updateDB() {
        // What is necessary to update DB?
        nodeFacade.edit(new ArrayList<Node>(nodesToUpdate));
    }

    private void trigger(FindBugsEventType evn) {
        EventInfo eInfo = new EventInfo(project, new Date());
        Event dpdENDED = evn.newEvent(eInfo);
        eventManager.trigger(dpdENDED);
    }

}
