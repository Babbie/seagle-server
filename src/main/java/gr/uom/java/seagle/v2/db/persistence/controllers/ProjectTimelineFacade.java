package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.ProjectTimeline;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author elvis
 */
@Stateless
public class ProjectTimelineFacade extends AbstractFacade<ProjectTimeline> {

   /**
    * Injected entity manager for the persistence context named {@code seaglePU}
    * .
    * <p>
    */
   @PersistenceContext(unitName = "seaglePU")
   private EntityManager em;

   public ProjectTimelineFacade() {
      super(ProjectTimeline.class);
   }

   @Override
   protected EntityManager getEntityManager() {
      return em;
   }

   public List<ProjectTimeline> findByProjectURLAndType(String url, String actionType) {
      final String QUERY = "ProjectTimeline.findByProjectURLAndType";
      return JPAUtils.namedQueryEntityTwoParam(em, ProjectTimeline.class,
            QUERY, "url", url, "mnemonic", actionType);
   }
   
   public List<ProjectTimeline> findByProjectNameAndType(String name, String actionType) {
      final String QUERY = "ProjectTimeline.findByProjectNameAndType";
      return JPAUtils.namedQueryEntityTwoParam(em, ProjectTimeline.class,
            QUERY, "name", name, "mnemonic", actionType);
   }
   
   public List<ProjectTimeline> findByProjectURL(String url) {
      final String QUERY = "ProjectTimeline.findByProjectURL";
      return JPAUtils.namedQueryEntityOneParam(em, ProjectTimeline.class,
            QUERY, "url", url);
   }
   
   public List<ProjectTimeline> findByProjectName(String name) {
      final String QUERY = "ProjectTimeline.findByProjectName";
      return JPAUtils.namedQueryEntityOneParam(em, ProjectTimeline.class,
            QUERY, "name", name);
   }
   
   public List<ProjectTimeline> findByType(String type) {
      final String query = "ProjectTimeline.findByType";
      return JPAUtils.namedQueryEntityOneParam(em, ProjectTimeline.class,
            query, "mnemonic", type);
   }
   
   public List<ProjectTimeline> findByDate(Date date) {
      final String query = "ProjectTimeline.findByTimestamp";
      return JPAUtils.namedQueryEntityOneParam(em, ProjectTimeline.class,
            query, "timestamp", date);
   }
   
   public List<ProjectTimeline> findBefore(Date date) {
      final String query = "ProjectTimeline.findCreatedBefore";
      return JPAUtils.namedQueryEntityOneParam(em, ProjectTimeline.class,
            query, "timestamp", date);
   }
   
   public List<ProjectTimeline> findAfter(Date date) {
      final String query = "ProjectTimeline.findCreatedAfter";
      return JPAUtils.namedQueryEntityOneParam(em, ProjectTimeline.class,
            query, "timestamp", date);
   }
   
   public List<ProjectTimeline> findWithin(Date start, Date end) {
      final String query = "ProjectTimeline.findCreatedWithin";
      return JPAUtils.namedQueryEntityTwoParam(em, ProjectTimeline.class,
            query, "start", start, "end", end);
   }
   
   public ProjectTimeline findLast(String type) {
      final String query = "ProjectTimeline.findLastByType";
      List<ProjectTimeline> list =  em.createNamedQuery(
            query, ProjectTimeline.class).setParameter("mnemonic", type).setMaxResults(1)
            .getResultList();
      if(list.isEmpty()) {
         return null;
      }
      return list.get(0);
   }
   
   public ProjectTimeline findLastByProjectUrl(String url, String type) {
      final String query = "ProjectTimeline.findLastByProjectURLAndType";
      List<ProjectTimeline> list =  em.createNamedQuery(
            query, ProjectTimeline.class).
            setParameter("url", url).
            setParameter("mnemonic", type).setMaxResults(1)
            .getResultList();
      if(list.isEmpty()) {
         return null;
      }
      return list.get(0);
   }
   
   public ProjectTimeline findLastByProjectName(String name, String type) {
      final String query = "ProjectTimeline.findLastByProjectNameAndType";
      List<ProjectTimeline> list =  em.createNamedQuery(
            query, ProjectTimeline.class).
            setParameter("name", name).
            setParameter("mnemonic", type).setMaxResults(1)
            .getResultList();
      if(list.isEmpty()) {
         return null;
      }
      return list.get(0);
   }
}
