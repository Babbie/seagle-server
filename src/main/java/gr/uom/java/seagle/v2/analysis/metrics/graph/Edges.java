package gr.uom.java.seagle.v2.analysis.metrics.graph;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.graph.AbstractEdge;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.SoftwareGraph;
import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;

/**
 *
 * @author Theodore Chaikalis
 */
public class Edges extends AbstractGraphExecutableMetric {

    public static final String MNEMONIC = "EDGES";

    public Edges(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(SoftwareProject softwareProject) {
        SoftwareGraph<AbstractNode, AbstractEdge> graph = softwareProject.getProjectGraph();
        int edgesCount = graph.getEdgeCount();
        softwareProject.putProjectLevelMetric(getMnemonic(), edgesCount);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Number of Edges for a version of the graph";
    }

    @Override
    public String getName() {
        return "Number of Edges";
    }

}
