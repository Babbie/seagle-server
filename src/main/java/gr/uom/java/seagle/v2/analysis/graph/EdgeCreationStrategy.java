
package gr.uom.java.seagle.v2.analysis.graph;

import gr.uom.java.ast.SystemObject;
import gr.uom.java.seagle.v2.analysis.graph.SoftwareGraph;

/**
 *
 * @author Theodore Chaikalis
 */
public interface EdgeCreationStrategy {

    public abstract void createEdges(SoftwareGraph softwareGraph, SystemObject system);
    
}
