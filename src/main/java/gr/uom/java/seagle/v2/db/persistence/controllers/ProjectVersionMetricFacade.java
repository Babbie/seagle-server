/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.MetricValue;
import gr.uom.java.seagle.v2.db.persistence.ProjectVersionMetric;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.se.util.validation.ArgsCheck;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Thodoris
 */
@Stateless
public class ProjectVersionMetricFacade extends
        AbstractFacade<ProjectVersionMetric> {

    @PersistenceContext(unitName = "seaglePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProjectVersionMetricFacade() {
        super(ProjectVersionMetric.class);
    }

    public List<ProjectVersionMetric> findByVersionAndMetric(Version version,
            Metric metric) {
        ArgsCheck.notNull("version", version);
        ArgsCheck.notNull("metric", metric);
        String query = "ProjectVersionMetric.findByVersionAndMetric";
        return JPAUtils.namedQueryEntityTwoParam(em, ProjectVersionMetric.class,
                query, "metric", metric, "version", version);
    }

    /**
     * Find all project version metrics for the given version.
     * <p>
     *
     * @param version the version of the metric
     * @return a list of all project version metrics for the given version.
     */
    public List<ProjectVersionMetric> findByVersion(Version version) {
        ArgsCheck.notNull("version", version);

        String query = "ProjectVersionMetric.findByVersion";
        return JPAUtils.namedQueryEntityOneParam(em, ProjectVersionMetric.class,
                query, "version", version);
    }

    public void upsertMetric(Version version, Metric metric, double value) {

        ProjectVersionMetric vmetric = getVersionMetric(version, metric);
         // If metric is null that means this is the first time
        // this processor is executed for this version
        if (vmetric == null) {
            // Create value
            MetricValue mvalue = new MetricValue();
            mvalue.setValue(value);
            mvalue.setMetric(metric);
            
            // Create relation
            vmetric = new ProjectVersionMetric();
            vmetric.setMetricValue(mvalue);
            vmetric.setVersion(version);
            mvalue.setProjectVersionMetric(vmetric);
            this.create(vmetric);
            // Edit version
            version.addProjectVersionMetric(vmetric);
        } else {
            // update value
            MetricValue mvalue = vmetric.getMetricValue();
            mvalue.setValue(value);
            em.merge(value);
        }
    }

    private ProjectVersionMetric getVersionMetric(Version version, Metric metric) {
        List<ProjectVersionMetric> list = this.findByVersionAndMetric(version,
                metric);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }
}
