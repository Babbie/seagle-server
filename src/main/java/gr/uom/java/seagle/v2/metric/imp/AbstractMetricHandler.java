/**
 *
 */
package gr.uom.java.seagle.v2.metric.imp;

import gr.uom.java.seagle.v2.AbstractSeagleResolver;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.db.persistence.ProjectTimeline;
import gr.uom.java.seagle.v2.db.persistence.controllers.PropertyFacade;
import gr.uom.java.seagle.v2.metric.MetricRunEventType;
import gr.uom.java.seagle.v2.metric.MetricRunInfo;
import gr.uom.java.seagle.v2.project.DBProjectActionType;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.se.util.validation.ArgsCheck;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * An abstract metric handler that provides support for metric executions.
 * <p>
 * 
 * When a listener receives a {@linkplain MetricRunEventType#START start} event
 * he is responsible to execute all metrics that he supports. Because there are
 * a lot of metrics and new metrics are unknown to the listener the listener
 * should consult the {@link MetricFactory} in order to get a metric handler. A
 * metric handler will provide a listener info about what kind of metrics it
 * support, as well as for a given set of metrics a metric task (a
 * {@link Runnable}). Also the listener may consult the metric handler for the
 * metrics that needs updates (always for the metrics the handler supports). For
 * all those metrics that the handler says there is no need to run again because
 * they are already up to date, the listener should return a metric
 * {@linkplain MetricRunEventType#END end} event to inform the scheduler. On the
 * other hand all metrics that will be executed should send the same event after
 * their execution, even if the execution is not successful.
 * <p>
 * Each listener should call
 * {@linkplain #getMetricsToCompute(MetricRunInfo, List)
 * getMetricsToCompute(...)} in order to specify which metrics needs to be
 * updated. The default implementation of this method will throw an exception if
 * a metric provided to this method is unknown to this handler. It will return
 * all metrics if the project has never been analyzed before, or it will return
 * each metric that has never been computed before, or the last time it was
 * computed is after the last time the project was updated. On the other hand,
 * for all those metrics that this method can not decide for sure if they should
 * update (execute) it will call
 * {@linkplain #computeMetrics(MetricRunInfo, List) computeMetrics(...)}.
 * <p>
 * This implementation works best with {@link AbstractMetricTask} subtypes of
 * metric runners because that task knows how to deal with metric handler and
 * will try to preserve consistency for the metric state.
 * <p>
 * After the listener consults which metrics should execute it should call
 * {@linkplain #getRunner(List, MetricRunInfo)} in order to get a runnable to be
 * executed. And the runnable returned shall be abstract metric task executor.
 *
 * @author Elvis Ligu
 */
public abstract class AbstractMetricHandler extends AbstractSeagleResolver {

   /**
    * Create an instance based on the given dependencies.
    * <p>
    * Generally speaking all metrics should have a dependency on seagle manager
    * because they need to read and write fom db and or from project manager.
    *
    * @param seagleManager
    *           must not be null
    */
   public AbstractMetricHandler(SeagleManager seagleManager) {
      super(seagleManager);
   }

   /**
    * Return all metrics that should be computed for the given request.
    * <p>
    * This method will check if the project has been analyzed in the past, by
    * looking into db for an event of
    * {@link DBProjectActionType#PROJECT_ANALYSED}. If no such event was found
    * it means the project was not analyzed so it will return true. If an event
    * is present that means we probably have an update request so it will check
    * the metric if it should be updated. If the metric should be updated it
    * will return true.
    *
    * @param info
    *           the event info that was received from a
    *           {@linkplain MetricRunEventType#START start} event. The info must
    *           contain a request of type {@link AnalysisRequestInfo}.
    * @param metrics
    *           the metric to check for computation
    *
    * @return return all metrics that should be computed
    */
   public List<ExecutableMetric> getMetricsToCompute(MetricRunInfo info,
         List<ExecutableMetric> metrics) {
      checkMetric(info, metrics);
      if (!isProjectAnalyzed(info)) {
         return metrics;
      }
      // We should compute accordingly to this method
      // Generally speaking if metric has never been computed
      // this will return true, or if the last time the metric
      // was computed is after the last time the project was
      // updated. If no one of the above is true that means
      // we should consult the handler itself by calling
      // computeMetrics() method.
      return computeMetricCheckDefaults(info, metrics);
   }

   /**
    * Precaution check, which throws an exception if the metric requested is not
    * contained within info object.
    * <p>
    *
    * @param info
    *           the start info
    * @param metrics
    *           the metrics to check
    */
   protected void checkMetric(MetricRunInfo info, List<ExecutableMetric> metrics) {

      // Check if the info contains this mnemonic
      for (ExecutableMetric metric : metrics) {
         if (!info.contains(metric.getMnemonic())) {
            throw new IllegalArgumentException(
                  "Metrinc run info object doesn't contain metric " + metric);
         }
      }
   }

   /**
    * Return a metric runner for the given metrics and given request.
    * <p>
    * Clients should first check if the metric should be analyzed otherwise this
    * method may return null.
    *
    * @param metric
    * @param info
    * @return
    */
   public abstract Runnable getRunner(List<ExecutableMetric> metric,
         MetricRunInfo info);

   /**
    * Returns true if the project that has the same url as the url provided by
    * request object has been analyzed in the past.
    *
    * This will check if there is an event registered to db for the project, of
    * type {@link DBProjectActionType#PROJECT_ANALYSED}
    *
    * @param info
    *           the event info that was received from a
    *           {@linkplain MetricRunEventType#START start} event. The info must
    *           contain a request of type {@link AnalysisRequestInfo}.
    * @return true if the project was analyzed in the past.
    */
   protected boolean isProjectAnalyzed(MetricRunInfo info) {
      // Get the request info to extract project url
      AnalysisRequestInfo request = getRequest(info);
      String url = request.getRemoteProjectUrl();
      // Get the project manager to check project timelines
      ProjectTimeline timeline = getLastTimeline(url,
            DBProjectActionType.PROJECT_ANALYSED.name());
      return timeline != null;
   }

   /**
    * Return the last timeline of the project with the given url, and of the
    * given type.
    * <p>
    *
    * @param purl
    * @param actionType
    * @return
    */
   private ProjectTimeline getLastTimeline(String purl, String actionType) {
      // Get the project manager to check project timelines
      ProjectManager projectManager = resolveComponentOrManager(ProjectManager.class);
      // Get the last event when the project was analyzed
      ProjectTimeline timeline = projectManager.getLastTimelineByProjectURL(
            purl, actionType);
      return timeline;
   }

   /**
    * Extract the analysis request info from metric request.
    * <p>
    *
    * @param info
    * @return the extracted analysis request info or null if there is not a
    *         request.
    */
   public static AnalysisRequestInfo getRequest(MetricRunInfo info) {
      Object req = info.request();
      if (req == null) {
         return null;
      }
      if (!(req instanceof AnalysisRequestInfo)) {
         throw new RuntimeException(
               "metric info doesn't contain an analysis request info");
      }
      return (AnalysisRequestInfo) info.request();
   }

   public static enum MetricSuffixProperty {

      COMPUTED;

      public String getPropertyName(ExecutableMetric metric) {
         return metric.getMnemonic() + "." + this.name();
      }
      
      public String getPropertyName(String mnemonic) {
         return mnemonic + "." + this.name();
      }
   }

   /**
    * Get the last computed date of the given property for the given project
    * url.
    * <p>
    * This method should return a date only if a property is present under
    * domain 'purl' with name 'mnemonic.COMPUTED' and that string property can
    * be converted to date.
    *
    * @param properties
    *           the property facade that can query properties
    * @param metric
    *           the metric to get the property for
    * @param purl
    *           the remote project url
    * @return the last computed date if this property was previously computed
    */
   protected final Date getLastComputed(PropertyFacade properties,
         ExecutableMetric metric, String purl) {
      String domain = purl;
      String name = MetricSuffixProperty.COMPUTED.getPropertyName(metric);
      String value = properties.get(domain, name);
      if (value == null) {
         return null;
      }
      try {
         return PropertyFacade.toDate(value);
      } catch (ParseException e) {
         throw new RuntimeException("property " + name + " for domain " + purl
               + " should had a Date string");
      }
   }

   /**
    * Return true if the project has been updated after the given date.
    * <p>
    *
    * @param purl
    *           project url to check for
    * @param date
    *           the date to check.
    * @return true if the project has been updated since the given date
    */
   protected final boolean isProjectUpdatedAfter(String purl, Date date) {
      ProjectTimeline timeline = getLastTimeline(purl,
            DBProjectActionType.PROJECT_UPDATED.name());
      if (timeline == null) {
         return false;
      }
      return timeline.getTimestamp().after(date);
   }

   /**
    * This method will check the defaults for a metric that should be computed
    * or not.
    * <p>
    * First will get the last computed date of the metric. If the metric has
    * never been computed for the given project it will return true. If metric
    * has been computed at a given date but that date is before the last update
    * of the project that means the project has been updated since then, and the
    * metric needs to check if it should compute or not.
    *
    * @param info
    *           the requested metric info
    * @param metrics
    *           the metric to check for computation
    *
    * @return true if the metric should be updated (computed) false otherwise.
    */
   protected List<ExecutableMetric> computeMetricCheckDefaults(
         MetricRunInfo info, List<ExecutableMetric> metrics) {
      // Should check the db if the metric has been computed before for
      // the given project
      PropertyFacade properties = resolveComponentOrManager(PropertyFacade.class);
      AnalysisRequestInfo request = getRequest(info);
      ArgsCheck.notNull("analysis request from metric info", request);
      String url = request.getRemoteProjectUrl();

      // List to place all metrics that should update for sure
      List<ExecutableMetric> toUpdate = new ArrayList<>(metrics.size());
      // Temp list to place all metrics that should ask the handler
      List<ExecutableMetric> toAsk = new ArrayList<>(metrics.size());
      for (ExecutableMetric metric : metrics) {
         // Get the last computed date
         Date last = getLastComputed(properties, metric, url);
         // If date is null that means we should compute this metric
         if (last == null) {
            toUpdate.add(metric);
            continue;
         }
         // If date is not null that means we should check which was the last
         // time
         // the project was updated. We do not need to check the creation time
         // because this metric was computed once since the creation of the
         // project. However we need to check if this project was updated since
         // last time this was computed
         if (isProjectUpdatedAfter(url, last)) {
            // The project was updated since this metric was computed
            // that means this metric has not been computed the last time so
            // we want to ask the handler if we should compute
            toAsk.add(metric);
         }
      }
      // Make this check in order to define if all metrics should update
      // that would be the case where no metric was updated and the size
      // of toUpdate should be the same as the size of metrics
      if (toAsk.isEmpty()) {
         return toUpdate;
      } else {
         int size = toUpdate.size() + toAsk.size();
         if (size == 0) {
            // This is the case where all metrics are up to date
            // so no need to go further
            return toUpdate;
         }
      }

      // Now ask the handler for all metrics that are not updated since the
      // last time the project was updated
      toUpdate.addAll(computeMetrics(info, toAsk));

      return toUpdate;
   }

   /**
    * Each metric handler should analyze the metric run info in order to check
    * if it should compute the metric. It will return true if the metric has to
    * be computed.
    * <p>
    * The general implementation of this handler will not call this method if
    * the metric has not been computed before (it will consider as it should be
    * computed) or if the last time the metric was updated is after the time the
    * project was last update. If the project was updated the last time after
    * the metric was executed that is the responsibility of metric handler to
    * check if it should be updated or not, therefore this method will be
    * called.
    *
    * @param info
    *           the event info that was received from a
    *           {@linkplain MetricRunEventType#START start} event. The info must
    *           contain a request of type {@link AnalysisRequestInfo}.
    * @param metrics
    *           the metric to check for computation
    *
    * @return true if the metric should be computed
    */
   protected abstract Collection<ExecutableMetric> computeMetrics(
         MetricRunInfo info, List<ExecutableMetric> metrics);
}
