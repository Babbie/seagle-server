package gr.uom.java.seagle.v2.analysis.smellDetection;

/**
 *
 * @author Thodoris
 */
public enum BadSmellEnum {

    GOD_CLASS("GOD_CLASS", "God Class", "Classes that tend to centralize the intelligence of the system. "
            + "An instance of a god-class performs most of the work, delegating only minor details "
            + "to a set of trivial classes and using the data from other classes.", GodClassDetector.class.getClass())
    ,
    FEATURE_ENVY("FEATURE_ENVY", "Feature Envy", "The Feature Envy design disharmony "
            + " refers to methods that seem more interested in the data"
            + "of other classes than that of their own class. These methods access "
            + "directly or via accessor methods a lot of data of other classes. This "
            + "might be a sign that the method was misplaced and that it should be "
            + "moved to another class.", FeatureEnvyMethodsDetector.class.getClass())
    ,
    DATA_CLASS("DATA_CLASS", "Data Class", "Data classes are plain classes with a set of properties and "
            + "their accessors, without implementing any business functionality. "
            + "They are indications of poor object oriented design, "
            + "where data are kept separated from logic.", DataClassDetector.class.getClass());

    private BadSmellEnum(String mnemonic, String name, String description, Class<?> detectorMnemonic) {
        this.mnemonic = mnemonic;
        this.name = name;
        this.description = description;
        this.badSmellDetectorClass = detectorMnemonic;
    }

    private final String mnemonic;
    private final String name;
    private final String description;
    private final Class<?> badSmellDetectorClass;

    public String getMnemonic() {
        return mnemonic;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Class<?> getDetectorClass() {
        return badSmellDetectorClass;
    }

}
