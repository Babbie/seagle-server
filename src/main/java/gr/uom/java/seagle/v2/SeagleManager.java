package gr.uom.java.seagle.v2;

import gr.uom.se.util.config.ConfigManager;
import gr.uom.se.util.manager.MainManager;

import javax.ejb.Local;

/**
 * The seagle manager, the main entry point for configurations and access to
 * other seagle features.
 * <p>
 * 
 * @author Elvis Ligu
 * @author Theodore Chakalis
 */
@Local
public interface SeagleManager extends MainManager {

   /**
    * Other clients must not use this method, this is only for initializing
    * Seagle manager. The preferred way to get config manager is calling
    * {@link #getManager(java.lang.Class)} and given the class of
    * {@link ConfigManager}.
    *
    * @return the internal config manager.
    */
   ConfigManager getConfig();

   /**
    * Get the path configration of Seagle.
    * <p>
    * 
    * @return the seagle path config.
    */
   SeaglePathConfig getSeaglePathConfig();

   /**
    * Resolve a managed component by looking up its name from an EJB context.
    * <p>
    * Each bean (session beans) or a resource that can be looked up by the
    * seagle context can be resolved. However the components that seagle can
    * expose are declared at seagle domain configuration file. If an entry in
    * that configuration is of type 'jndi.<class name>' then when calling this
    * method the seagle manager will look under this domain to find a property
    * like that for the given type. If a property was not found that means
    * seagle is not exposing this type to non ejb components so this method will
    * return null. If a malformed jndi name was specified at seagle
    * configuration this method may throw an exception.
    * 
    * @param type
    *           the managed type (i.e. a session bean) to look up.
    * @return the managed component
    */
   <T> T resolveComponent(Class<T> type);
}
