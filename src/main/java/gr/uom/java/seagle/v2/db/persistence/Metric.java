package gr.uom.java.seagle.v2.db.persistence;

import gr.uom.se.util.validation.ArgsCheck;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Represents a metric within system.
 * <p>
 * A metric is an instance that describes some quality characteristics of a
 * project. Metrics are categorized into categories. Each metric may have other
 * metric dependencies, for example a CBO metric applied to a project may have
 * dependency a source file metric.
 *
 * @author Elvis Ligu
 */
@Entity
@Table(name = "metric")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Metric.findAll", query = "SELECT m FROM Metric m"),
    @NamedQuery(name = "Metric.findById", query = "SELECT m FROM Metric m WHERE m.id = :id"),
    @NamedQuery(name = "Metric.findByMnemonic", query = "SELECT m FROM Metric m WHERE m.mnemonic = :mnemonic"),
    @NamedQuery(name = "Metric.findCategoriesByMetricMnemonic", query = "SELECT c FROM Metric m JOIN m.category c WHERE m.mnemonic = :mnemonic"),
    @NamedQuery(name = "Metric.findLanguagesByMetricMnemonic", query = "SELECT l FROM Metric m JOIN m.programmingLanguages l WHERE m.mnemonic = :mnemonic"),
    @NamedQuery(name = "Metric.findByName", query = "SELECT m FROM Metric m WHERE m.name = :name"),
    @NamedQuery(name = "Metric.findByProject", query = "SELECT m FROM Metric m JOIN m.registeredProjects p WHERE p = :project"),
    @NamedQuery(name = "Metric.findByCategory", query = "SELECT m FROM Metric m WHERE m.category = :category")})
public class Metric implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Id of this instance.
     * <p>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**
     * A unique keyword of this metric.
     * <p>
     * Max of 45 characters. May not be null or empty.
     */
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "mnemonic")
    private String mnemonic;

    /**
     * Name of this metric.
     * <p>
     * Max of 255 characters. May not be null or empty.
     */
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;

    /**
     * Description of this metric.
     * <p>
     * Max of 65535 characters.
     */
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;

    /**
     * Programming languages this metric support.
     * <p>
     * Optional property.
     */
    @ManyToMany(mappedBy = "metrics")
    private Collection<ProgrammingLanguage> programmingLanguages;

    /**
     * The dependencies of this metric.
     * <p>
     * Optional property.
     */
    @JoinTable(name = "metric_dependency", joinColumns = {
        @JoinColumn(name = "metric_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "metric_dependency_id", referencedColumnName = "id")})
   @ManyToMany
    private Collection<Metric> metricDependencies;

    /**
     * Metrics depended on this metric.
     * <p>
     * Optional property.
     */
    @ManyToMany(mappedBy = "metricDependencies")
    private Collection<Metric> dependentMetrics;

    /**
     * Projects this metric is registered to.
     * <p>
     */
    @JoinTable(name = "project_registered_metrics", joinColumns = {
        @JoinColumn(name = "metric_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "project_id", referencedColumnName = "id")})
    @ManyToMany
    private Collection<Project> registeredProjects;

    /**
     * Metric values computed for this type of metric.
     * <p>
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "metric")
    private Collection<MetricValue> computedValues;

    /**
     * Category where this metric belongs to.
     * <p>
     */
    @JoinColumn(name = "metric_category_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private MetricCategory category;

    /**
     * Create an instance of this metric.
     * <p>
     */
    public Metric() {
    }

    /**
     * @return the id of this metric
     */
    public Long getId() {
        return id;
    }

    /**
     * @return a unique keyword for this metric
     */
    public String getMnemonic() {
        return mnemonic;
    }

    /**
     * Set a unique keyword for this metric.
     * <p>
     * @param mnemonic must not be null not or empty. Max of 45 characters.
     */
    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }

    /**
     * @return the name of this metric
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of this metric.
     * <p>
     * @param name of this metric. Max of 255 characters.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description of this metric
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description of this metric.
     * <p>
     * @param description A large text max of 65K characters
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the programming languages this metric applies to.
     */
    //@XmlTransient
    public Collection<ProgrammingLanguage> getProgrammingLanguages() {
        return programmingLanguages;
    }

    /**
     * Set the programming languages this metric applies to.
     * <p>
     * @param programmingLanguages the languages this metric applies to
     */
    public void setProgrammingLanguages(Collection<ProgrammingLanguage> programmingLanguages) {
        this.programmingLanguages = programmingLanguages;
    }

    /**
     * Add a language where this metric applies to.
     * <p>
     * @param language this metric applies to. Must not be null.
     */
    public void addProgrammingLanguage(ProgrammingLanguage language) {
        ArgsCheck.notNull("language", language);
        if (programmingLanguages == null) {
            programmingLanguages = new HashSet<>();
        }
        programmingLanguages.add(language);
    }

    /**
     * Remove a language where this metric applies to.
     * <p>
     * @param language to be removed. Must not be null.
     */
    public void removeProgrammingLanguage(ProgrammingLanguage language) {
        ArgsCheck.notNull("language", language);
        if (programmingLanguages != null) {
            programmingLanguages.remove(language);
        }
    }

    /**
     * @return the metrics this metric depends on.
     */
    @XmlTransient
    public Collection<Metric> getMetricDependencies() {
        return metricDependencies;
    }

    /**
     * @param metricDependencies set the dependencies of this metric.
     */
    public void setMetricDependencies(Collection<Metric> metricDependencies) {
        this.metricDependencies = metricDependencies;
    }

    /**
     * Add a dependency for this metric.
     * <p>
     * @param metric dependency of this metric. Must not be null.
     */
    public void addDependency(Metric metric) {
        ArgsCheck.notNull("metric", metric);
        if (metricDependencies == null) {
            metricDependencies = new HashSet<>();
        }
        metricDependencies.add(metric);
    }

    /**
     * Remove a dependency from this metic.
     * <p>
     * @param metric the dependency of this metric to be removed. Must not be
     * null.
     */
    public void removeDependency(Metric metric) {
        ArgsCheck.notNull("metric", metric);
        if (metricDependencies != null) {
            metricDependencies.remove(metric);
        }
    }

    /**
     * @return metrics dependent on this metric
     */
    @XmlTransient
    public Collection<Metric> getDependentMetrics() {
        return dependentMetrics;
    }

    /**
     * Set the metrics that depends on this metric.
     * <p>
     * @param dependentMetrics metrics depending on this metric
     */
    public void setDependentMetrics(Collection<Metric> dependentMetrics) {
        this.dependentMetrics = dependentMetrics;
    }

    /**
     * Add a metric that depends on this metric.
     * <p>
     * @param metric depending on this metric
     */
    public void addDependent(Metric metric) {
        ArgsCheck.notNull("metric", metric);
        if (dependentMetrics == null) {
            dependentMetrics = new HashSet<>();
        }
        dependentMetrics.add(metric);
    }

    /**
     * Remove the metric depending on this one.
     * <p>
     * @param metric to be removed
     */
    public void removeDependent(Metric metric) {
        ArgsCheck.notNull("metric", metric);
        if (dependentMetrics != null) {
            dependentMetrics.remove(metric);
        }
    }

    /**
     * @return the projects registering to metric
     */
    @XmlTransient
    public Collection<Project> getRegisteredProjects() {
        return registeredProjects;
    }

    /**
     * Set the projects that this metric is registered for future calculation.
     * <p>
     * When a project is inserted into the system, a list of metrics will be
     * registered to this project. After an event of calculate metrics all
     * registered metrics for a given project will be calculated.
     *
     * @param registeredProjects the project this metric is registered to.
     */
    public void setRegisteredProjects(Collection<Project> registeredProjects) {
        this.registeredProjects = registeredProjects;
    }

    /**
     * Register this metric to the specified project, for future calculations.
     * <p>
     * When a project is inserted into the system, a list of metrics will be
     * registered to this project. After an event of calculate metrics all
     * registered metrics for a given project will be calculated.
     *
     * @param project to register this metric for future calculation. Must not
     * be null.
     */
    public void registerProject(Project project) {
        ArgsCheck.notNull("project", project);
        boolean add = false;
        if (registeredProjects == null) {
            registeredProjects = new HashSet<>();
            add = true;
        }
        if(!add) {
            if(registeredProjects.contains(project)) {
                return;
            }
        }
        registeredProjects.add(project);
    }

    /**
     * Deregister this metric to the specified project, for future calculations.
     * <p>
     * When a project is inserted into the system, a list of metrics will be
     * registered to this project. After an event of calculate metrics all
     * registered metrics for a given project will be calculated. Deregistering
     * a metric means removing it from future calculations on the given project.
     *
     * @param project to register this metric for future calculation. Must not
     * be null.
     */
    public void deregisterProject(Project project) {
        ArgsCheck.notNull("project", project);
        if (registeredProjects != null) {
            registeredProjects.remove(project);
        }
    }

    /**
     * The values of this metric that are computed as of now.
     * <p>
     * This method may produce a very large collection of metric values.
     * Usage of this method should be avoided, and is available only for
     * internal use.
     * @return the values of this metric that are computed as of now.
     */
    @XmlTransient
    public Collection<MetricValue> getComputedValues() {
        return computedValues;
    }

    /**
     * Set the values computed so far.
     * <p>
     * Usage of this method should be avoided, and is available only for
     * internal use.
     * @param computedValues the values of this metric computed so far.
     */
    public void setComputedValues(Collection<MetricValue> computedValues) {
        this.computedValues = computedValues;
    }
    
    public void addComputedValue(MetricValue metricValue){
        if(this.computedValues == null){
            this.computedValues = new HashSet<>(); 
        }
        this.computedValues.add(metricValue);
    }

    /**
     * @return the category of this metric 
     */
    public MetricCategory getCategory() {
        return category;
    }

    /**
     * Set the category of this metric.
     * @param category where this metric belongs to.
     */
    public void setCategory(MetricCategory category) {
        this.category = category;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Metric)) {
            return false;
        }
        Metric other = (Metric) object;
        if(this.name.equals(other.name)) {
            return true;
        }
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "db.Metric[ id=" + id + " ],["+name+"]";
    }

}
