package gr.uom.java.seagle.v2.analysis.metrics.sourceCode;

import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.MethodObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.java.ast.util.ExpressionExtractor;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.java.seagle.v2.analysis.project.evolution.JavaProject;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.WhileStatement;

/**
 *
 * @author Thodoris Chaikalis
 */
public class WMC extends AbstractJavaExecutableMetric {

    public static final String MNEMONIC = "WMC";

    public WMC(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(SystemObject systemObject, JavaProject javaProject) {
        ListIterator<ClassObject> classIterator = systemObject.getClassListIterator();
        Map<String, Double> valuePerClass = new LinkedHashMap<>();

        while (classIterator.hasNext()) {
            ClassObject classObject = classIterator.next();
            calculateCC(valuePerClass, classObject);
        }
        storeValuesForAllNodesInMemory(getMnemonic(), valuePerClass, javaProject);
         storeProjectLevelAggregationMetricInMemory(getMnemonic(), javaProject, valuePerClass, MetricAggregationStrategy.Average);
    }

    private void calculateCC(Map<String, Double> valuePerClass, ClassObject classObject) {
        int complexity = 0;
        for (MethodObject mo : classObject.getMethodList()) {
            MethodDeclaration method = mo.getMethodDeclaration();
            CyclomaticComplexityVisitor ccVisitor = new CyclomaticComplexityVisitor();
            method.accept(ccVisitor);
            complexity += ccVisitor.getCyclomaticComplexity();
        }
        valuePerClass.put(classObject.getName(), (double) complexity);
    }

    public class CyclomaticComplexityVisitor extends ASTVisitor {

        private double cyclomaticComplexity;

        public CyclomaticComplexityVisitor() {
            cyclomaticComplexity = 1;
        }

        public double getCyclomaticComplexity() {
            return cyclomaticComplexity;
        }

        @Override
        public boolean visit(IfStatement stmt) {
            cyclomaticComplexity++;
            ExpressionExtractor exprExtractor = new ExpressionExtractor();
            List<Expression> infixExpressions = exprExtractor.getInfixExpressions(stmt);
            int numberOfInfixExpressions = infixExpressions.size();
            if (numberOfInfixExpressions > 2) {
                cyclomaticComplexity += numberOfInfixExpressions - 1;
            }
            return super.visit(stmt);
        }

        @Override
        public boolean visit(WhileStatement stmt) {
            cyclomaticComplexity++;
            return super.visit(stmt);
        }

        @Override
        public boolean visit(ForStatement stmt) {
            cyclomaticComplexity++;
            return super.visit(stmt);
        }

        @Override
        public boolean visit(EnhancedForStatement stmt) {
            cyclomaticComplexity++;
            return super.visit(stmt);
        }

        @Override
        public boolean visit(DoStatement stmt) {
            cyclomaticComplexity++;
            return super.visit(stmt);
        }

        @Override
        public boolean visit(SwitchStatement stmt) {
            cyclomaticComplexity++;
            return super.visit(stmt);
        }

    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "The WMC metric is the sum of the complexities of all class methods. "
                + "It is an indicator of how much effort is required to develop and maintain a particular class.";
    }

    @Override
    public String getName() {
        return "Weighted Method Complexity";
    }

}
