package gr.uom.java.seagle.v2.policy;

import gr.uom.se.util.validation.ArgsCheck;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 *
 * @author Theodore Chaikalis & Elvis Ligu
 */
public class FilePolicyImp implements FilePolicy {

   private final ReadWriteLock mapLock = new ReentrantReadWriteLock();
   private final Map<Path, ReadWriteLock> pathLocks = new HashMap<>();

   public FilePolicyImp() {
   }

   /**
    * Try to lock for read on the given path.
    * <p>
    * Ensure that after the work on this lock call the unlock method on the same
    * path, otherwise other threads will get stuck.
    *
    * @param path
    *           the path to lock for reading.
    */
   @Override
   public void readLockPath(Path path) {
      ArgsCheck.notNull("path", path);
      mapLock.writeLock().lock();
      try {
         ReadWriteLock lock = getOrCreateLock(path);
         lock.readLock().lock();
      } finally {
         mapLock.writeLock().unlock();
      }
   }

   /**
    * Check the map if there is a lock object for the given path. If present
    * return it, otherwise create one, put it to map and return it.
    *
    * @param path
    * @return
    */
   private ReadWriteLock getOrCreateLock(Path path) {
      ReadWriteLock lock;
      if (pathLocks.containsKey(path)) {
         lock = pathLocks.get(path);
      } else {
         lock = new ReentrantReadWriteLock();
         pathLocks.put(path, lock);
      }
      return lock;
   }

   /**
    * Unlock a previous read lock for the given path.
    * <p>
    * This method should be used immediately after a read lock is requested on
    * the given path, and the reading has finished.
    *
    * @param path
    *           the path to unlock for reading.
    */
   @Override
   public void readUnlockPath(Path path) {
      ArgsCheck.notNull("path", path);
      mapLock.writeLock().lock();
      try {
         ReadWriteLock lock = pathLocks.get(path);
         if (lock != null) {
            lock.readLock().unlock();
            pathLocks.remove(lock);
         }
      } finally {
         mapLock.writeLock().unlock();
      }
   }

   /**
    * Try to lock for write on the given path.
    * <p>
    * Ensure that after the work on this lock call the unlock method on the same
    * path, otherwise other threads will get stuck.
    *
    * @param path
    *           the path to lock for writing.
    */
   @Override
   public void writeLockPath(Path path) {
      ArgsCheck.notNull("path", path);
      mapLock.writeLock().lock();
      try {
         ReadWriteLock lock = getOrCreateLock(path);
         lock.writeLock().lock();
      } finally {
         mapLock.writeLock().unlock();
      }
   }

   /**
    * Unlock a previous write lock for the given path.
    * <p>
    * This method should be used immediately after a write lock is requested on
    * the given path, and the writing has finished.
    *
    * @param path
    *           the path to unlock for writing.
    */
   @Override
   public void writeUnlockPath(Path path) {
      ArgsCheck.notNull("path", path);
      mapLock.writeLock().lock();
      try {
         ReadWriteLock lock = pathLocks.get(path);
         if (lock != null) {
            lock.writeLock().unlock();
            pathLocks.remove(lock);
         }
      } finally {
         mapLock.writeLock().unlock();
      }
   }
}
