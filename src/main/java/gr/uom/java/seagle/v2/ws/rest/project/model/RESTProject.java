/**
 *
 */
package gr.uom.java.seagle.v2.ws.rest.project.model;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Elvis Ligu
 */
@XmlRootElement(name = "project")
public class RESTProject implements Comparable<RESTProject> {

    private long id;
    private String name;
    private String url;
    private Date inserted;
    private long versionCount;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getVersionCount() {
        return versionCount;
    }

    public void setVersionCount(long versionCount) {
        this.versionCount = versionCount;
    }

    public Date getInserted() {
        return inserted;
    }

    public void setInserted(Date inserted) {
        this.inserted = inserted;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Date getAnalyzed() {
        return analyzed;
    }

    public void setAnalyzed(Date analyzed) {
        this.analyzed = analyzed;
    }

    private Date updated;
    private Date analyzed;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public RESTProject(String name, String url) {
        this.name = name;
        this.url = url;
    }

    /**
     *
     */
    public RESTProject() {
    }

    @Override
    public int compareTo(RESTProject o) {
        return -1*getAnalyzed().compareTo(o.getAnalyzed());
    }

}
