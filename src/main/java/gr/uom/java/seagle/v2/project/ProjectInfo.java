package gr.uom.java.seagle.v2.project;

import gr.uom.se.util.validation.ArgsCheck;

/**
 *
 * @author Elvis Ligu & Theodore Chaikalis
 */
public class ProjectInfo {

    private final String name;
    private final String remoteUrl;
    private final String localFolder;

    public ProjectInfo(String name, String remoteUrl, String localFolder) {
        ArgsCheck.notEmpty("name", name);
        ArgsCheck.notEmpty("remoteUrl", remoteUrl);
        ArgsCheck.notEmpty("localFolder", localFolder);
        this.name = name;
        this.remoteUrl = remoteUrl;
        this.localFolder = localFolder;
    }

    public String getName() {
        return name;
    }

    public String getRemoteUrl() {
        return remoteUrl;
    }

    public String getLocalFolder() {
        return localFolder;
    }
}
