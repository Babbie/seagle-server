/**
 * 
 */
package gr.uom.java.seagle.v2.ws.rest.project.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Elvis Ligu
 */
@XmlRootElement(name = "versions")
public class RESTVersionCollection {

   private Collection<RESTVersion> versions;
   
   /**
    * 
    */
   public RESTVersionCollection() {
      this(null);
   }

   public RESTVersionCollection(Collection<RESTVersion> versions) {
      this.setVersions(versions);
   }

   public Collection<RESTVersion> getVersions() {
      return versions;
   }

   public void setVersions(Collection<RESTVersion> versions) {
      if(versions == null) {
         versions = new ArrayList<>();
      }
      this.versions = versions;
   }
   
   public void add(RESTVersion version) {
      this.versions.add(version);
   }
   
}
