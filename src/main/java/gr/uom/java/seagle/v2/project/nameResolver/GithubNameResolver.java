package gr.uom.java.seagle.v2.project.nameResolver;

import gr.uom.se.util.validation.ArgsCheck;

import java.net.URI;
import java.net.URISyntaxException;

/**
 *
 * @author Theodore Chaikalis
 */
public class GithubNameResolver implements ProjectNameResolver {

   private GithubNameResolver() {
   }

   private static ProjectNameResolver INSTANCE = new GithubNameResolver();
   
   public static ProjectNameResolver getInstance() {
      return INSTANCE;
   }
   
   @Override
   public String resolveName(String gitPath) {
      ArgsCheck.notNull("path", gitPath);
      URI uri = tryGetURI(gitPath);
      // Uri can not be resolved so a name can not to
      if (uri == null) {
         return null;
      }

      String scheme = uri.getScheme();
      if (scheme == null) {
         return null;
      }
      if (!(scheme.equalsIgnoreCase("https") || scheme.equalsIgnoreCase("http") || scheme
            .equalsIgnoreCase("git"))) {
         return null;
      }
      String host = uri.getHost();
      if (!host.equalsIgnoreCase("github.com")) {
         return null;
      }

      String name = null;
      String path = uri.getPath();
      if (path == null || path.isEmpty() || path.equals("/")) {
         return null;
      }

      int index = path.lastIndexOf('/');
      if (index > 0) {
         name = path.substring(index + 1);
      } else {
         // No github.com/uname/repo found?
         return null;
      }

      if (name.endsWith(".git")) {
         int len = name.length();
         name = name.substring(0, len - 4);
      }
      if (name.isEmpty()) {
         name = null;
      }
      return name;
   }

   private URI tryGetURI(String path) {
      try {
         return new URI(path.trim());
      } catch (URISyntaxException e) {
         return null;
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public int hashCode() {
      return EqualityUtils.typeHash(this);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean equals(Object obj) {
      return EqualityUtils.typeEqual(this, obj);
   }

   public static void main(String... args) throws URISyntaxException {

      System.out.println(new URI("c://elvis/c").getHost());
   }
}
