
package gr.uom.java.seagle.v2.analysis.graph.javaGraph;

import gr.uom.java.seagle.v2.analysis.graph.AbstractEdge;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.db.persistence.Version;
import java.io.Serializable;

/**
 *
 * @author Theodore Chaikalis
 */
public  class JavaEdge extends AbstractEdge implements Serializable {

    
    public JavaEdge(AbstractNode sourceNode, AbstractNode targetNode, Version versionCreated) {
        super(sourceNode, targetNode, versionCreated);
    }

    
}
