package gr.uom.java.seagle.v2.analysis.metrics.graph.evolutionary;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.distributions.StatUtils;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.evolution.GraphEvolution;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.java.seagle.v2.db.persistence.controllers.MetricValueFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectVersionMetricFacade;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author Theodore Chaikalis
 */
public class ExistingNodesPerVersion extends AbstractGraphEvolutionaryMetric {

    public static final String MNEMONIC = "EXISTING_NODES_PER_VERSION";

    public ExistingNodesPerVersion(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(GraphEvolution ge) {
        Map<Version, Set<AbstractNode>> existingNodesMap = new LinkedHashMap<>();
        Map<Integer, Integer> existingNodeCountFrequencyMap = new TreeMap<>();
        Version[] versionList = ge.getVersionArray();
        for (int i = 1; i < versionList.length; i++) {
            Version currentVersion = versionList[i];
            Set<AbstractNode> existingNodes = new LinkedHashSet<>();
            Collection<AbstractNode> nodesInCurrentVersion = ge.getGraphForAVersion(currentVersion).getVertices();
            for (AbstractNode node : nodesInCurrentVersion) {
                if (node.getAge() > 0) {
                    existingNodes.add(node);
                }
            }
            existingNodesMap.put(currentVersion, existingNodes);
            StatUtils.updateFrequencyMap(existingNodeCountFrequencyMap, existingNodes.size());
        }
        ge.setExistingNodesMap(existingNodesMap);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Existing nodes per version";
    }

    @Override
    public String getName() {
        return "Existing nodes per version";
    }

}
