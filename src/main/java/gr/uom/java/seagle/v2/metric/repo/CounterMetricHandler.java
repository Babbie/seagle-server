/**
 * 
 */
package gr.uom.java.seagle.v2.metric.repo;

import gr.uom.java.seagle.v2.metric.imp.AbstractMetricHandler;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.db.persistence.Property;
import gr.uom.java.seagle.v2.db.persistence.controllers.PropertyFacade;
import gr.uom.java.seagle.v2.metric.MetricRunInfo;
import gr.uom.se.util.module.annotations.NULLVal;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * @author Elvis Ligu
 */
public class CounterMetricHandler extends AbstractMetricHandler {

   /**
    * @param seagleManager
    * @param provider
    * @param repo
    */
   @ProvideModule
   public CounterMetricHandler(
         @gr.uom.se.util.module.annotations.Property(name = NULLVal.NO_PROP) SeagleManager seagleManager) {
      super(seagleManager);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Runnable getRunner(List<ExecutableMetric> metrics, MetricRunInfo info) {
      checkMetric(info, metrics);
      return new CommitCounterTask(seagleManager, info);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected void checkMetric(MetricRunInfo info, List<ExecutableMetric> metrics) {
      super.checkMetric(info, metrics);
      if (metrics.size() != 1) {
         throw new IllegalArgumentException(
               "metrics should contain only one metric "
                     + RepoMetricEnum.COM_PROJ.getMnemonic());
      }
      ExecutableMetric metric = metrics.get(0);
      // Precaution check for the type of metric we recognize
      if (!metric.getMnemonic().equals(RepoMetricEnum.COM_PROJ.getMnemonic())) {
         throw new IllegalArgumentException("unrecognized metric "
               + metric.getMnemonic());
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Collection<ExecutableMetric> computeMetrics(MetricRunInfo info,
         List<ExecutableMetric> metrics) {
      checkMetric(info, metrics);

      // At this point the metric has been computed before and the last
      // update time of the project is after the last time this metric
      // was computed, so we should check all the heads of the branches,
      // if one of them is updated we should rerun the metric
      // Check specific properties first
      AnalysisRequestInfo request = getRequest(info);
      String url = request.getRemoteProjectUrl();
      PropertyFacade properties = resolveComponentOrManager(PropertyFacade.class);
      try {
         // Get all properties that has a prefix BRANCH under the domain url
         // these are maintained by project property updater and are
         // created, updated, deleted each time a project undergone the same
         // action
         Collection<Property> bProperties = ProjectBranchPropertyUpdater
               .getBranchProperties(properties, url);

         // These are branch properties that are created by counter metric
         // and are updated only when this metric is computed
         Collection<Property> mbProperties = getMetricBranchProperties(
               properties, url);

         for (Property bProperty : bProperties) {

            // Find the corresponding mbProperty
            Property mbProperty = null;
            Iterator<Property> it = mbProperties.iterator();
            String name = bProperty.getName();
            while (mbProperty == null && it.hasNext()) {
               Property current = it.next();
               if (current.getName().endsWith(name)) {
                  // found it here
                  mbProperty = current;
               }
            }
            // Property should update if there is no branch
            // property within db
            if (mbProperty == null) {
               return metrics;
            } else {
               // The head of the branch of the property is not the
               // same as the head of the current branch, that means the
               // current branch has been updated
               if (!isBranchUpToDate(bProperty, mbProperty)) {
                  return metrics;
               }
            }
         }
      } catch (VCSRepositoryException e) {
         throw new RuntimeException(e);
      }
      // At this point we have checked the branches and they are
      // not updated, they are the same as the last time we computed
      // the metric
      metrics.remove(0);
      return metrics;
   }

   static Collection<Property> getMetricBranchProperties(
         PropertyFacade properties, String url) {
      String domain = url;
      String prefix = getMetricBranchPrefix();
      return properties.getDomainByPrefix(domain, prefix);
   }

   static String getMetricBranchPropertyName(String bid) {
      return getMetricBranchPrefix() + "." + bid;
   }

   static String getMetricBranchPrefix() {
      return RepoMetricEnum.COM_PROJ
            .getPrefixedPropertyName(ProjectBranchPropertyUpdater.BRANCH_SUFFIX);
   }

   static String extractMetricBranchId(Property property) {
      String name = property.getName();
      String prefix = getMetricBranchPrefix();
      if (!name.startsWith(prefix)) {
         throw new IllegalArgumentException(
               "Property is not a branch property " + property.getDomain()
                     + ":" + name);
      }
      return name.substring(prefix.length());
   }

   private boolean isBranchUpToDate(Property bProperty, Property mbProperty)
         throws VCSRepositoryException {
      String head = bProperty.getValue();
      String value = bProperty.getValue();
      return value.equals(head);
   }
}
