package gr.uom.java.seagle.v2.db.persistence;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This entity combines a given node, a specified version and a metric value.
 * <p>
 * Given these three combined entities this entity specifies for a node in a
 * given version a computed metric (its value).
 *
 * @author Elvis Ligu
 */
@Entity
@Table(name = "node_version_metric")
@IdClass(NodeVersionMetricPK.class)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NodeVersionMetric.findAll", query = "SELECT n FROM NodeVersionMetric n"),
    @NamedQuery(name = "NodeVersionMetric.findByNodeName", query = "SELECT nvm FROM NodeVersionMetric nvm JOIN nvm.node nod WHERE nod.name = :nodeName AND nvm.version = :version"),
    @NamedQuery(name = "NodeVersionMetric.findByVersion", query = "SELECT n FROM NodeVersionMetric n WHERE n.version = :version")})
public class NodeVersionMetric implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * The node to where the metric value was computed.
     * <p>
     */
    @Id
    @ManyToOne
    private Node node;

    /**
     * The version of the node where the metric value was computed.
     * <p>
     */
    @Id
    @ManyToOne
    private Version version;

    /**
     * The metric value of the node.
     * <p>
     */
    @Id
    @OneToOne(cascade = CascadeType.ALL)
    private MetricValue metricValue;

    public NodeVersionMetric() {
    }

    /**
     * @return the node
     */
    public Node getNode() {
        return node;
    }

    /**
     * Set the node.
     * <p>
     * @param node
     */
    public void setNode(Node node) {
        this.node = node;
    }

    /**
     * @return the version
     */
    public Version getVersion() {
        return version;
    }

    /**
     * Set the version.
     * <p>
     * @param version
     */
    public void setVersion(Version version) {
        this.version = version;
    }

    /**
     * @return the computed value for the given node in the specified version
     */
    public MetricValue getMetricValue() {
        return metricValue;
    }

    public void setMetricValue(MetricValue metricValue) {
        this.metricValue = metricValue;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash += (node != null ? node.hashCode() : 0);
        hash += (version != null ? version.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NodeVersionMetric other = (NodeVersionMetric) obj;
        if (!Objects.equals(this.version, other.version)) {
            return false;
        }
        return Objects.equals(this.node, other.node) && Objects.equals(this.version, other.version);
    }

}
