package gr.uom.java.seagle.v2.analysis.metrics.graph;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.graph.evolution.GraphEvolution;
import gr.uom.java.seagle.v2.analysis.metrics.AbstractAnalysisMetric;
import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.metric.Category;
import gr.uom.java.seagle.v2.metric.Language;
import gr.uom.java.seagle.v2.metric.MetricManager;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;

/**
 *
 * @author Theodore Chaikalis
 */
public abstract class AbstractGraphExecutableMetric extends AbstractAnalysisMetric implements ExecutableMetric {

    public AbstractGraphExecutableMetric(SeagleManager seagleManager) {
        super(seagleManager);
    }

    public abstract void calculate(SoftwareProject softwareProject);

    @Override
    public String getCategory() {
        return Category.GRAPH_METRICS;
    }

    @Override
    public String[] getLanguages() {
        return new String[]{Language.UNSPECIFIED};
    }

    protected Metric resolveMetricByMnemonic(String mnemonic) {
        MetricManager mm = resolveComponentOrManager(MetricManager.class);
        return mm.getMetricByMnemonic(mnemonic);
    }
    
}
