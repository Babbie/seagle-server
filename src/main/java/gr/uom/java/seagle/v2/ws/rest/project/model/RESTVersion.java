/**
 *
 */
package gr.uom.java.seagle.v2.ws.rest.project.model;

import gr.uom.java.seagle.v2.db.persistence.Version;

import java.util.Date;

/**
 * @author Elvis Ligu
 */
public class RESTVersion {

    /**
     *
     */
    public RESTVersion() {
    }

    private Long versionDbId;
    private String commitHashId;
    private String vname;
    private Date cdate;

    public RESTVersion(Version version) {
        this.versionDbId = version.getId();
        this.commitHashId = version.getCommitID();
        this.vname = version.getName();
        this.cdate = version.getDate();
    }

    public Long getVersionDbId() {
        return versionDbId;
    }

    public String getCommitHashId() {
        return commitHashId;
    }

    public Date getDate() {
        return cdate;
    }

    public String getName() {
        return vname;
    }

    public void setName(String vname) {
        this.vname = vname;
    }

    public void setVersionDbId(Long versionDbId) {
        this.versionDbId = versionDbId;
    }

    public void setDate(Date cdate) {
        this.cdate = cdate;
    }

    public void setCommitHashId(String id) {
        this.commitHashId = id;
    }
}
