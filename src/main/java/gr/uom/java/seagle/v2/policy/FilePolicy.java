package gr.uom.java.seagle.v2.policy;

import java.nio.file.Path;

/**
 * A file policy manager (a single instance for whole system). Use main manager
 * to get an instance of this type, and use these methods to request locks
 * (read/write) on file paths (be it a folder or file). Be careful when
 * requesting a lock, after finishing a job to unlock the same path, otherwise
 * it can cause problems. A general use case would be:
 * 
 * <pre>
 * FilePolicy fpolicy = mainManager.getManager(FilePolicy.class);
 * fpolicy.readLockPath(path);
 * try {
 *     ...
 * } finally {
 *     fpolicy.readUnlockPath(path);
 * }
 * </pre>
 *
 * @author Theodore Chaikalis & Elvis Ligu
 */
public interface FilePolicy {

   /**
    * Try to lock for read on the given path.
    * <p>
    * Ensure that after the work on this lock call the unlock method on the same
    * path, otherwise other threads will get stuck.
    *
    * @param path
    *           the path to lock for reading.
    */
   void readLockPath(Path path);

   /**
    * Unlock a previous read lock for the given path.
    * <p>
    * This method should be used immediately after a read lock is requested on
    * the given path, and the reading has finished.
    *
    * @param path
    *           the path to unlock for reading.
    */
   void readUnlockPath(Path path);

   /**
    * Try to lock for write on the given path.
    * <p>
    * Ensure that after the work on this lock call the unlock method on the same
    * path, otherwise other threads will get stuck.
    *
    * @param path
    *           the path to lock for writing.
    */
   void writeLockPath(Path path);

   /**
    * Unlock a previous write lock for the given path.
    * <p>
    * This method should be used immediately after a write lock is requested on
    * the given path, and the writing has finished.
    *
    * @param path
    *           the path to unlock for writing.
    */
   void writeUnlockPath(Path path);
}
