package gr.uom.java.seagle.v2.metric;

import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.se.util.event.Event;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author elvis
 */
final class RequestQueue {

   /**
    * The queue with running request
    */
   private final ConcurrentHashMap<Long, Pair> requests;
   private final ConcurrentHashMap<Pair, Long> ids;

   /**
    * The max allowed requests
    */
   private final int maxSize;

   /**
    * The current number of requests in queue
    */
   private final AtomicInteger reqNo = new AtomicInteger(0);

   /**
    * The lock for threads that waits when the queue is full.
    */
   private final ArrayBlockingQueue<Object> lock;
   /**
    * Used as a dummy object to put it in lock queue.
    */
   private final Object queueObject = new Object();
   
   public RequestQueue(int size, boolean wait) {
      if (size < 1) {
         throw new IllegalArgumentException("size must be greater than 1");
      }
      this.requests = new ConcurrentHashMap<>();
      this.ids = new ConcurrentHashMap<>();
      this.maxSize = size;
      if (wait) {
         lock = new ArrayBlockingQueue<>(1);
      } else {
         lock = null;
      }
   }

   public RequestQueue(int size) {
      this(size, true);
   }

   final void tryAdd() {
      if (lock != null) {
         // In this case all threads have to wait until
         // the size become less then max size
         if (reqNo.incrementAndGet() >= maxSize) {
            try {
               lock.put(queueObject);
            } catch (InterruptedException ex) {
               throw new RuntimeException(ex);
            }
         }
         // In this case an exception is thrown if
         // size is getting greater than maximum
      } else if (reqNo.incrementAndGet() > maxSize) {
         reqNo.getAndDecrement();
         throw new RuntimeException("Maximum number of request "
                 + maxSize + " was achieved");
      }
   }

   final void tryRemove() {
      if (lock != null) {
         // In this case release a thread from waiting
         // if we are at the maximum size
         if (reqNo.decrementAndGet() == maxSize) {
            lock.poll();
         }
      } else {
         reqNo.decrementAndGet();
      }
   }

   final Event schedule(AnalysisRequestInfo info, String[] mnemonics) {

      // Do this to check the current size
      tryAdd();

      // Create an event first
      Event event = MetricRunInfo.start(info, mnemonics);
      long id = extractId(event);

      // Create the pair
      // 1 - Create the set of mnemonics
      Set<String> set = Collections.newSetFromMap(
              new ConcurrentHashMap<String, Boolean>(mnemonics.length));

      for (String str : mnemonics) {
         set.add(str);
      }
      Pair pair = new Pair(info, set);
      Long oldId = ids.putIfAbsent(pair, id);
      // There is a request for this, so we return a null event
      // to inform the client that he doesn't need to send the event
      if (oldId != null) {
         requests.get(oldId).queue.add(pair);
         return null;
      } else {
         requests.put(id, pair);
         return event;
      }
   }

   final NextEvent scheduleNext(long id) {
      tryRemove();
      Pair pair = requests.remove(id);
      if (pair == null) {
         return null;
      }

      ids.remove(pair);
      NextEvent event = new NextEvent(null, pair.info, pair.mnemonics);
      if (pair.queue.isEmpty()) {
         return event;
      }

      Pair next = pair.queue.poll();
      if (next != null) {
         tryRemove();
         event.event = schedule(next.info, (String[]) next.mnemonics.toArray());
         
      }
      return event;
   }

   final Pair get(long id) {
      return requests.get(id);
   }
   
   private static long extractId(Event event) {
      return ((MetricRunInfo) event.getInfo().getDescription()).id();
   }

   static final class NextEvent {

      /**
       * The next event
       */
      Event event;
      /**
       * The current request
       */
      AnalysisRequestInfo info;
      /**
       * Mnemonics for the request
       */
      Set<String> mnemonics;

      public NextEvent(Event event, AnalysisRequestInfo info, Set<String> mnemonics) {
         this.event = event;
         this.info = info;
         this.mnemonics = mnemonics;
      }
   }

   /**
    * This class is used as a tuple for an analysis request.
    * <p>
    * Each pair is equal to another pair if the info properties are equals.
    */
   static final class Pair {

      /**
       * The current request
       */
      AnalysisRequestInfo info;
      /**
       * Mnemonics for the request
       */
      Set<String> mnemonics;
      /**
       * Queued requests of the same type
       */
      ConcurrentLinkedQueue<Pair> queue = new ConcurrentLinkedQueue<>();

      public Pair(AnalysisRequestInfo info, Set<String> mnemonics) {
         this.info = info;
         this.mnemonics = mnemonics;
      }

      @Override
      public int hashCode() {
         int hash = 3;
         hash = 79 * hash + Objects.hashCode(this.info);
         return hash;
      }

      public boolean equals(Object obj) {
         if (obj instanceof AnalysisRequestInfo) {
            AnalysisRequestInfo otherInfo = (AnalysisRequestInfo) obj;
            return info.equals(otherInfo);
         }
         Pair other = (Pair) obj;
         return info.equals(other.info);
      }
   }
}
