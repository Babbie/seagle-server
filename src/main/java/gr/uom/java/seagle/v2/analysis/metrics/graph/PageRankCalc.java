package gr.uom.java.seagle.v2.analysis.metrics.graph;

import edu.uci.ics.jung.algorithms.scoring.PageRank;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.graph.AbstractEdge;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.SoftwareGraph;
import gr.uom.java.seagle.v2.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Theodore Chaikalis
 */
public class PageRankCalc extends AbstractGraphExecutableMetric {

    public static final String MNEMONIC = "PAGE_RANK";

    public PageRankCalc(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(SoftwareProject softwareProject) {

        Map<String, Double> valuePerClass = new LinkedHashMap<>();
        SoftwareGraph<AbstractNode, AbstractEdge> softwareGraph = softwareProject.getProjectGraph();
        PageRank<AbstractNode, AbstractEdge> pageRank = new PageRank<>(softwareGraph, 0.15);
        pageRank.evaluate();
        for (AbstractNode node : softwareGraph.getVertices()) {
            double nodePageRank = pageRank.getVertexScore(node);
            valuePerClass.put(node.getName(), nodePageRank);
        }
        storeValuesForAllNodesInMemory(getMnemonic(), valuePerClass, softwareProject);
        storeProjectLevelAggregationMetricInMemory(getMnemonic(), softwareProject, valuePerClass, MetricAggregationStrategy.Average);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "PageRank works by counting the number and quality of links to a vertex to "
                + "determine a rough estimate of how important the vertex is. "
                + "The underlying assumption is that more important vertices are "
                + "likely to receive more links from other vertices."
                + "More Info: https://en.wikipedia.org/wiki/PageRank";
    }

    @Override
    public String getName() {
        return "Page Rank";
    }

}
