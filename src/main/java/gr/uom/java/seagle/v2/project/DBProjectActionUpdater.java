package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.AbstractSeagleListener;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventType;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author elvis
 */
public class DBProjectActionUpdater extends AbstractSeagleListener {

   public DBProjectActionUpdater(SeagleManager seagleManager,
         EventManager eventManager) {
      super(seagleManager, eventManager);
   }

   @Override
   protected void register() {
      super.register();
   }
   
   /**
    * {@inheritDoc}
    */
   @Override
   protected Set<EventType> getTypes() {
      Set<EventType> types = new HashSet<>();
      types.add(ProjectEventType.DB_INSERTED);
      types.add(ProjectEventType.DB_UPDATED);
      // Dont need to add the action updater to remove events because
      // it can't create a remove action for a project that has been removed
      // from db.
      return types;
   }
   
   @Override
   protected void acceptEvent(EventType type, EventInfo info) {
      if (type instanceof ProjectEventType) {
         ProjectInfo pInfo = (ProjectInfo) info.getDescription();
         ProjectEventType pType = (ProjectEventType) type;

         switch (pType) {
         case DB_INSERTED:
            createInsertAction(pInfo, info.getStartDate());
            break;
         // case DB_REMOVED: do nothing, as long as the project
         // was removed from db we can not create an action for
         // that project, because it will throw a DB exception
         case DB_UPDATED:
            createUpdateAction(pInfo, info.getStartDate());
            break;
         default:
            break;
         }
      }
   }

   private void createInsertAction(ProjectInfo info, Date startDate) {
      createAction(info, startDate, DBProjectActionType.PROJECT_INSERTED);
   }

   private void createUpdateAction(ProjectInfo info, Date startDate) {
      createAction(info, startDate, DBProjectActionType.PROJECT_UPDATED);
   }

   private void createAction(ProjectInfo info, Date startDate,
         DBProjectActionType ptype) {
      ProjectDBManager dbManager = resolveManager(ProjectDBManager.class);

      dbManager.createTimelineForProjectName(info.getName(), ptype.name(),
            startDate);
   }

   private <T> T resolveManager(Class<T> type) {
      T t = seagleManager.getManager(type);
      if (t == null) {
         throw new RuntimeException("Can not resolve manager " + type
               + " from seagle manager");
      }
      return t;
   }
}
