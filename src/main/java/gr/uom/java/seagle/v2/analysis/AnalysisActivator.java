package gr.uom.java.seagle.v2.analysis;

import gr.uom.java.seagle.v2.analysis.metrics.SourceCodeEvolutionAnalysisHandler;
import gr.uom.java.seagle.v2.analysis.metrics.AMetricsCollection;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.metrics.AbstractAnalysisMetric;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.metric.AbstractMetricActivator;
import gr.uom.java.seagle.v2.metric.LanguageAndMetricCategoryActivator;
import gr.uom.java.seagle.v2.metric.MetricManager;
import gr.uom.java.seagle.v2.metric.imp.MetricFactory;
import gr.uom.se.util.manager.annotations.Activator;
import gr.uom.se.util.module.annotations.NULLVal;
import gr.uom.se.util.module.annotations.Property;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.util.validation.ArgsCheck;

/**
 *
 * @author Theodore Chaikalis
 */
@Activator(
        dependencies = LanguageAndMetricCategoryActivator.class
)
public class AnalysisActivator extends AbstractMetricActivator {

    private EventManager eventManager;

    @ProvideModule
    public AnalysisActivator(
            @Property(name = NULLVal.NO_PROP) EventManager evm,
            @Property(name = NULLVal.NO_PROP) SeagleManager seagleManager,
            @Property(name = NULLVal.NO_PROP) MetricManager metricManager) {

        super(seagleManager, metricManager);
        ArgsCheck.notNull("evm", evm);
        this.eventManager = evm;
    }

    @Override
    protected void activate() {
        activateMetrics();
    }

    private void activateMetrics() {
        MetricManager metricManager = getMetricManager();
        AnalysisListener analysisListener = new AnalysisListener(seagleManager, eventManager);
        MetricFactory factory = MetricFactory.getInstance(analysisListener);

        AMetricsCollection.createSystemMetrics(seagleManager);

        SourceCodeEvolutionAnalysisHandler sourceCodeEvolutionAnalysisHandler = new SourceCodeEvolutionAnalysisHandler(seagleManager);

        for (AbstractAnalysisMetric metric : AMetricsCollection.getCreatedGraphMetrics()) {
            metricManager.activateMetric(metric.getCategory(), metric.getMnemonic(), metric.getName(), metric.getDescription());
            factory.registerHandler(metric, sourceCodeEvolutionAnalysisHandler);
        }
        
        for (AbstractAnalysisMetric metric : AMetricsCollection.getCreatedSourceCodeMetrics()) {
            metricManager.activateMetric(metric.getCategory(), metric.getMnemonic(), metric.getName(), metric.getDescription());
            factory.registerHandler(metric, sourceCodeEvolutionAnalysisHandler);
        }

        analysisListener.register();
    }

}
