package gr.uom.java.seagle.v2.ws.rest.project;

import gr.uom.java.seagle.v2.ws.rest.project.model.RESTVersionCollection;

import javax.ejb.Local;
import javax.ws.rs.core.Response;

/**
 * Version Requestor returns the versions of a project.
 * <p>
 * If the project does not exist, it will be cloned.
 *
 * @author Elvis Ligu & Theodore Chaikalis
 */
@Local
public interface VersionRequestor {

    /**
     * Returns the versions of the given project that is at the remote
     * repository.
     * <p>
     * Calling this method is the same as calling
     * {@link #getVersions(java.lang.String, boolean, boolean)} with both
     * parameters true.
     *
     * @param pUrl The remote URL of the desired project
     * @return a list of versions for the given project with the given remote
     * url
     */
    RESTVersionCollection getVersions(String pUrl);

    /**
     * Returns the versions of the given project that is at the remote
     * repository.
     * <p>
     * This method will return a response with 200 code only if the results of
     * calling {@link #getVersions(java.lang.String, boolean, boolean)} with the
     * same parameter returns a non empty list. If the list is empty it will
     * return a 204 (no content) code with no body. If the list is null it will
     * return a 404 (not found) response, and probably an explanation message.
     *
     * @param pUrl the remote URL of the requested project
     * @return a response to the client
     */
    public Response getVersionsWS(String pUrl);
}
