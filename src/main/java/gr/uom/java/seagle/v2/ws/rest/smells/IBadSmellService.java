package gr.uom.java.seagle.v2.ws.rest.smells;


import javax.ejb.Local;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Response;

/**
 *
 * @author elvis
 */
@Local
public interface IBadSmellService {
    
  // Response identifyBadSmells(String email, String projectURL);

   Response getProjectSmellsByVersion(String versionID);
   
   Response getProjectSmells(String projectName);
   
   Response getProjectSmellsWithUrl(String pUrl);

   void identifyBadSmells(AsyncResponse asyncResponse, String email, String projectUrl);

    public Response getAllProjectSmells(String projectName);

}
