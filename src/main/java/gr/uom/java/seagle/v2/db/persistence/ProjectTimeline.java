package gr.uom.java.seagle.v2.db.persistence;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The project action info entity.
 * <p>
 * When an event is registered an action is performed. If this action is related
 * to a project an entity is created of this type to keep track of the actions
 * within system.
 *
 * @author Elvis Ligu
 */
@Entity
@Table(name = "project_timeline")
@XmlRootElement
@NamedQueries({
      @NamedQuery(name = "ProjectTimeline.findAll", query = "SELECT p FROM ProjectTimeline p"),
      @NamedQuery(name = "ProjectTimeline.findById", query = "SELECT p FROM ProjectTimeline p WHERE p.id = :id"),
      @NamedQuery(name = "ProjectTimeline.findByProjectURLAndType", query = "SELECT p FROM ProjectTimeline p JOIN p.project pr JOIN p.actionType at WHERE pr.remoteRepoPath = :url AND at.mnemonic = :mnemonic"),
      @NamedQuery(name = "ProjectTimeline.findByProjectNameAndType", query = "SELECT p FROM ProjectTimeline p JOIN p.project pr JOIN p.actionType at WHERE pr.name = :name AND at.mnemonic = :mnemonic"),
      @NamedQuery(name = "ProjectTimeline.findByProjectName", query = "SELECT p FROM ProjectTimeline p JOIN p.project pr WHERE pr.name = :name"),
      @NamedQuery(name = "ProjectTimeline.findByProjectURL", query = "SELECT p FROM ProjectTimeline p JOIN p.project pr WHERE pr.remoteRepoPath = :url"),
      @NamedQuery(name = "ProjectTimeline.findByType", query = "SELECT p FROM ProjectTimeline p JOIN p.actionType at WHERE at.mnemonic = :mnemonic"),
      @NamedQuery(name = "ProjectTimeline.findByTimestamp", query = "SELECT p FROM ProjectTimeline p WHERE p.timestamp = :timestamp"),
      @NamedQuery(name = "ProjectTimeline.findCreatedBefore", query = "SELECT p FROM ProjectTimeline p WHERE p.timestamp <= :timestamp"),
      @NamedQuery(name = "ProjectTimeline.findCreatedAfter", query = "SELECT p FROM ProjectTimeline p WHERE p.timestamp >= :timestamp"),
      @NamedQuery(name = "ProjectTimeline.findLastByType", query = "SELECT p FROM ProjectTimeline p JOIN p.actionType at WHERE at.mnemonic = :mnemonic ORDER BY p.timestamp DESC"),
      @NamedQuery(name = "ProjectTimeline.findLastByProjectURLAndType", query = "SELECT p FROM ProjectTimeline p JOIN p.project pr JOIN p.actionType at WHERE pr.remoteRepoPath = :url AND at.mnemonic = :mnemonic ORDER BY p.timestamp DESC"),
      @NamedQuery(name = "ProjectTimeline.findLastByProjectNameAndType", query = "SELECT p FROM ProjectTimeline p JOIN p.project pr JOIN p.actionType at WHERE pr.name = :name AND at.mnemonic = :mnemonic ORDER BY p.timestamp DESC"),
      
      @NamedQuery(name = "ProjectTimeline.findCreatedWithin", query = "SELECT p FROM ProjectTimeline p WHERE p.timestamp >= :start AND p.timestamp <= :end") })
public class ProjectTimeline implements Serializable {

   private static final long serialVersionUID = 1L;

   /**
    * The id of this entity.
    * <p>
    */
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
  // @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="TimelineSequenceGenerator")
  // @SequenceGenerator(initialValue =1, allocationSize=1, schema="seagledb",  name="TimelineSequenceGenerator", sequenceName = "TimelineSequence")
  
   @Column(name = "id")
   private Long id;

   /**
    * The time when the action was performed.
    * <p>
    */
   @Column(name = "timestamp")
   @Temporal(TemporalType.TIMESTAMP)
   private Date timestamp;

   /**
    * The project on which the action was performed.
    * <p>
    */
   @JoinColumn(name = "project_id", referencedColumnName = "id")
   @ManyToOne()
   private Project project;

   /**
    * The type of this action.
    * <p>
    */
   @JoinColumn(name = "action_id", referencedColumnName = "id")
   @ManyToOne(optional = false)
   private ActionType actionType;

   /**
    * Create an empty instance.
    * <p>
    */
   public ProjectTimeline() {
   }


   /**
    * @return the id of this project
    */
   public Long getId() {
      return id;
   }

   /**
    * @return the time when this action was performed.
    */
   public Date getTimestamp() {
      return timestamp;
   }

   /**
    * Set the time when this action was performed.
    * <p>
    * 
    * @param timestamp
    *           the time of this action
    */
   public void setTimestamp(Date timestamp) {
      this.timestamp = timestamp;
   }

   /**
    * @return the project this action was performed on.
    */
   public Project getProject() {
      return project;
   }

   /**
    * Set the project this action was performed on.
    * <p>
    * 
    * @param project
    *           of this action
    */
   public void setProject(Project project) {
      this.project = project;
   }

   /**
    * @return the action type
    */
   public ActionType getActionType() {
      return actionType;
   }

   /**
    * Set the action type.
    * <p>
    * 
    * @param actionType
    *           type of this action
    */
   public void setActionType(ActionType actionType) {
      this.actionType = actionType;
   }

   @Override
   public int hashCode() {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object) {
      // TODO: Warning - this method won't work in the case the id fields are
      // not set
      if (!(object instanceof ProjectTimeline)) {
         return false;
      }
      ProjectTimeline other = (ProjectTimeline) object;
      if ((this.id == null && other.id != null)
            || (this.id != null && !this.id.equals(other.id))) {
         return false;
      }
      return true;
   }

   @Override
   public String toString() {
      return "gr.uom.java.seagle.db.persistence.v2.ProjectTimeline[ id=" + id
            + " ]";
   }

}
