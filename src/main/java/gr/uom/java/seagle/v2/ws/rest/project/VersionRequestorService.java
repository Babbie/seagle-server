package gr.uom.java.seagle.v2.ws.rest.project;

import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.java.seagle.v2.db.persistence.controllers.VersionFacade;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.java.seagle.v2.ws.rest.AbstractRestService;
import gr.uom.java.seagle.v2.ws.rest.project.model.RESTVersion;
import gr.uom.java.seagle.v2.ws.rest.project.model.RESTVersionCollection;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Theodore Chaikalis
 */
@Path("/project/versions")
@Stateless
public class VersionRequestorService extends AbstractRestService implements
      VersionRequestor {

   @EJB
   VersionFacade versionFacade;

   @EJB
   ProjectManager projectManager;

   /**
    * A default value for purl query parameter.
    * <p>
    * If purl is not specified we should return a BAD_REQUEST response.
    */
   private static final String DEFAULT_PURL = "NO_URL";

   @GET
   @Produces(MediaType.APPLICATION_JSON)
   @Override
   public Response getVersionsWS(
         @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String pUrl) {
      // If the client didn't specified a repository url we should
      // return a 400 (Bad Request) code to inform it
      if (pUrl.equals(DEFAULT_PURL)) {
         String errMsg = getErrorResponse("'purl' parameter must be specified");
         illegalRequest(errMsg);
      }

      RESTVersionCollection list = getVersions(pUrl);
      // Check if versions list is null
      // if so that means the repository was not found on
      // this server and we must return a response with
      // code 404
      if (list == null) {
         String errMsg = getErrorResponse(pUrl)
               + " was not found on this server.";
         notFoundException(errMsg);
      }

      // We found versions here, we must return the contents to
      // the client
      return Response.ok(list).build();
   }

   private static String getErrorResponse(String pUrl) {
      return "Required repository: " + pUrl;
   }

   @Override
   public RESTVersionCollection getVersions(String pUrl) {

      // First check if the version exists
      Project project = projectManager.findByUrl(pUrl);
      if (project == null) {
         projectManager.clone(pUrl);
         project = projectManager.findByUrl(pUrl);
      }

      RESTVersionCollection versions = new RESTVersionCollection();
      for (Version v : versionFacade.findByProject(project)) {
         versions.add(new RESTVersion(v));
      }

      return versions;
   }
}
