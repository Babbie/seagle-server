package gr.uom.java.seagle.v2.scheduler;

import gr.uom.java.seagle.v2.AbstractSeagleResolver;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.se.util.concurrent.Task;
import gr.uom.se.util.concurrent.TaskType;
import gr.uom.se.util.validation.ArgsCheck;

/**
 * This task is used to schedule background work on a managed executor service.
 * After the creation of this task the caller must call schedule() in order
 * to schedule this task.
 * 
 * @author elvis_000
 */
public abstract class AbstractBackgroundTask extends AbstractSeagleResolver
      implements Task {

   private final TaskType type;
   
   public AbstractBackgroundTask(SeagleManager seagleManager, String taskId) {
      super(seagleManager);
      ArgsCheck.notEmpty(taskId, "taskId");
      SeagleTaskSchedulerManager scheduler = this.resolveComponentOrManager(SeagleTaskSchedulerManager.class);
      type = scheduler.getType(taskId);
      ArgsCheck.notNull("taskType", type);
   }
   
   public AbstractBackgroundTask(SeagleManager seagleManager, TaskType type) {
      super(seagleManager);
      ArgsCheck.notNull("taskType", type);
      SeagleTaskSchedulerManager scheduler = this.resolveComponentOrManager(SeagleTaskSchedulerManager.class);
      if(!scheduler.canSchedule(type)) {
         throw new IllegalArgumentException("can not schedule task of type " + type);
      }
      this.type = type;
   }

   @Override
   public TaskType getType() {
      return type;
   }

   public void schedule() {
      SeagleTaskSchedulerManager scheduler = this.resolveComponentOrManager(SeagleTaskSchedulerManager.class);
      scheduler.schedule(this);
   }
}
