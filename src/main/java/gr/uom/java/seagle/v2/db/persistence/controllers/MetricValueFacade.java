/**
 * 
 */
package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.MetricValue;
import gr.uom.se.util.validation.ArgsCheck;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Elvis Ligu
 */
@Stateless
public class MetricValueFacade extends AbstractFacade<MetricValue> {

   /**
    * Injected entity manager for the persistence context named {@code seaglePU}
    * .
    * <p>
    */
   @PersistenceContext(unitName = "seaglePU")
   private EntityManager em;

   /**
    * @param entityClass
    */
   public MetricValueFacade() {
      super(MetricValue.class);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected EntityManager getEntityManager() {
      return em;
   }

   /**
    * Return all metric values for a given metric.
    * <p>
    * 
    * @param metric
    *           to get the values for
    * @return all computed values of the given metric
    */
   public List<MetricValue> findByMetric(Metric metric) {
      ArgsCheck.notNull("metric", metric);
      String query = "MetricValue.findByMetric";
      String param = "metric";
      Object val = metric;
      return JPAUtils.namedQueryEntityOneParam(em, MetricValue.class, query,
            param, val);
   }
}
