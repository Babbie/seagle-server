/**
 * 
 */
package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.project.nameResolver.RepositoryInfoResolverFactory;
import gr.uom.java.seagle.v2.project.nameResolver.RepositoryType;
import gr.uom.se.util.module.ModuleConstants;
import gr.uom.se.util.module.annotations.NULLVal;
import gr.uom.se.util.module.annotations.Property;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.util.validation.ArgsCheck;
import gr.uom.se.vcs.VCSRepository;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A factory that will register all repository providers into one place.
 * <p>
 * This factory should be used as a manager, that is it should be a singleton
 * instance in order to register all repository providers into one place. To
 * take an instance of the repository factory use the seagle manager:
 * 
 * <pre>
 * RepositoryProviderFactory factory = seagleManager
 *       .getManager(RepositoryProviderFactory.class);
 * // Add a new repository provider
 * factory.addProvider(svnProvider);
 * // Load the repository for the remote url
 * VCSRepository repo = factory.getRepositoryForRemoteUrl(remoteUrl);
 * </pre>
 * 
 * Clients <strong>SHOULD NOT</strong> create instances of this factory. This
 * factory is managed by system, and in order for clients to retrieve the
 * (singleton) instance of this factory they should call seagle manager.
 * 
 * @author Elvis Ligu
 */
public class RepositoryProviderFactory implements RepositoryProvider {

   private final ConcurrentHashMap<RepositoryType, RepositoryProvider> providers = new ConcurrentHashMap<>();

   /**
    * DO NOT USE this constructor to create instances of this factory, instead
    * use seagle managaer, because this factory is registered as a manager.
    */
   @ProvideModule
   public RepositoryProviderFactory(
         @Property(name = NULLVal.NO_PROP) ProjectManager repoManager) {
      // Register the known providers
      this.addProvider(new GitRepositoryProvider(repoManager));
   }

   /**
    * Add a provider to this factory.
    * <p>
    * Note this provider may override other providers if this supports
    * repository types that are previously mapped.
    * 
    * @param provider
    *           must not be null not or supports no types.
    */
   public void addProvider(RepositoryProvider provider) {
      ArgsCheck.notNull("provider", provider);
      Set<RepositoryType> types = provider.getSupportedTypes();
      ArgsCheck.notEmpty("types", types);

      for (RepositoryType type : types) {
         ArgsCheck.notNull("type", type);
         providers.put(type, provider);
      }
   }

   /**
    * Remove the provider for the given repository type.
    * 
    * @param type
    *           the type of the repository to remove the provider for.
    */
   public void removeProvider(RepositoryType type) {
      ArgsCheck.notNull("type", type);
      providers.remove(type);
   }

   /**
    * {@inheritDoc}
    * <p>
    * When used in conjunction with modules API the parameter provider should
    * provide the appropriate remoteUrl value in order to load the repository.
    * However because the remoteUrl property is set to be required from the
    * default property place (domain name
    * {@link ModuleConstants#DEFAULT_PROPERTY_ANNOTATION_DOMAIN}) that means
    * that there should be a specific context which provides this property. To
    * obtain a context (let say for a given project) the client may use:
    * 
    * <pre>
    * ContextManager ctxm = seagleManager.getManager(ContextManager.class);
    * ProjectContext ctx = ctxm.lookupContext(project, ProjectContext.class);
    * VCSRepository repo = ctx.load(VCSRepository.class);
    * </pre>
    */
   @ProvideModule
   @Override
   public VCSRepository getRepositoryForRemoteUrl(
         @Property(name = "remoteUrl") String remoteUrl) {
      RepositoryType type = RepositoryInfoResolverFactory.getInstance()
            .tryResolveType(remoteUrl);
      if (type == null) {
         throw new UnsupportedOperationException(
               "repository type can not be resolved from " + remoteUrl);
      }
      RepositoryProvider provider = providers.get(type);
      if (provider == null) {
         throw new UnsupportedOperationException("no provider was found for "
               + remoteUrl);
      }
      return provider.getRepositoryForRemoteUrl(remoteUrl);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Set<RepositoryType> getSupportedTypes() {
      return new HashSet<>(providers.keySet());
   }
}
