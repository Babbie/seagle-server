/**
 * 
 */
package gr.uom.java.seagle.v2.metric.repo;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.metric.AbstractMetricActivator;
import gr.uom.java.seagle.v2.metric.LanguageAndMetricCategoryActivator;
import gr.uom.java.seagle.v2.metric.MetricManager;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;
import gr.uom.java.seagle.v2.metric.imp.MetricEventListener;
import gr.uom.java.seagle.v2.metric.imp.MetricFactory;
import gr.uom.se.util.module.annotations.NULLVal;
import gr.uom.se.util.module.annotations.Property;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.util.validation.ArgsCheck;

/**
 * Activate all metrics of {@link RepoMetricEnum}.
 * <p>
 * This activator should be created by seagle manager, and should be activated
 * by activator manager. In order to tell seagle to activate this you should
 * specify the 'activator.' property in managers configuration domain. The
 * activator should start at the deployment of seagle. This activator has a
 * dependency on {@linkplain LanguageAndMetricCategoryActivator languages
 * activator}.
 * 
 * @author Elvis Ligu
 */
public class RepoMetricActivator extends AbstractMetricActivator {

   private final EventManager eventManager;

   /**
    * This activator instance should normally be created by seagle, and the
    * dependency should be resolved by seagle too.
    */
   @ProvideModule
   public RepoMetricActivator(
         @Property(name = NULLVal.NO_PROP) SeagleManager seagleManager,
         @Property(name = NULLVal.NO_PROP) EventManager eventManager,
         @Property(name = NULLVal.NO_PROP) MetricManager metricManager) {
      super(seagleManager, metricManager);
      ArgsCheck.notNull("eventManager", eventManager);
      this.eventManager = eventManager;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected void activate() {
      activateMetrics();
      activateListeners();
   }

   /**
    * Activate the metrics specified at {@link RepoMetricEnum}.
    */
   private void activateMetrics() {
      MetricManager manager = getMetricManager();
      for (ExecutableMetric m : RepoMetricEnum.values()) {
         // Activate metric
         manager.activateMetric(m.getCategory(), m.getMnemonic(), m.getName(),
               m.getDescription());
         // Add the languages to metric
         manager.addMetricLanguage(m.getMnemonic(), m.getLanguages());
      }
   }

   /**
    * Activate repo metric listener and project property updater.
    */
   private void activateListeners() {
      MetricEventListener repoListener = new RepoMetricListener(seagleManager,
            eventManager);
      repoListener.register();
      ProjectBranchPropertyUpdater projectUpdater = new ProjectBranchPropertyUpdater(
            seagleManager, eventManager);
      projectUpdater.register();
      registerMetricHandlers(repoListener);
   }

   /**
    * Register metric handler to metric factory.
    */
   private void registerMetricHandlers(MetricEventListener listener) {
      // Register a factory for the repository metrics
      // Metric listener that will execute the metrics will be querying
      // the metrics it support using this factory.
      MetricFactory factory = MetricFactory.getInstance(listener);

      // Set the handler for commit counter
      factory.registerHandler(RepoMetricEnum.COM_PROJ,
            new CounterMetricHandler(seagleManager));

      // Set the handler for version metrics
      VersionMetricHandler vhandler = new VersionMetricHandler(seagleManager);

      // Register the metrics now

      // Number of authors per version
      factory.registerHandler(RepoMetricEnum.AUTHOR_COUNT_VER, vhandler);

      // Number of authors per version
      factory.registerHandler(RepoMetricEnum.COMMITER_COUNT_VER, vhandler);

      // Number of commits per version
      factory.registerHandler(RepoMetricEnum.COM_COUNT_VER, vhandler);

      // Register added files counter
      factory.registerHandler(RepoMetricEnum.ADD_FILE_COUNT_VER, vhandler);

      // Register deleted files counter
      factory.registerHandler(RepoMetricEnum.DEL_FILE_COUNT_VER, vhandler);

      // Register modified files counter
      factory.registerHandler(RepoMetricEnum.MOD_FILE_COUNT_VER, vhandler);

      // Register new lines per version counter
      factory.registerHandler(RepoMetricEnum.ADD_LINE_COUNT_VER, vhandler);

      // Register new lines per version counter
      factory.registerHandler(RepoMetricEnum.DEL_LINE_COUNT_VER, vhandler);
   }
}
