package gr.uom.java.seagle.v2.ws.rest.project;

import javax.ejb.Local;
import javax.ws.rs.core.Response;

/**
 *
 * @author elvis
 */
@Local
public interface IProjectService {
   
   Response create(String purl);
   
   Response updateByUrl(String purl);
   
   Response updateByName(String name);
   
   Response getProjectByUrl(String purl);

   Response getProjectByName(String name);

    public Response deleteByName(String name);

    public Response deleteByUrl(String purl);

}
