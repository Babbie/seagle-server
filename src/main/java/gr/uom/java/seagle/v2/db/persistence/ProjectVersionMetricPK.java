package gr.uom.java.seagle.v2.db.persistence;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 * An embedded entity used as a primary key for {@link ProjectVersionMetric}.
 * <p>
 * @author Elvis Ligu
 */
@Embeddable
public class ProjectVersionMetricPK implements Serializable {

   private static final long serialVersionUID = 4955902433672158127L;

   private Long metricValue;

   private Long version;

   public ProjectVersionMetricPK() {
   }

   public Long getMetricValue() {
      return metricValue;
   }

   public Long getVersion() {
      return version;
   }

   @Override
   public int hashCode() {
      int hash = 0;
      hash += metricValue.hashCode();
      hash += version.hashCode();
      return hash;
   }

   @Override
   public boolean equals(Object object) {
      // TODO: Warning - this method won't work in the case the id fields are not set
      if (!(object instanceof ProjectVersionMetricPK)) {
         return false;
      }
      ProjectVersionMetricPK other = (ProjectVersionMetricPK) object;
      return (this.metricValue.equals(other.metricValue) && this.version.equals(other.version)) ;
   }

   @Override
   public String toString() {
      return "gr.uom.java.seagle.db.persistence.v2.ProjectVersionMetricPK[ metricValue=" + metricValue+ ", versionId=" + version + " ]";
   }

}
