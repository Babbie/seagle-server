/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.analysis.findbugs.data;

/**
 *
 * @author Samet
 */
public class FBBugInstanceMethod 
{
    private final String methodName;
    private final int bugStartLine;
    private final int bugEndLine;
    
    public FBBugInstanceMethod(String methodName, int bugStartLine, int bugEndLine) 
    {
        this.methodName = methodName;
        this.bugStartLine = bugStartLine;
        this.bugEndLine = bugEndLine;
    }
    
    public String getMethodName()
    {
        return this.methodName;
    }
    
    public int getBugStartLine()
    {
        return this.bugStartLine;
    }
    
    public int getBugEndLine()
    {
        return this.bugEndLine;
    }    
}
