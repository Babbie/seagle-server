package gr.uom.java.seagle.v2.ws.rest.metric;

import java.util.List;

import javax.ejb.Local;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

/**
 *
 * @author elvis
 */
@Local
public interface IMetricService {

   Response registerMetricToProjectByName(String name, String mnemonic);

   Response registerMetricToProjectByUrl(String purl, String mnemonic);

   Response registerMetricToProjectByName(String name, List<String> metrics);

   Response registerMetricToProjectByUrl(String purl, List<String> metrics);

   Response removeMetricFromProjectByBame(String name, String mnemonic);

   Response removeMetricFromProjectByUrl(String purl, String mnemonic);

   Response removeMetricFromProjectByBame(String name, List<String> metrics);

   Response removeMetricFromProjectByUrl(String purl, List<String> metrics);

   Response getProjectMetricsByName(String name);

   Response getProjectMetricsByUrl(String url);

   Response getMetricCategories(String metricMnemonic);

   Response getMetricLanguages(String metricMnemonic);

   Response getMetrics();

   Response getCategories();

   Response getLanguages();

   Response getProjectMetricValuesByName(String name);

   Response getProjectMetricValuesByUrl(String name);

   Response getProjectSpecificMetricValuesByUrl(String purl, String metricMnemonic);
   
   Response getProjectSpecificMetricValuesForSpecificVersionByUrl(String purl, String metricMnemonic, String versionHash);
   
   Response getProjectSpecificMetricValuesForSpecificClassByUrl(String purl, String metricMnemonic, String className);
   
   Response getProjectClassesByUrl( String purl, String versionID);
   
   Response getAllMetricValuesForSpecificClass(String metricMnemonic,String className) ;
   
   Response getProjectMetricEvolution(String metricMnemonic, String projectUrl);

    public Response removeMetricsOfCategoryFromProjectByUrl(String purl, String category);
}
