package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.se.util.validation.ArgsCheck;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

/**
 * Base class for all entity services.
 * <p>
 *
 * It provides the basics of CRUD operations. This class doesn't support
 * transaction, thus sublcasses should provide transactions (i.e. they should be
 * EJBs managed by a container) or override each method in order to manage the
 * transactions.
 *
 * @author Elvis Ligu
 */
public abstract class AbstractFacade<T> {

    /**
     * The type of entity this object should be using.
     * <p>
     */
    private final Class<T> entityClass;

    /**
     * Create a new instance given the type of the entity this instance should
     * be using.
     * <p>
     * @param entityClass the type of entity, must not be null
     */
    public AbstractFacade(Class<T> entityClass) {
        ArgsCheck.notNull("entityClass", entityClass);
        this.entityClass = entityClass;
    }

    /**
     * To be implemented by sublcasses providing the entity manager.
     * <p>
     *
     * @return the entity manager
     */
    protected abstract EntityManager getEntityManager();

    /**
     * Insert the entity into db.
     * <p>
     *
     * @param entity to be inserted. Must not be null and must not violate db
     * constraints, otherwise exception will be thrown.
     */
    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    /**
     * Insert the entity into db.
     * <p>
     *
     * @param entities to be inserted. Must not be null and must not violate db
     * constraints, otherwise exception will be thrown.
     */
    public void create(final List<T> entities) {
        for (T t : entities) {
            getEntityManager().persist(t);
        }
    }

    /**
     * Update the given entity.
     * <p>
     * The entity's primary key should not be null, also it must be a managed
     * entity.
     *
     * @param entity to be updated
     */
    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    public void edit(final List<T> entities) {
        for (T t : entities) {
            getEntityManager().merge(t);
        }
    }

    /**
     * Delete the given entity from db.
     * <p>
     * The primary key must not be null, and it should be a managed entity.
     *
     * @param entity to be removed
     */
    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    /**
     * Find entity with the given id (primary key).
     * <p>
     *
     * @param id of entity to look for
     * @return the entity with the given id or a null if no entity is found.
     */
    public T findById(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    /**
     * List all the entities within db.
     * <p>
     * This might be a very resource consuming operation. Use it wisely.
     *
     * @return all the entities within the db.
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<T> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    /**
     * List all the entities within a given range.
     * <p>
     * The first value of range array is the start position of the entities
     * after executing a SELECT query, and the second value is the end. Range
     * bounds are inclusive.
     *
     * @param range array of values bounding the range of entities to retrieve.
     *
     * @return the entities within the given range
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<T> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    /**
     * Count the number of entities in db.
     * <p>
     *
     * @return the number of entities.
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    /**
     * A delegate to entity's manager {@code flush} method.
     * <p>
     * Used in situation where a flush is required.
     */
    public void flushChanges() {
        getEntityManager().flush();

    }

    /**
     * A delegate to entity's manager {@code contains} method.
     * <p>
     * Used to check if the given entity is managed by this manager.
     * <p>
     * <b> This method should not be used to determine if the given entity is
     * contained within the DB or not. Use {@link #findById(java.lang.Object)}
     * for that purpose.</b>
     *
     * @param entity to check if it is a managed entity
     * @return true if the given entity is a managed one
     */
    public boolean contains(T entity) {
        return getEntityManager().contains(entity);
    }

    public void clearCache(T entity, Object key) {
        getEntityManager().getEntityManagerFactory().getCache().evict(entityClass, key);
    }

    public T refresh(T entity, Object key) {
        clearCache(entity, key);
        return this.findById(key);
    }
}
