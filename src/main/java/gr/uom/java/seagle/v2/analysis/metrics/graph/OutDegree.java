
package gr.uom.java.seagle.v2.analysis.metrics.graph;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.graph.AbstractEdge;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.SoftwareGraph;
import gr.uom.java.seagle.v2.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Theodore Chaikalis
 */
public class OutDegree extends AbstractGraphExecutableMetric {
    
    public static final String MNEMONIC = "OUT_DEGREE";

    public OutDegree(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(SoftwareProject softwareProject) {
         Map<String, Double> valuePerClass = new LinkedHashMap<>();
         SoftwareGraph<AbstractNode, AbstractEdge> softwareGraph = softwareProject.getProjectGraph();
         for (AbstractNode node : softwareGraph.getVertices()) {
            int outDegree = softwareGraph.outDegree(node);
            valuePerClass.put(node.getName(), (double)outDegree);
        }    
        storeValuesForAllNodesInMemory(getMnemonic(), valuePerClass, softwareProject);
        storeProjectLevelAggregationMetricInMemory(getMnemonic(), softwareProject, valuePerClass, MetricAggregationStrategy.Average);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
         return "Out Degree of each node";
    }

    @Override
    public String getName() {
         return "Out Degree";
    }
    
}
