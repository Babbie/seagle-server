package gr.uom.java.seagle.v2.project.build;

import gr.uom.java.seagle.v2.SeagleManager;
import org.gradle.tooling.BuildLauncher;
import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ProjectConnection;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel Feitosa
 */
public class GradleBuilder implements ProjectBuilder {

    /**
     * Folder of the project to be built
     */
    private final String projectFolder;
    private final SeagleManager seagleManager;
    private final Logger logger;
    
    /**
     * Version of Gradle used for building the current project
     */
    private String gradleVersion;

    protected GradleBuilder(SeagleManager seagleManager, String projectFolder){
        this.seagleManager = seagleManager;
        this.projectFolder = projectFolder + File.separator;
        this.logger = Logger.getLogger(GradleBuilder.class.getName());
        // Verify necessary version (it may not be necessary)
        // Verify if it is available

        if(isAvailable()) {
            System.out.println("Gradle version " + gradleVersion + " is available.");
        }
        else {
            System.out.println("Gradle is not available.");
        }

        // Gradle version is set in isAvailable() method.
        // Set necessary properties
    }
    
    @Override
    public String getName() {
        return "Gradle";
    }

    @Override
    public String getVersion() {
        return gradleVersion;
    }
    
    protected static boolean canBuild(String folder) {
        //If build.gradle file exists in the directory "folder", it can be built by Gradle.
        
        File buildGradleFile = new File(folder + File.separator + "build.gradle");
        
        return buildGradleFile.exists() && !buildGradleFile.isDirectory();
    }
    
    @Override
    public boolean canBuild() {
        return GradleBuilder.canBuild(projectFolder);
    }

    @Override
    public boolean isAvailable() {
        // Gradle is always available, the code will automatically download.
        return true;
    }

    @Override
    public boolean tryToBuild() {
        ProjectConnection connection = GradleConnector.newConnector()
                .forProjectDirectory(new File(projectFolder)).connect();
        
        BuildLauncher build = connection.newBuild();

        build.setJavaHome(new File(seagleManager.getSeaglePathConfig().getJavaHome()));
        build.forTasks("clean", "build");

        // Get output for logging
        OutputStream logStream = new ByteArrayOutputStream();
        build.setStandardOutput(logStream);
        build.setStandardError(logStream);
        
        try
        {
            build.run();
        }
        catch (Exception e) 
        {
            logger.log(Level.INFO, logStream.toString());

            try (PrintWriter logFile = new PrintWriter(new FileWriter(getPathToBuildLog(), true)))
            {
                logFile.print(logStream.toString());
            }
            catch (IOException e1)
            {
                logger.log(Level.SEVERE, "Could not write build log");
                logger.log(Level.SEVERE, e1.getLocalizedMessage());
            }

            logger.log(Level.INFO, "Gradle cannot compile the project in the folder " + projectFolder);
            logger.log(Level.INFO, e.getLocalizedMessage());
            return false;
        }

        logger.log(Level.INFO, logStream.toString());

        try (PrintWriter logFile = new PrintWriter(new FileWriter(getPathToBuildLog(), true)))
        {
            logFile.print(logStream.toString());
        }
        catch (IOException e)
        {
            logger.log(Level.SEVERE, "Could not write build log");
            logger.log(Level.SEVERE, e.getLocalizedMessage());
        }
        return true;
    }

    @Override
    public String getPathToBuildLog() {
        return projectFolder + seagleManager.getSeaglePathConfig().getBuildLogName();
    }

    @Override
    public String getPathClassFiles() {
        return this.projectFolder;
    }
    
    
}
