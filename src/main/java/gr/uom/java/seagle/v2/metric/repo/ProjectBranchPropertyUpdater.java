/**
 * 
 */
package gr.uom.java.seagle.v2.metric.repo;

import gr.uom.java.seagle.v2.AbstractSeagleListener;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.Property;
import gr.uom.java.seagle.v2.db.persistence.controllers.PropertyFacade;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.project.ProjectEventType;
import gr.uom.java.seagle.v2.project.ProjectInfo;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventType;
import gr.uom.se.vcs.VCSBranch;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * This listener will listen to DB project events such as insert, remove update,
 * and will create branch properties to db that are needed for metrics to
 * calculate.
 * <p>
 * Eac time a project is inserted to DB an event
 * {@linkplain ProjectEventType#DB_INSERTED DB_INSERTED} is sent. When this
 * event is received from this listener, this will register into properties for
 * each branch of the project a property {domain:url, name:BRANCH.bid,
 * value:headid}. Where url is the project url, bid is the branch id, and headid
 * is the commit id of the head of this branch. In order for metrics to decide
 * whether a project is updated they should look at these properties to check if
 * a branch is updated or not.
 * 
 * @author Elvis Ligu
 */
public class ProjectBranchPropertyUpdater extends AbstractSeagleListener {

   public static final String BRANCH_SUFFIX = "BRANCH";
   
   /**
    * Create an instance based on its dependencies.
    * <p>
    * 
    * @param seagleManager
    * @param eventManager
    */
   public ProjectBranchPropertyUpdater(SeagleManager seagleManager,
         EventManager eventManager) {
      super(seagleManager, eventManager);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void register() {
      super.register();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected void acceptEvent(EventType type, EventInfo info) {
      ProjectInfo pinfo = (ProjectInfo) info.getDescription();
      ProjectEventType pType = (ProjectEventType) type;

      switch (pType) {
      case DB_INSERTED:
         createBranchProperties(pinfo);
         break;
      case DB_UPDATED:
         updateBranchProperties(pinfo);
         break;
      case DB_REMOVED:
         deleteBranchProperties(pinfo);
         break;
      default:
         break;
      }
   }

   /**
    * @param pinfo
    */
   private void deleteBranchProperties(ProjectInfo pinfo) {
      String url = pinfo.getRemoteUrl();
      PropertyFacade properties = resolveComponentOrManager(PropertyFacade.class);
      properties.deleteDomain(url);
   }

   /**
    * @param pinfo
    */
   private void updateBranchProperties(ProjectInfo pinfo) {
      ProjectManager projects = resolveComponentOrManager(ProjectManager.class);
      String url = pinfo.getRemoteUrl();
      PropertyFacade properties = resolveComponentOrManager(PropertyFacade.class);

      try (VCSRepository repo = projects.getRepository(url)) {
         Collection<VCSBranch> branches = repo.getBranches();
         for (VCSBranch branch : branches) {
            String bid = branch.getID();
            String value = branch.getHead().getID();
            // Update the property for the given project
            // The property should be into this db
            Property property = getBranchProperty(properties, url, bid);
            if (property == null) {
               // should create the property as it seems the
               // branch is new
               String domain = url;
               String name = getBranchPropertyName(bid);
               properties.create(domain, name, value);
            } else {
               String oldValue = property.getValue();
               if (!value.equals(oldValue)) {
                  // should update the branch property as it seems
                  // the branch has been updated
                  property.setValue(value);
                  properties.edit(property);
               }
            }
         }

      } catch (VCSRepositoryException e) {
         throw new RuntimeException(e);
      }
   }

   /**
    * @param pinfo
    */
   private void createBranchProperties(ProjectInfo pinfo) {
      ProjectManager projects = resolveComponentOrManager(ProjectManager.class);
      String url = pinfo.getRemoteUrl();
      PropertyFacade properties = resolveComponentOrManager(PropertyFacade.class);

      try (VCSRepository repo = projects.getRepository(url)) {
         Collection<VCSBranch> branches = repo.getBranches();
         for (VCSBranch branch : branches) {
            String bid = branch.getID();
            String domain = url;
            String name = getBranchPropertyName(bid);
            String value = branch.getHead().getID();
            // Create the property for the given project
            // The property should not be into this db
            properties.create(domain, name, value);
         }

      } catch (VCSRepositoryException e) {
         throw new RuntimeException(e);
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Set<EventType> getTypes() {
      Set<EventType> types = new HashSet<>(2);
      types.add(ProjectEventType.DB_INSERTED);
      types.add(ProjectEventType.DB_REMOVED);
      types.add(ProjectEventType.DB_UPDATED);
      return types;
   }

   public static Property getBranchProperty(PropertyFacade properties,
         String url, String bid) {
      String domain = url;
      String name = getBranchPropertyName(bid);
      return properties.getPropertyByName(domain, name);
   }

   public static String getBranchPropertyName(String bid) {
      return BRANCH_SUFFIX + "." + bid;
   }

   public static String extractBid(String bName) {
      return bName.substring(BRANCH_SUFFIX.length() + 1);
   }

   public static Collection<Property> getBranchProperties(
         PropertyFacade properties, String url) {
      String domain = url;
      String prefix = BRANCH_SUFFIX;
      return properties.getDomainByPrefix(domain, prefix);
   }
}
