package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.BadSmell;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Theodore Chaikalis
 */
@Stateless
public class BadSmellFacade extends AbstractFacade<BadSmell> {

   /**
    * Injected entity manager for the persistence context named
    * {@code seaglePU}.
    * <p>
    */
   @PersistenceContext(unitName = "seaglePU")
   private EntityManager em;

   public BadSmellFacade() {
      super(BadSmell.class);
   }


   @Override
   protected EntityManager getEntityManager() {
      return em;
   }
   
    public List<BadSmell> findByName(String name) {
         String query = "BadSmell.findByName";
         return JPAUtils.namedQueryEntityOneParam(em, BadSmell.class, query, "name",name);
    }
    
}
