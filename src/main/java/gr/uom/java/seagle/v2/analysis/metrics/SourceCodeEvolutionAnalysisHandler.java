
package gr.uom.java.seagle.v2.analysis.metrics;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.metric.MetricRunInfo;
import gr.uom.java.seagle.v2.metric.imp.AbstractMetricHandler;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author Theodore Chaikalis
 */
public class SourceCodeEvolutionAnalysisHandler extends AbstractMetricHandler {

    
    
    public SourceCodeEvolutionAnalysisHandler(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public Runnable getRunner(List<ExecutableMetric> metrics, MetricRunInfo info) {
        return new SourceCodeEvolutionAnalysisMetricTask(seagleManager, info, null, new HashSet(metrics));
    }

    @Override
    protected Collection<ExecutableMetric> computeMetrics(MetricRunInfo info, List<ExecutableMetric> metrics) {
        return metrics;
    }

}
