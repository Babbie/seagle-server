package gr.uom.java.seagle.v2.db.persistence;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Represents a computed value for a given metric.
 * <p>
 * In order to allow for extendibility the system specifies a generic value
 * entity that can be used for all kind of metrics. Each value is related to a
 * metric and to an entity that it was computed for. For example a metric that
 * measures the complexity of a given project is computed for a given project
 * version. The computed value is an instance of this type and contains the
 * project version, for which it was computed.
 * <p>
 * Although, some metrics have integer values, all the metrics have a type of
 * double, to support the widest possible range.
 *
 * @author Elvis Ligu
 */
@Entity
@Table(name = "metric_value")
@XmlRootElement
@NamedQueries({
      @NamedQuery(name = "MetricValue.findAll", query = "SELECT m FROM MetricValue m"),
      @NamedQuery(name = "MetricValue.findById", query = "SELECT m FROM MetricValue m WHERE m.id = :id"),
      @NamedQuery(name = "MetricValue.findByValue", query = "SELECT m FROM MetricValue m WHERE m.mvalue = :value"),
      @NamedQuery(name = "MetricValue.findByMetric", query = "SELECT m FROM MetricValue m WHERE m.metric = :metric")})
public class MetricValue implements Serializable {

   private static final long serialVersionUID = 1L;

   /**
    * The id of this value.
    * <p>
    */
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id")
   private Long id;

   /**
    * The computed value.
    * <p>
    */
   @Basic(optional = false)
   @NotNull
   @Column(name = "value")
   private double mvalue;

   /**
    * The node (the class) to whom this value is related.
    * <p>
    * Optional if this value was computed on another entity.
    */
   @OneToOne(cascade = CascadeType.ALL, mappedBy = "metricValue")
   private NodeVersionMetric nodeVersionMetric;

   /**
    * The metric type of this value.
    * <p>
    */
   @NotNull
   @JoinColumn(name = "metric_id", referencedColumnName = "id")
   @ManyToOne(cascade = CascadeType.PERSIST, optional = false)
   private Metric metric;

   /**
    * The project version this value was computed for.
    * <p>
    */
   @OneToOne(cascade = CascadeType.ALL, mappedBy = "metricValue")
   private ProjectVersionMetric projectVersionMetric;

   /**
    * The projects this value was computed for.
    * <p>
    */
   @OneToOne(cascade = CascadeType.ALL, mappedBy = "metricValue")
   private ProjectMetric projectMetric;

   /**
    * Create an empty instance.
    * <p>
    */
   public MetricValue() {
   }

   /**
    * Create an instance given its id.
    * <p>
    * 
    * @param id
    *           of this instance. Auto-generated value.
    */
   public MetricValue(Long id) {
      this.id = id;
   }

   /**
    * @return the id of this value
    */
   public Long getId() {
      return id;
   }

   /**
    * @return value of this entity
    */
   public double getValue() {
      return mvalue;
   }

   /**
    * Set the value of this entity.
    * <p>
    * 
    * @param value
    *           of this entity
    */
   public void setValue(double value) {
      this.mvalue = value;
   }

   /**
    * Get the node of this value.
    * <p>
    * The node is an entity within system representing a class/module/source
    * file within a project. A node is always under a given version, and its
    * state is specified by its version (see VCS). If this is a value computed
    * for a given node then this method should return a non null object. I any
    * other case will return null.
    *
    * @return the node this value is computed for
    */
   public NodeVersionMetric getNodeVersionMetric() {
      return nodeVersionMetric;
   }

   /**
    * Set the node of this value.
    * <p>
    * The node is an entity within system representing a class/module/source
    * file within a project. A node is always under a given version, and its
    * state is specified by its version (see VCS). If this value is computed for
    * a given node, then use this method to link the node with this value.
    *
    * @param nodeVersionMetric
    *           the node vthis value is computed for
    */
   public void setNodeVersionMetric(NodeVersionMetric nodeVersionMetric) {
      this.nodeVersionMetric = nodeVersionMetric;
   }

   /**
    * @return the type of metric this value is related to
    */
   public Metric getMetric() {
      return metric;
   }

   /**
    * Set the metric type of this value.
    * <p>
    * 
    * @param metric
    *           type of this value
    */
   public void setMetric(Metric metric) {
      this.metric = metric;
   }

   /**
    * Get the project version for which this value was computed.
    * <p>
    * Metrics that are computed on a given project should return a project
    * version. If this metric is not a project metric then this method should
    * return true.
    *
    * @return the project version this value was computed for (if any)
    */
   public ProjectVersionMetric getProjectVersionMetric() {
      return projectVersionMetric;
   }

   /**
    * Set the project version for which this value was computed.
    * <p>
    * Metrics that are computed on a given project should return a project
    * version. If this metric is not a project metric then this method should
    * return true.
    *
    * @param projectVersionMetric
    *           the project version on which this value where computed.
    */
   public void setProjectVersionMetric(ProjectVersionMetric projectVersionMetric) {
      this.projectVersionMetric = projectVersionMetric;
   }

   /**
    * Get the project for which this metric was computed for.
    * <p>
    * 
    * @return
    */
   public ProjectMetric getProjectMetric() {
      return projectMetric;
   }

   /**
    * Set the project for which this metric was computed for.
    * <p>
    * 
    * @param projectMetric
    */
   public void setProjectMetric(ProjectMetric projectMetric) {
      this.projectMetric = projectMetric;
   }

   @Override
   public int hashCode() {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object) {
      // TODO: Warning - this method won't work in the case the id fields are
      // not set
      if (!(object instanceof MetricValue)) {
         return false;
      }
      MetricValue other = (MetricValue) object;
      if ((this.id == null && other.id != null)
            || (this.id != null && !this.id.equals(other.id))) {
         return false;
      }
      return true;
   }

   @Override
   public String toString() {
      return "gr.uom.java.seagle.db.persistence.v2.MetricValue[ id=" + id
            + " ]";
   }

}
