/**
 * 
 */
package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.se.util.validation.ArgsCheck;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * Different JPA utility methods (static) that are used by the facades.
 * <p>
 * 
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
public class JPAUtils {

   /**
    * Executes the given named query with a single parameter.
    * <p>
    * 
    * @param query
    *           to be executed (usually a SELECT).
    * @param param
    *           the name of the parameter in the query
    * @param val
    *           the value of the parameter
    * @return the results
    */
   public static <T> List<T> namedQueryEntityOneParam(EntityManager em,
         Class<T> type, String query, String param, Object val) {

      ArgsCheck.notNull("entity manager", em);
      ArgsCheck.notNull("type", type);
      ArgsCheck.notEmpty("query", query);
      ArgsCheck.notNull("parameter name", param);
      ArgsCheck.notNull(param, val);

      return em.createNamedQuery(query, type).setParameter(param, val)
            .getResultList();
   }

   /**
    * Executes the given named update with a single parameter.
    * <p>
    * 
    * @param query
    *           the update to be executed (a DELETE or UPDATE).
    * @param param
    *           the name of the parameter in the query
    * @param val
    *           the value of the parameter
    */
   public static void namedUpdateOneParam(EntityManager em, String query,
         String param, Object val) {
      ArgsCheck.notNull("entity manager", em);
      ArgsCheck.notEmpty("query", query);
      ArgsCheck.notNull("parameter name", param);
      ArgsCheck.notNull(param, val);

      em.createNamedQuery(query).setParameter(param, val).executeUpdate();
   }

   /**
    * Executes the given named query with a single parameter.
    * <p>
    * 
    * @param query
    *           to be executed (usually a SELECT).
    * @param param
    *           the name of the parameter in the query
    * @param val
    *           the value of the parameter
    * @return the single result
    */
   public static <T> T namedQueryEntityOneParamSingle(EntityManager em,
         Class<T> type, String query, String param, Object val) {

      ArgsCheck.notNull("entity manager", em);
      ArgsCheck.notNull("type", type);
      ArgsCheck.notEmpty("query", query);
      ArgsCheck.notNull("parameter name", param);
      ArgsCheck.notNull(param, val);

      return em.createNamedQuery(query, type).setParameter(param, val)
            .getSingleResult();
   }

   /**
    * Executes the given named query with two parameters.
    * <p>
    * 
    * @param query
    *           to be executed (usually a SELECT)
    * @param param1
    *           the name of first parameter
    * @param val1
    *           the value of first parameter
    * @param param2
    *           the name of second parameter
    * @param val2
    *           the value of second parameter
    * @return the results
    */
   public static <T> List<T> namedQueryEntityTwoParam(EntityManager em,
         Class<T> type, String query, String param1, Object val1,
         String param2, Object val2) {

      ArgsCheck.notNull("entity manager", em);
      ArgsCheck.notNull("type", type);
      ArgsCheck.notEmpty("query", query);
      ArgsCheck.notNull("parameter1 name", param1);
      ArgsCheck.notNull(param1, val1);
      ArgsCheck.notNull("parameter2 name", param2);
      ArgsCheck.notNull(param2, val2);

      return em.createNamedQuery(query, type).setParameter(param1, val1)
            .setParameter(param2, val2).getResultList();
   }

   /**
    * Executes the given named query with two parameters.
    * <p>
    * 
    * @param query
    *           to be executed (usually a SELECT)
    * @param param1
    *           the name of first parameter
    * @param val1
    *           the value of first parameter
    * @param param2
    *           the name of second parameter
    * @param val2
    *           the value of second parameter
    * @return the result
    */
   public static <T> T namedQueryEntityTwoParamSingle(EntityManager em,
         Class<T> type, String query, String param1, Object val1,
         String param2, Object val2) {

      ArgsCheck.notNull("entity manager", em);
      ArgsCheck.notNull("type", type);
      ArgsCheck.notEmpty("query", query);
      ArgsCheck.notNull("parameter1 name", param1);
      ArgsCheck.notNull(param1, val1);
      ArgsCheck.notNull("parameter2 name", param2);
      ArgsCheck.notNull(param2, val2);

      return em.createNamedQuery(query, type).setParameter(param1, val1)
            .setParameter(param2, val2).getSingleResult();
   }

   /**
    * Executes the given named update with a single parameter.
    * <p>
    * 
    * @param query
    *           the update to be executed (a DELETE or UPDATE).
    * @param param1
    *           the name of first parameter
    * @param val1
    *           the value of first parameter
    * @param param2
    *           the name of second parameter
    * @param val2
    *           the value of second parameter
    */
   public static void namedUpdateTwoParam(EntityManager em, String query,
         String param1, Object val1, String param2, Object val2) {
      ArgsCheck.notNull("entity manager", em);
      ArgsCheck.notEmpty("query", query);
      ArgsCheck.notNull("parameter1 name", param1);
      ArgsCheck.notNull(param1, val1);
      ArgsCheck.notNull("parameter2 name", param2);
      ArgsCheck.notNull(param2, val2);

      em.createNamedQuery(query).setParameter(param1, val1)
            .setParameter(param2, val2).executeUpdate();
   }

   /**
    * Executes the given named query with the given parameters.
    * 
    * @param em
    *           the entity manager to create the query against
    * @param type
    *           the type of the returned result
    * @param query
    *           the named query
    * @param params
    *           a map of parameter names and their corresponding values
    * @return a list of results, the result can be a single result
    */
   public static <T> List<T> namedQueryEntity(EntityManager em, Class<T> type,
         String query, Map<String, Object> params) {

      ArgsCheck.notNull("entity manager", em);
      ArgsCheck.notNull("type", type);
      ArgsCheck.notEmpty("query", query);
      ArgsCheck.notNull("params", params);

      TypedQuery<T> typedQuery = em.createNamedQuery(query, type);
      for (String param : params.keySet()) {
         Object val = params.get(param);
         typedQuery.setParameter(param, val);
      }
      return typedQuery.getResultList();
   }

   /**
    * Executes the given named query with the given parameters.
    * 
    * @param em
    *           the entity manager to create the query against
    * @param type
    *           the type of the returned result
    * @param query
    *           the named query
    * @param params
    *           a map of parameter names and their corresponding values
    * @return the result
    */
   public static <T> T namedQueryEntitySingle(EntityManager em, Class<T> type,
         String query, Map<String, Object> params) {

      ArgsCheck.notNull("entity manager", em);
      ArgsCheck.notNull("type", type);
      ArgsCheck.notEmpty("query", query);
      ArgsCheck.notNull("params", params);

      TypedQuery<T> typedQuery = em.createNamedQuery(query, type);
      for (String param : params.keySet()) {
         Object val = params.get(param);
         typedQuery.setParameter(param, val);
      }
      return typedQuery.getSingleResult();
   }

   /**
    * Executes the given named update with the given parameters.
    * 
    * @param em
    *           the entity manager to create the query against
    * @param query
    *           the named update (DELETE or UPDATE)
    * @param params
    *           a map of parameter names and their corresponding values
    */
   public static void namedUpdate(EntityManager em, String query,
         Map<String, Object> params) {

      ArgsCheck.notNull("entity manager", em);
      ArgsCheck.notEmpty("query", query);
      ArgsCheck.notNull("params", params);

      Query typedQuery = em.createNamedQuery(query);
      for (String param : params.keySet()) {
         Object val = params.get(param);
         typedQuery.setParameter(param, val);
      }
      typedQuery.executeUpdate();
   }
}
