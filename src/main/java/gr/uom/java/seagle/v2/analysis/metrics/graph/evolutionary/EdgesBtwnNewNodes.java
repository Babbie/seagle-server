package gr.uom.java.seagle.v2.analysis.metrics.graph.evolutionary;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.distributions.StatUtils;
import gr.uom.java.seagle.v2.analysis.graph.AbstractEdge;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.evolution.GraphEvolution;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.Version;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author Theodore Chaikalis
 */
public class EdgesBtwnNewNodes extends AbstractGraphEvolutionaryMetric {
    
    public static final String MNEMONIC = "EDGES_BTWN_NEW";

    public EdgesBtwnNewNodes(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(GraphEvolution ge) {
        Map<Version, Set<AbstractEdge>> edgesBtwnNewMap = new LinkedHashMap<>();
        Map<Integer, Integer> edgesBtwnNewCountFrequencyMap = new TreeMap<>();

        Version[] versionList = ge.getVersionArray();
        for (int i = 1; i < versionList.length; i++) {
            Version currentVersion = versionList[i];
            Version previousVersion = versionList[i - 1];
            
            Set<AbstractEdge> edgesBtwnNew = new LinkedHashSet<>();
            Collection<AbstractEdge> edgesInCurrentVersion = ge.getGraphForAVersion(currentVersion).getEdges();
            Collection<AbstractEdge> edgesInPreviousVersion = ge.getGraphForAVersion(previousVersion).getEdges();
            
            Set<AbstractEdge> newEdges = new HashSet<>(edgesInCurrentVersion);
            newEdges.removeAll(edgesInPreviousVersion);

            Collection<AbstractNode> nodesInPreviousVersion = ge.getGraphForAVersion(previousVersion).getVertices();
            
            for(AbstractEdge edge : newEdges) {
                AbstractNode sourceNode = edge.getSourceNode();
                AbstractNode targetNode = edge.getTargetNode();
                if(!nodesInPreviousVersion.contains(sourceNode) && !nodesInPreviousVersion.contains(targetNode) ){
                    edgesBtwnNew.add(edge);
                }
            }
            
            edgesBtwnNewMap.put(currentVersion, edgesBtwnNew);
            StatUtils.updateFrequencyMap(edgesBtwnNewCountFrequencyMap, edgesBtwnNew.size());
        }
        ge.setEdgesBtwnNewMap(edgesBtwnNewMap);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Number of edges that attached between new nodes during a version";
    }

    @Override
    public String getName() {
        return "Edges between new nodes per version";
    }
}
