package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.VersionInfo;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * A simple version info service used for basic CRUD operations.
 * <p>
 * This class should be run within a container which supports
 * managed transactions. Each db operation is supposed to be run
 * within a transaction provided by the container (DEFAULT = required).
 * For more control over operations provided by this class the calling
 * method should provide its own support of transactions.
 * 
 * @author Daniel Feitosa 
*/
@Stateless
public class VersionInfoFacade extends AbstractFacade<VersionInfo> {

    @PersistenceContext(unitName = "seaglePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Create an instance of this service by specifying the entity
     * type of class {@link VersionInfo}.
     */
    public VersionInfoFacade() {
        super(VersionInfo.class);
    }
}
