package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.MetricCategory;
import gr.uom.se.util.validation.ArgsCheck;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Elvis Ligu
 */
@Stateless
public class MetricCategoryFacade extends AbstractFacade<MetricCategory> {

   /**
    * Injected entity manager for the persistence context named
    * {@code seaglePU}.
    * <p>
    */
   @PersistenceContext(unitName = "seaglePU")
   private EntityManager em;

   public MetricCategoryFacade() {
      super(MetricCategory.class);
   }

   @Override
   protected EntityManager getEntityManager() {
      return em;
   }

   /**
    * Get the metric categories with the given name.
    * <p>
    * The name of a category should be unique to the system and
    * calling this method should return at most one entity within the list.
    *
    * @param name the name of the metric category to find, must not be
    * empty
    * @return a list containing the category entity with the given
    * name, or empty if no entity was found.
    */
   public List<MetricCategory> findByCategory(String name) {
      ArgsCheck.notEmpty("name", name);
      String query = "MetricCategory.findByCategory";
      return JPAUtils.namedQueryEntityOneParam(em, MetricCategory.class,
              query, "category", name);
   }
}
