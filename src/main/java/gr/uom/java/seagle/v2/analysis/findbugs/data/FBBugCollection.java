/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.analysis.findbugs.data;

import java.util.ArrayList;

/**
 *
 * @author Samet
 */
public class FBBugCollection 
{
    private String projectName;
    private final ArrayList<FBBugInstance> bugInstances;

    public FBBugCollection() 
    {
        this.bugInstances = new ArrayList<FBBugInstance>();
    }

    public FBBugCollection(String projectName) 
    {
        this();
        this.projectName = projectName;
    }
    
    public void setProjectName(String projectName)
    {
        if(projectName != null && !projectName.isEmpty())
        {
            this.projectName = projectName;
        }
        else
        {
            System.out.println("Project name can not be null or empty");
        }    
    }
    
    public void addBugInstance(FBBugInstance bugInstance)
    {
        if(bugInstance != null)
        {
            this.bugInstances.add(bugInstance);
        }
    }
}
