package gr.uom.java.seagle.v2.db.persistence;

import gr.uom.se.util.validation.ArgsCheck;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Represent a developer within system.
 * <p>
 * Generally speaking a developer is a person who has committed at a given
 * project. A developer is also related to a project, when he is recognized as
 * the project owner. A developer is identified by its email.
 *
 * @author Elvis Ligu
 */
@Entity
@Table(name = "developer")
@XmlRootElement
@NamedQueries({
      @NamedQuery(name = "Developer.findAll", query = "SELECT d FROM Developer d"),
      @NamedQuery(name = "Developer.findById", query = "SELECT d FROM Developer d WHERE d.id = :id"),
      @NamedQuery(name = "Developer.findByName", query = "SELECT d FROM Developer d WHERE d.name = :name"),
      @NamedQuery(name = "Developer.findByEmail", query = "SELECT d FROM Developer d WHERE d.email = :email"),
      @NamedQuery(name = "Developer.findByProjectName", query = "SELECT d FROM Developer d JOIN d.ownProjects p WHERE p.name = :name"),
      @NamedQuery(name = "Developer.findByProjectURL", query = "SELECT d FROM Developer d JOIN d.ownProjects p WHERE p.remoteRepoPath = :url") })
public class Developer implements Serializable {

   private static final long serialVersionUID = 1L;

   /**
    * The id of this instance.
    * <p>
    */
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id")
   private Long id;

   /**
    * The name of this developer.
    * <p>
    * Max of 256 characters.
    */
   @Size(max = 256)
   @Column(name = "name")
   private String name;

   /**
    * Email of this developer.
    * <p>
    * Max of 255 characters. Null is not allowed.
    */
   @Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message = "Invalid email")
   @Size(max = 255)
   @NotNull
   @Column(name = "email", unique = true)
   private String email;

   /**
    * The list of projects this developer owns.
    * <p>
    */
   @JoinTable(name = "project_owner", joinColumns = { @JoinColumn(name = "developer_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "project_id", referencedColumnName = "id") })
   @ManyToMany
   private Collection<Project> ownProjects;

   /**
    * The list of versions this developer is author.
    * <p>
    */
   @JoinTable(name = "version_author", joinColumns = { @JoinColumn(name = "developer_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "version_id", referencedColumnName = "id") })
   @ManyToMany
   private Collection<Version> authorVersions;

   /**
    * The list of versions this developer is committer.
    * <p>
    */
   @JoinTable(name = "version_commiter", joinColumns = { @JoinColumn(name = "developer_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "version_id", referencedColumnName = "id") })
   @ManyToMany
   private Collection<Version> committerVersions;

   /**
    * Create an empty instance of developer.
    * <p>
    */
   public Developer() {
   }

   /**
    * @return developer id
    */
   public Long getId() {
      return id;
   }

   /**
    * @return the name of this developer
    */
   public String getName() {
      return name;
   }

   /**
    * Set this developer name.
    * <p>
    *
    * @param name
    *           of this developer
    */
   public void setName(String name) {
      this.name = name;
   }

   /**
    * @return the email of this developer
    */
   public String getEmail() {
      return email;
   }

   /**
    * Set the email of this developer.
    * <p>
    *
    * @param email
    *           the email of this developer. Null is not allowed.
    */
   public void setEmail(String email) {
      this.email = email;
   }

   /**
    * @return the projects this developer owns.
    */
   @XmlTransient
   public Collection<Project> getOwnProjects() {
      return ownProjects;
   }

   /**
    * Add a project owned by this developer.
    * <p>
    *
    * @param project
    *           owned by this developer. Must not be null.
    */
   public void addOwnProject(Project project) {
      ArgsCheck.notNull("project", project);
      if (ownProjects == null) {
         ownProjects = new HashSet<>();
      }
      ownProjects.add(project);
   }

   /**
    * Remove the project from the list of this developer owns.
    * <p>
    *
    * @param project
    *           this developer owns. Must not be null.
    */
   public void removeOwnProject(Project project) {
      if (ownProjects != null) {
         ArgsCheck.notNull("project", project);
         ownProjects.remove(project);
      }
   }

   /**
    * Set the projects this developer owns.
    * <p>
    *
    * @param ownProjects
    *           the projects of this developer
    */
   public void setOwnProjects(Collection<Project> ownProjects) {
      this.ownProjects = ownProjects;
   }

   /**
    * Get the versions this developer committed.
    * <p>
    * 
    * @return the versions this developer is committer
    */
   public Collection<Version> getCommitterVersions() {
      return committerVersions;
   }

   /**
    * Set the versions this developer committed.
    * <p>
    * 
    * @param authorVersions
    *           the versions this developer is committer
    */
   public void setCommitterVersions(Collection<Version> committerVersions) {
      this.committerVersions = committerVersions;
   }

   /**
    * Add a version that this developer is committer.
    * <p>
    * 
    * @param version
    *           that this developer is committer
    */
   public void addCommitterVersion(Version version) {
      ArgsCheck.notNull("version", version);
      if (committerVersions == null) {
         committerVersions = new HashSet<>();
      }
      committerVersions.add(version);
   }

   /**
    * Remove a version that this developer committed.
    * <p>
    * 
    * @param version
    *           this developer committed
    */
   public void removeCommitterVersion(Version version) {
      if (committerVersions != null) {
         ArgsCheck.notNull("version", version);
         committerVersions.remove(version);
      }
   }

   /**
    * Get the versions this developer authored.
    * <p>
    * 
    * @return the versions this developer is author
    */
   public Collection<Version> getAuthorVersions() {
      return authorVersions;
   }

   /**
    * Set the versions this developer authored.
    * <p>
    * 
    * @param authorVersions
    *           the versions this developer is author
    */
   public void setAuthorVersions(Collection<Version> authorVersions) {
      this.authorVersions = authorVersions;
   }

   /**
    * Add a version that this developer is author.
    * <p>
    * 
    * @param version
    *           that this developer is author
    */
   public void addAuthorVersion(Version version) {
      ArgsCheck.notNull("version", version);
      if (authorVersions == null) {
         authorVersions = new HashSet<>();
      }
      authorVersions.add(version);
   }

   /**
    * Remove a version that this developer authored.
    * <p>
    * 
    * @param version
    *           this developer authored
    */
   public void removeAuthorVersion(Version version) {
      if (authorVersions != null) {
         ArgsCheck.notNull("version", version);
         authorVersions.remove(version);
      }
   }

   @Override
   public int hashCode() {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object) {
      // TODO: Warning - this method won't work in the case the id fields are
      // not set
      if (!(object instanceof Developer)) {
         return false;
      }
      Developer other = (Developer) object;
      if ((this.id == null && other.id != null)
            || (this.id != null && !this.id.equals(other.id))) {
         return false;
      }
      return true;
   }

   @Override
   public String toString() {
      return "gr.uom.java.seagle.db.persistence.v2.Developer[ id=" + id + " ]";
   }

}
