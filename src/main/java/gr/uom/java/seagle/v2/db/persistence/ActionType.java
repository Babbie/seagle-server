package gr.uom.java.seagle.v2.db.persistence;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Represents the type of an action within the system.
 * <p>
 * Each time an event is triggered, even by an external component or by an
 * internal (maintenance) one, an action will be registered. There will be
 * different types of actions, and each action is related to an entity, for
 * example PROJECT_INSERT is an action.
 *
 * @author Elvis Ligu
 */
@Entity
@Table(name = "action_type")
@XmlRootElement
@NamedQueries({
   @NamedQuery(name = "ActionType.findAll", query = "SELECT a FROM ActionType a"),
   @NamedQuery(name = "ActionType.findById", query = "SELECT a FROM ActionType a WHERE a.id = :id"),
   @NamedQuery(name = "ActionType.findByDescription", query = "SELECT a FROM ActionType a WHERE a.description = :description"),
   @NamedQuery(name = "ActionType.findByMnemonic", query = "SELECT a FROM ActionType a WHERE a.mnemonic = :mnemonic")})
public class ActionType implements Serializable {

   private static final long serialVersionUID = 1L;

   /**
    * Id of this instance
    */
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id")
   private Long id;

   /**
    * The description of this instance.
    * <p>
    * Max of 256 characters.
    */
   @Basic(optional = false)
   @Size(max = 255)
   @Column(name = "description")
   private String description;

   /**
    * A unique keyword for this type.
    * <p>
    * Max of 45 characters. Null is not allowed.
    */
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 45)
   @Column(name = "mnemonic", unique = true)
   private String mnemonic;

   /**
    * Actions related to projects, such as INSERT, UPDATE.
    */
   @OneToMany(cascade = CascadeType.ALL, mappedBy = "actionType")
   private Collection<ProjectTimeline> projectTimelines;

   /**
    * Create an empty instance.
    * <p>
    */
   public ActionType() {
   }

   /**
    * Create an empty instance given its id.
    * <p>
    *
    * @param id of the action type. Usually is auto generated.
    */
   public ActionType(Long id) {
      this.id = id;
   }

   /**
    * Create an instance given its properties.
    * <p>
    *
    * @param id of the action type. Usually is auto generated.
    * @param description of the action
    * @param mnemonic a unique keyword of the action.
    */
   public ActionType(Long id, String description, String mnemonic) {
      this.id = id;
      this.description = description;
      this.mnemonic = mnemonic;
   }

   /**
    * @return the action type id
    */
   public Long getId() {
      return id;
   }


   /**
    * @return the description of this action type
    */
   public String getDescription() {
      return description;
   }

   /**
    * Set the description of this action type.
    * <p>
    *
    * @param description of this action type
    */
   public void setDescription(String description) {
      this.description = description;
   }

   /**
    * @return a unique keyword of this action type
    */
   public String getMnemonic() {
      return mnemonic;
   }

   /**
    * Set a unique keyword of this action type.
    * <p>
    *
    * @param mnemonic a unique keyword of this action type. Null or empty
    * strings are not allowed.
    */
   public void setMnemonic(String mnemonic) {
      this.mnemonic = mnemonic;
   }

   /**
    * @return the actions (events) of this type, related to projects.
    */
   @XmlTransient
   public Collection<ProjectTimeline> getProjectTimelines() {
      return projectTimelines;
   }

   /**
    * Set actions of this type related to projects.
    * <p>
    *
    * @param projectTimelines the actions of this type related to projects
    */
   public void setProjectTimelines(Collection<ProjectTimeline> projectTimelines) {
      this.projectTimelines = projectTimelines;
   }

   @Override
   public int hashCode() {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object) {
      // TODO: Warning - this method won't work in the case the id fields are
      // not set
      if (!(object instanceof ActionType)) {
         return false;
      }
      ActionType other = (ActionType) object;
      if ((this.id == null && other.id != null)
              || (this.id != null && !this.id.equals(other.id))) {
         return false;
      }
      return true;
   }

   @Override
   public String toString() {
      return "gr.uom.java.seagle.db.persistence.v2.ActionType[ id=" + id + " ]";
   }

}
