/**
 * 
 */
package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.Property;
import gr.uom.se.util.mapper.MapperFactory;
import gr.uom.se.util.validation.ArgsCheck;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Elvis Ligu
 */
@Singleton
public class PropertyFacade extends AbstractFacade<Property> {

   /**
    * 
    */
   public PropertyFacade() {
      super(Property.class);
   }

   /**
    * Injected entity manager for the persistence context named {@code seaglePU}
    * .
    * <p>
    */
   @PersistenceContext(unitName = "seaglePU")
   private EntityManager em;

   @Override
   protected EntityManager getEntityManager() {
      return em;
   }

   /**
    * Get the property for the given domain with the given name.
    * <p>
    * 
    * @param domain
    *           of the property, must not be empty
    * @param name
    *           of the property must not be empty
    * @return the property entity if found or null
    */
   public Property getPropertyByName(String domain, String name) {
      ArgsCheck.notEmpty("domain", domain);
      ArgsCheck.notEmpty("name", name);
      String id = generatePropertyId(domain, name);
      String query = "Property.findById";
      List<Property> list = JPAUtils.namedQueryEntityOneParam(em,
            Property.class, query, "id", id);
      if (list.isEmpty()) {
         return null;
      }
      return list.get(0);
   }

   /**
    * Get the properties of the given domain.
    * <p>
    * 
    * @param domain
    *           of the properties, must not be empty
    * @return all properties of a given domain
    */
   public List<Property> getDomainProperties(String domain) {
      ArgsCheck.notEmpty("domain", domain);
      String query = "Property.findByDomain";
      return JPAUtils.namedQueryEntityOneParam(em, Property.class, query,
            "domain", domain);
   }

   /**
    * Get all properties with the given name.
    * <p>
    * 
    * @param name
    *           of properties, must not be empty.
    * @return all properties with the given name.
    */
   public List<Property> getNamedProperties(String name) {
      ArgsCheck.notEmpty("name", name);
      String query = "Property.findByName";
      return JPAUtils.namedQueryEntityOneParam(em, Property.class, query,
            "name", name);
   }

   /**
    * Get all properties of a given domain with a given prefix.
    * <p>
    * If prefix is null or empty it will return all properties of the given
    * domain.
    * 
    * @param domain
    *           of the properties, must not be empty.
    * @param prefix
    *           of properties
    * @return all properties of the given domain with the given prefix
    */
   public List<Property> getDomainByPrefix(String domain, String prefix) {
      if (prefix == null || prefix.isEmpty()) {
         return getDomainProperties(domain);
      }
      ArgsCheck.notEmpty("domain", domain);
      String query = "Property.findByDomainAndPrefixName";
      prefix = prefix + "%";
      return JPAUtils.namedQueryEntityTwoParam(em, Property.class, query,
            "domain", domain, "prefix", prefix);
   }

   /**
    * Get the value of a property in the given domain.
    * <p>
    * 
    * @param domain
    *           of the property, must not be empty.
    * @param name
    *           of the property must not be empty.
    * @return the value of the property in the given domain.
    */
   public String get(String domain, String name) {
      ArgsCheck.notEmpty("domain", domain);
      ArgsCheck.notEmpty("name", name);
      String id = generatePropertyId(domain, name);
      String query = "Property.findByIdVal";
      List<String> list = JPAUtils.namedQueryEntityOneParam(em, String.class,
            query, "id", id);
      if (list.isEmpty()) {
         return null;
      }
      return list.get(0);
   }

   /**
    * Get the value of a property in the given domain, by converting its string
    * representation to the required type.
    * <p>
    * NOTE: for date values try to use their milliseconds property to avoid any
    * formatting exceptions.
    * 
    * @param domain
    *           of the property, must not be empty.
    * @param name
    *           of the property must not be empty.
    * @return the value of the property in the given domain.
    */
   public <T> T get(String domain, String name, Class<T> type) {

      String value = get(domain, name);
      return MapperFactory.getInstance().getMapper(String.class, type)
            .map(value, type);
   }

   /**
    * Create a property with the given parameters.
    * <p>
    * If property exists for the given name and domain it will throw an
    * exception.
    * 
    * @param domain
    *           property domain
    * @param name
    *           property name
    * @param value
    *           the value of property, if null it will do nothing
    */
   public void create(String domain, String name, String value) {
      if (value == null) {
         return;
      }
      ArgsCheck.notEmpty("domain", domain);
      ArgsCheck.notEmpty("name", name);
      ArgsCheck.notNull("value", value);
      Property property = new Property();
      property.setDomain(domain);
      property.setName(name);
      property.setValue(value);
      create(property);
   }

   @Override
   public void create(Property property) {
      generateId(property);
      super.create(property);
   }

   /**
    * Create a property with the given parameters.
    * <p>
    * If property exists for the given name and domain it will throw an
    * exception.
    * 
    * @param domain
    *           property domain
    * @param name
    *           property name
    * @param value
    *           the value of property, if null it will do nothing. The value
    *           will be converted to string before storing it, so only primitive
    *           types (or their wrappers) and or arrays of them are allowed.
    *           Also if you want to store a date try to store its milliseconds
    *           as a string value.
    */
   public void create(String domain, String name, Object value) {
      if (value == null) {
         return;
      }
      Class<?> type = value.getClass();
      String str = MapperFactory.getInstance().getMapper(type, String.class)
            .map(value, String.class);
      create(domain, name, str);
   }

   /**
    * Update a property with the given parameters.
    * <p>
    * If property doesn't exists for the given name and domain it will throw an
    * exception.
    * 
    * @param domain
    *           property domain
    * @param name
    *           property name
    * @param value
    *           the value of property, if null it will remove the property. The
    *           value will be converted to string before storing it, so only
    *           primitive types (or their wrappers) and or arrays of them are
    *           allowed. Also if you want to store a date try to store its
    *           milliseconds as a string value.
    */
   public void edit(String domain, String name, Object value) {
      Property property = getPropertyByName(domain, name);
      if (property == null) {
         if (value == null) {
            return;
         }
         throw new IllegalArgumentException("Property under domain " + domain
               + " with name " + name + " was not found");
      }

      // Must delete the property
      if (value == null) {
         remove(property);
         return;
      }

      if (value instanceof String) {
         property.setValue((String) value);
      } else {
         Class<?> type = value.getClass();
         String str = MapperFactory.getInstance().getMapper(type, String.class)
               .map(value, String.class);
         property.setValue(str);
      }
      edit(property);
   }

   /**
    * Delete all properties of the given domain.
    * <p>
    * 
    * @param domain
    *           name of the domain, must not be null
    */
   public void deleteDomain(String domain) {
      ArgsCheck.notNull("domain", domain);
      String query = "Property.deleteByDomain";
      JPAUtils.namedUpdateOneParam(em, query, "domain", domain);
   }

   private final static DateFormat dateFormatter = new SimpleDateFormat(
         "yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

   public static String toString(Date date) {
      ArgsCheck.notNull("date", date);
      return dateFormatter.format(date);
   }

   public static Date toDate(String str) throws ParseException {
      ArgsCheck.notEmpty("str", str);
      return dateFormatter.parse(str);
   }

   public static String toString(Date[] dates) {
      ArgsCheck.notNull("dates", dates);
      StringBuilder sb = new StringBuilder();
      sb.append('[');
      for (int i = 0; i < dates.length; i++) {
         Date d = dates[i];
         String c = toString(d);
         sb.append(c);
         if (i < dates.length - 1) {
            sb.append(',');
         }
      }
      sb.append(']');
      return sb.toString();
   }

   public static Date[] toDates(String[] str) throws ParseException {
      ArgsCheck.containsNoNull("str", (Object[]) str);
      Date[] dates = new Date[str.length];
      for (int i = 0; i < str.length; i++) {
         dates[i] = toDate(str[i]);
      }
      return dates;
   }

   public static String toString(Date[][] dates) {
      ArgsCheck.notNull("dates", dates);
      StringBuilder sb = new StringBuilder();
      sb.append('[');
      for (int i = 0; i < dates.length; i++) {
         Date[] d = dates[i];
         String c = toString(d);
         sb.append(c);
         if (i < dates.length - 1) {
            sb.append(',');
         }
      }
      sb.append(']');
      return sb.toString();
   }

   public static Date[][] toDates(String[][] str) throws ParseException {
      ArgsCheck.notNull("str", str);
      Date[][] dates = new Date[str.length][];
      for (int i = 0; i < str.length; i++) {
         dates[i] = toDates(str[i]);
      }
      return dates;
   }

   public static String toString(Date[][][] dates) {
      ArgsCheck.notNull("dates", dates);
      StringBuilder sb = new StringBuilder();
      sb.append('[');
      for (int i = 0; i < dates.length; i++) {
         Date[][] d = dates[i];
         String c = toString(d);
         sb.append(c);
         if (i < dates.length - 1) {
            sb.append(',');
         }
      }
      sb.append(']');
      return sb.toString();
   }

   public static Date[][][] toDates(String[][][] str) throws ParseException {
      ArgsCheck.notNull("str", str);
      Date[][][] dates = new Date[str.length][][];
      for (int i = 0; i < str.length; i++) {
         dates[i] = toDates(str[i]);
      }
      return dates;
   }

   static MessageDigest md = null;
   static {
      try {
         md = MessageDigest.getInstance("SHA-256");
      } catch (NoSuchAlgorithmException e) {
         throw new RuntimeException(e);
      }
   }

   /**
    * A workaround for computing the id from 'domain' and 'value' using sha-256
    * hashing. This must be executed before the entity is being created in db
    * and both domain and name are specified.
    */
   public void generateId(Property property) {
      String value = generatePropertyId(property.getDomain(),
            property.getName());
      property.setId(value);
   }

   public String generatePropertyId(String domain, String name) {
      String value = domain + name;
      try {

         byte[] bytes = value.getBytes("UTF-8");
         bytes = md.digest(bytes);
         value = bytesToHexString(bytes);
         return value;
      } catch (UnsupportedEncodingException e) {
         throw new RuntimeException(e);
      }
   }

   private static String bytesToHexString(byte[] bytes) {
      // http://stackoverflow.com/questions/332079
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < bytes.length; i++) {
         String hex = Integer.toHexString(0xFF & bytes[i]);
         if (hex.length() == 1) {
            sb.append('0');
         }
         sb.append(hex);
      }
      return sb.toString();
   }
}
