package gr.uom.java.seagle.v2.db.persistence;

import gr.uom.se.util.validation.ArgsCheck;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * An entity specifying a programming language within system.
 * <p>
 * The identification of a programming language is a useful feature of of the
 * system, especially in computing different metrics.
 *
 * @author Elvis Ligu
 */
@Entity
@Table(name = "programming_language")
@XmlRootElement
@NamedQueries({
      @NamedQuery(name = "ProgrammingLanguage.findAll", query = "SELECT p FROM ProgrammingLanguage p"),
      @NamedQuery(name = "ProgrammingLanguage.findById", query = "SELECT p FROM ProgrammingLanguage p WHERE p.id = :id"),
      @NamedQuery(name = "ProgrammingLanguage.findByName", query = "SELECT p FROM ProgrammingLanguage p WHERE p.name = :name"),
      @NamedQuery(name = "ProgrammingLanguage.findByMetric", query = "SELECT p FROM ProgrammingLanguage p JOIN p.metrics m WHERE m = :metric"),
      @NamedQuery(name = "ProgrammingLanguage.findByNames", query = "SELECT p FROM ProgrammingLanguage p WHERE p IN :names"),
      @NamedQuery(name = "ProgrammingLanguage.findByProject", query = "SELECT p FROM ProgrammingLanguage p JOIN p.projects pr WHERE pr = :project") })
public class ProgrammingLanguage implements Serializable {

   private static final long serialVersionUID = 1L;

   /**
    * Language id.
    * <p>
    */
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id")
   private Long id;

   /**
    * The string representation of the language.
    * <p>
    */
   @NotNull
   @Size(min = 1, max = 100)
   @Column(name = "name")
   private String name;

   /**
    * A list of projects using this language.
    * <p>
    */
   @JoinTable(name = "project_language", joinColumns = { @JoinColumn(name = "programming_language_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "project_id", referencedColumnName = "id") })
   @ManyToMany
   private Collection<Project> projects;

   /**
    * The list of metrics that can be computed against this language.
    * <p>
    */
   @JoinTable(name = "language_applied", joinColumns = { @JoinColumn(name = "prog_lang_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "metric_id", referencedColumnName = "id") })
   @ManyToMany
   private Collection<Metric> metrics;

   /**
    * Create an empty language.
    * <p>
    */
   public ProgrammingLanguage() {
   }

   /**
    * @return the language id
    */
   public Long getId() {
      return id;
   }


   /**
    * Get the string of this language.
    * <p>
    * 
    * @return the string of this language
    */
   public String getName() {
      return name;
   }

   /**
    * Set the string of this language.
    * <p>
    * 
    * @param name
    *           the string of this language. Not null, max of 100 chars.
    */
   public void setName(String name) {
      this.name = name;
   }

   /**
    * Get the projects using this language.
    * <p>
    * 
    * @return the projects using this language.
    */
   @XmlTransient
   public Collection<Project> getProjects() {
      return projects;
   }

   /**
    * Set the projects using this language.
    * <p>
    * 
    * @param projects
    *           the projects using this language.
    */
   public void setProjects(Collection<Project> projects) {
      this.projects = projects;
   }

   /**
    * Add a project using this language.
    * <p>
    * 
    * @param project
    *           using this language. Must not be null.
    */
   public void addProject(Project project) {
      ArgsCheck.notNull("project", project);
      if (projects == null) {
         projects = new HashSet<>();
      }
      projects.add(project);
   }

   /**
    * Remove a project from the list.
    * <p>
    * 
    * @param project
    *           to be removed. Must not be null.
    */
   public void removeProject(Project project) {
      ArgsCheck.notNull("project", project);
      if (projects != null) {
         projects.remove(project);
      }
   }

   /**
    * @return the metrics applying to this language.
    */
   @XmlTransient
   public Collection<Metric> getMetrics() {
      return metrics;
   }

   /**
    * Set the metrics applying to this language.
    * <p>
    * 
    * @param metrics
    *           applied to this language.
    */
   public void setMetrics(Collection<Metric> metrics) {
      this.metrics = metrics;
   }

   /**
    * Add a metric that apply to this language.
    * <p>
    * 
    * @param metric
    *           to add to this language.
    */
   public void addMetric(Metric metric) {
      ArgsCheck.notNull("metric", metric);
      if (metrics == null) {
         metrics = new HashSet<>();
      }
      metrics.add(metric);
   }

   /**
    * Remove a metric from the list of metrics.
    * <p>
    * 
    * @param metric
    *           to be removed
    */
   public void removeMetric(Metric metric) {
      ArgsCheck.notNull(" metric", metric);
      if (metrics != null) {
         metrics.remove(metric);
      }
   }

   @Override
   public int hashCode() {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object) {
      // TODO: Warning - this method won't work in the case the id fields are
      // not set
      if (!(object instanceof ProgrammingLanguage)) {
         return false;
      }
      ProgrammingLanguage other = (ProgrammingLanguage) object;
      if ((this.id == null && other.id != null)
            || (this.id != null && !this.id.equals(other.id))) {
         return false;
      }
      return true;
   }

   @Override
   public String toString() {
      return "gr.uom.java.seagle.db.persistence.v2.ProgrammingLanguage[ id="
            + id + " ]";
   }

}
