/**
 * 
 */
package gr.uom.java.seagle.v2.ws.rest.metric.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Elvis Ligu
 */
@XmlRootElement(name = "metric")
public class RESTMetricValue {

   private String mnemonic;
   private String className;
   private double value;
   
   public String getMnemonic() {
      return mnemonic;
   }

   public void setMnemonic(String metric) {
      this.mnemonic = metric;
   }

   public double getValue() {
      return value;
   }

   public void setValue(double value) {
      this.value = value;
   }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

   /**
    * 
    */
   public RESTMetricValue() {
   }

   public RESTMetricValue(String mnemonic, double value) {
      this.mnemonic = mnemonic;
      this.value = value;
      className = " ";
   }
   
   public RESTMetricValue(String mnemonic, double value, String className) {
      this.mnemonic = mnemonic;
      this.value = value;
      this.className = className;
   }
}
