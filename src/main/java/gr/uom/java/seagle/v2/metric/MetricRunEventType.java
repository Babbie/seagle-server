
package gr.uom.java.seagle.v2.metric;

import gr.uom.se.util.event.EventType;

/**
 *
 * @author elvis
 */
public enum MetricRunEventType implements EventType {
   START,
   END
}
