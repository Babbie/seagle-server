
package gr.uom.java.seagle.v2.metric.repo;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;
import gr.uom.java.seagle.v2.metric.imp.MetricEventListener;

/**
 *
 * @author elvis
 */
public class RepoMetricListener extends MetricEventListener {

   public RepoMetricListener(SeagleManager seagleManager, 
           EventManager eventManager) {
      super(seagleManager, eventManager);
   }
   
   @Override
   protected ExecutableMetric getMetric(String mnemonic) {
      try {
         return RepoMetricEnum.valueOf(mnemonic);
      } catch (Exception ex) {
         return null;
      }
   }
}
