package gr.uom.java.seagle.v2.db.persistence;

import gr.uom.se.util.validation.ArgsCheck;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * This class represent the category of a metric.
 * <p>
 * @author Elvis Ligu
 */
@Entity
@Table(name = "metric_category")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MetricCategory.findAll", query = "SELECT m FROM MetricCategory m"),
    @NamedQuery(name = "MetricCategory.findById", query = "SELECT m FROM MetricCategory m WHERE m.id = :id"),
    @NamedQuery(name = "MetricCategory.findByCategory", query = "SELECT m FROM MetricCategory m WHERE m.category = :category")})
public class MetricCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * The id of this entity.
     * <p>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**
     * The category of this metric.
     * <p>
     * Should be a unique single word, max of 255 characters.
     */
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "category", unique = true)
    private String category;

    /**
     * The description of this metric.
     * <p>
     * May be a large text max of 65K characters.
     */
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;

    /**
     * Metrics of this category.
     * <p>
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "category", orphanRemoval = true)
    private Collection<Metric> metrics;

    /**
     * Create an empty instance.
     * <p>
     */
    public MetricCategory() {
    }

    /**
     * @return the id of this category
     */
    public Long getId() {
        return id;
    }

    /**
     * @return a unique keyword of this category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Set a unique keyword of this category.
     * <p>
     * @param category a unique keyword of this category, max of 45 characters
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the description of this category
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description of this category.
     * <p>
     * @param description of this category, max of 65K characters
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the metrics of this category 
     */
    @XmlTransient
    public Collection<Metric> getMetrics() {
        return metrics;
    }

    /**
     * Set the metrics of this category.
     * <p>
     * @param metrics of this category 
     */
    public void setMetrics(Collection<Metric> metrics) {
        this.metrics = metrics;
    }

    /**
     * Add a metric to this category.
     * <p>
     * @param metric to be added to this category, must not be null.
     */
    public void addMetric(Metric metric) {
        ArgsCheck.notNull("metric", metric);
        if(metrics == null) {
            metrics = new HashSet<>();
        }
        metrics.add(metric);
    }
    
    public void removeMetric(Metric metric) {
        ArgsCheck.notNull("metric", metric);
        if(metrics != null) {
            metrics.remove(metric);
        }
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MetricCategory)) {
            return false;
        }
        MetricCategory other = (MetricCategory) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gr.uom.java.seagle.db.persistence.v2.MetricCategory[ id=" + id + " ]";
    }

}
