/**
 * 
 */
package gr.uom.java.seagle.v2.metric.imp;

import gr.uom.java.seagle.v2.AbstractSeagleResolver;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.scheduler.SchedulerConfig;
import gr.uom.java.seagle.v2.scheduler.SeagleTaskSchedulerManager;
import gr.uom.se.util.concurrent.Task;
import gr.uom.se.util.concurrent.TaskType;

import java.util.concurrent.Executor;

/**
 * An implementation of executor that will be used by metric event handler to
 * schedule a metric for execution into {@linkplain SeagleTaskSchedulerManager
 * task scheduler}.
 * 
 * @author Elvis Ligu
 */
public class MetricExecutor extends AbstractSeagleResolver implements Executor {

   /**
    * Creates an instance of this executor.
    * 
    * @param seagleManager
    */
   public MetricExecutor(SeagleManager seagleManager) {
      super(seagleManager);
   }

   /**
    * {@inheritDoc}
    * <p>
    * Schedule the command to be executed into seagle
    * {@linkplain SeagleTaskSchedulerManager task scheduler} scheduler. If the
    * given command is of type {@link Task}, it will be executed as is by the
    * scheduler, however if it is not it will be executed using parallel task
    * type using 4 threads.
    */
   @Override
   public void execute(Runnable command) {

      // Check the defaults first
      SeagleTaskSchedulerManager scheduler = resolveComponentOrManager(SeagleTaskSchedulerManager.class);
      TaskType type = null;
      if (command instanceof Task) {
         Task task = (Task) command;
         type = task.getType();
      } else {
         type = scheduler.getType(SchedulerConfig.TASKTYPE_PARALLEL);
         // The task type should be present
         // otherwise we have a problem
         if (type == null) {
            throw new RuntimeException("Task type was not found "
                  + SchedulerConfig.TASKTYPE_PARALLEL);
         }
      }

      // The scheduler should support this task type
      // otherwise we can't execute
      if (!scheduler.canSchedule(type)) {
         throw new RuntimeException("Task type can not be scheduled " + type);
      }

      scheduler.schedule(command, type);
   }
}
