package gr.uom.java.seagle.v2.project.build;

import gr.uom.java.seagle.v2.SeagleManager;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel Feitosa
 */
public class AntBuilder implements ProjectBuilder {

    /**
     * Folder of the project to be built
     */
    private final String projectFolder;
    private final SeagleManager seagleManager;
    private final Logger logger;
    
    /**
     * Version of Ant used for building the current project
     */
    private String antVersion;

    protected AntBuilder(SeagleManager seagleManager, String projectFolder){
        this.seagleManager = seagleManager;
        this.projectFolder = projectFolder + File.separator;
        this.logger = Logger.getLogger(AntBuilder.class.getName());
        // Verify necessary version (it may not be necessary)
        // Verify if it is available
        
        if(isAvailable()) {
            System.out.println("Ant version " + antVersion + " is available.");
        }
        else {
            System.out.println("Ant is not available.");
        }
        
        // Ant version is set in isAvailable() method.
        // Set necessary properties
    }
    
    @Override
    public String getName() {
        return "Ant";
    }

    @Override
    public String getVersion() {
        return antVersion;
    }
    
    protected static boolean canBuild(String folder) {
         //If build.xml file exists in the directory "folder", it can be built by Ant.
        
        File buildXmlFile = new File(folder + File.separator + "build.xml");
        
        return buildXmlFile.exists() && !buildXmlFile.isDirectory();
    }
    
    @Override
    public boolean canBuild() {
        return AntBuilder.canBuild(projectFolder);
    }

    @Override
    public boolean isAvailable() {
        System.setProperty("JAVA_HOME", seagleManager.getSeaglePathConfig().getJavaHome());
        ProcessBuilder pb = new ProcessBuilder(seagleManager.getSeaglePathConfig().getAntCommand(), "-version");
        
        try 
        {
            Process p = pb.start();
            
            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String s;
            while ((s = in.readLine()) != null)
            {
                if (s.startsWith("Apache Ant"))
                {
                    //Example s string : Apache Ant(TM) version 1.9.3 compiled on April 8 2014
                    antVersion = s.substring(23, s.indexOf(" compiled"));
                    p.destroy();
                    return true;
                }
            }
            
            p.waitFor();
            antVersion = "Ant not available.";
            return false;
            
        }
        catch (Exception e) 
        {
            logger.log(Level.INFO, "Cannot compile by Ant");
            logger.log(Level.INFO, e.getLocalizedMessage());
            antVersion = "Ant not available";
            return false;
        }
    }

    @Override
    public boolean tryToBuild() 
    {
        System.setProperty("JAVA_HOME", seagleManager.getSeaglePathConfig().getJavaHome());
        ProcessBuilder pb = new ProcessBuilder(seagleManager.getSeaglePathConfig().getAntCommand(), "-buildfile", projectFolder + "build.xml");

        try (PrintWriter logFile = new PrintWriter(new FileWriter(getPathToBuildLog(), true)))
        {
            Process p = pb.start();
            
            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String s;
            while ((s = in.readLine()) != null)
            {
                logger.log(Level.INFO, s);
                logFile.println(s);
            }

            logFile.close();

            int status = p.waitFor();
            
            if (status != 0)
            {
                logger.log(Level.INFO, "Encountered with an error while trying to compile by Ant");
                return false;
            }
            else
            {
                return true;
            }
        }
        catch (Exception e) 
        {
            logger.log(Level.SEVERE, "Cannot start compilation by Ant");
            logger.log(Level.SEVERE, e.getLocalizedMessage());
            return false;
        }
    }

    @Override
    public String getPathToBuildLog() {
        return projectFolder + seagleManager.getSeaglePathConfig().getBuildLogName();
    }

    @Override
    public String getPathClassFiles() {
        return this.projectFolder;
    }
}
