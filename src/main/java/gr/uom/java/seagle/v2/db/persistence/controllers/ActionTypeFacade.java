package gr.uom.java.seagle.v2.db.persistence.controllers;

import gr.uom.java.seagle.v2.db.persistence.ActionType;
import gr.uom.se.util.validation.ArgsCheck;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author elvis
 */
@Stateless
public class ActionTypeFacade extends AbstractFacade<ActionType> {

   /**
    * Injected entity manager for the persistence context named
    * {@code seaglePU}.
    * <p>
    */
   @PersistenceContext(unitName = "seaglePU")
   private EntityManager em;

   public ActionTypeFacade() {
      super(ActionType.class);
   }

   /**
    * Given a string mnemonic for the action type, return the entity that
    * corresponds to that mnemonic.
    * <p>
    * Note that this method returns a list to the caller, however the mnemonic
    * of an action is unique so at most one entity should be contained within
    * the result.
    *
    * @param mnemonic the mnemonic of the action type to find. Must not be null.
    * @return a list containing an action type entity that has the same mnemonic
    * as the provided one, or an empty list if no entity was found.
    */
   public List<ActionType> findByMnemonic(String mnemonic) {
      ArgsCheck.notNull("mnemonic", mnemonic);
      String query = "ActionType.findByMnemonic";
      String name = "mnemonic";
      return JPAUtils.namedQueryEntityOneParam(
              em, ActionType.class, query, name, mnemonic);
   }

   @Override
   protected EntityManager getEntityManager() {
      return em;
   }
}
