/**
 * 
 */
package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.AbstractSeagleResolver;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.se.util.context.Context;
import gr.uom.se.util.context.ContextProvider;
import gr.uom.se.util.module.ModuleContext;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.util.validation.ArgsCheck;
import gr.uom.se.vcs.VCSRepository;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Elvis Ligu
 */
public class RepositoryContextProvider extends AbstractSeagleResolver implements ContextProvider {

   /**
    * 
    */
   @ProvideModule
   public RepositoryContextProvider(SeagleManager seagleManager) {
      super(seagleManager);
   }

   /**
    * {@inheritDoc}
    */
   @SuppressWarnings("unchecked")
   @Override
   public <C extends Context> C getContext(Object instance, Class<C> contextType) {
      ArgsCheck.notNull("instance", instance);
      ArgsCheck.notNull("contextType", contextType);
      if (!(contextType.isAssignableFrom(ModuleContext.class) || contextType
            .isAssignableFrom(ProjectContext.class))) {
         return null;
      }
      Class<?> iclass = instance.getClass();
      ModuleContext context = null;
      if (VCSRepository.class.isAssignableFrom(iclass)) {

         context = getContextFromRepository((VCSRepository) instance);

      } else if (Project.class.isAssignableFrom(iclass)) {
         context = getContextFromProject((Project) instance);
      }

      return (C) context;
   }

   private ModuleContext getContextFromRepository(VCSRepository repo) {
      Project project = resolveProjectManager().findByUrl(repo.getRemotePath());
      return getContext(project, repo);
   }

   private ModuleContext getContext(Project project,
         VCSRepository repo) {
      return new DefaultProjectContext(seagleManager, project, repo);
   }

   private ModuleContext getContextFromProject(Project project) {
      VCSRepository repo = resolveProjectManager().getRepository(
            project.getRemoteRepoPath());
      return getContext(project, repo);
   }

   private ProjectManager resolveProjectManager() {
      return resolveComponentOrManager(ProjectManager.class);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public <C extends Context> C getContext(Class<?> type, Class<C> contextType) {
      throw new UnsupportedOperationException(
            "A repository or a project should be provided in order to get a context");
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Set<Class<?>> getTypes() {
      Set<Class<?>> types = new HashSet<>();
      types.add(VCSRepository.class);
      types.add(Project.class);
      return types;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Set<Class<? extends Context>> getContextTypes() {
      Set<Class<? extends Context>> types = new HashSet<>();
      types.add(ModuleContext.class);
      types.add(ProjectContext.class);
      return types;
   }
}
