/**
 *
 */
package gr.uom.java.seagle.v2.ws.rest.metric.model;

import gr.uom.java.seagle.v2.ws.rest.smells.model.RESTSmell;
import gr.uom.java.seagle.v2.ws.rest.smells.model.RESTSmellSummary;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Elvis Ligu
 */
@XmlRootElement(name = "version")
public class RESTVersion {

    private String name;
    private Collection<RESTMetricValue> metrics;
    private Collection<RESTSmellSummary> smellsSummary;
    private Collection<RESTSmell> smells;

    public RESTVersion(String versionName) {
        this(versionName, null, null);
    }
    /**
     *
     */
    public RESTVersion() {
        this(null, null, null);
    }

    public RESTVersion(String name, Collection<RESTMetricValue> metrics) {
        this.name = name;
        this.metrics = new ArrayList<>();
        this.smells = new ArrayList<>();
        this.smellsSummary = new ArrayList<>();
    }

    public RESTVersion(String name, Collection<RESTMetricValue> metrics,
            Collection<RESTSmell> smells) {
        this.name = name;
        this.metrics = new ArrayList<>();
        this.smells = new ArrayList<>();
        this.smellsSummary = new ArrayList<>();
    }
 
    public void addSmell(String smellName, String containerName){
        this.getRestSmell(smellName).addSmellyContainer(containerName);
    }

    private RESTSmell getRestSmell(String smellName) {
        for(RESTSmell smell : smells){
            if(smell.getName().equals(smellName)){
                return smell;
            }
        }
        RESTSmell rs = new RESTSmell(smellName);
        smells.add(rs);
        return rs;
    }
    
    public Collection<RESTSmellSummary> getSmellsSummary() {
        return smellsSummary;
    }

    public void setSmellsSummary(Collection<RESTSmellSummary> smellsSummary) {
        this.smellsSummary = smellsSummary;
    }

    public Collection<RESTSmell> getSmells() {
        return smells;
    }

    public void setSmells(Collection<RESTSmell> smells) {
        this.smells = smells;
    }

    public void addSmell(RESTSmell smell) {
        smells.add(smell);
    }
    
     public void addSmellSummary(RESTSmellSummary smellSummary) {
        smellsSummary.add(smellSummary);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<RESTMetricValue> getMetrics() {
        return metrics;
    }

    public void setMetrics(Collection<RESTMetricValue> metrics) {
        if (metrics == null) {
            this.metrics = new ArrayList<>();
        } else {
            this.metrics = metrics;
        }
    }

    public void addMetric(RESTMetricValue metric) {
        this.metrics.add(metric);
    }
}