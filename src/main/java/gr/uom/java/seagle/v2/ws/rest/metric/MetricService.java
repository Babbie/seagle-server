package gr.uom.java.seagle.v2.ws.rest.metric;

import gr.uom.java.seagle.v2.analysis.metrics.graph.evolutionary.NodeAge;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.MetricCategory;
import gr.uom.java.seagle.v2.db.persistence.MetricValue;
import gr.uom.java.seagle.v2.db.persistence.NodeVersionMetric;
import gr.uom.java.seagle.v2.db.persistence.ProgrammingLanguage;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.ProjectMetric;
import gr.uom.java.seagle.v2.db.persistence.ProjectVersionMetric;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.java.seagle.v2.db.persistence.controllers.MetricCategoryFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.NodeVersionMetricFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectMetricFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectVersionMetricFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.VersionFacade;
import gr.uom.java.seagle.v2.metric.MetricManager;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.java.seagle.v2.ws.rest.AbstractRestService;
import gr.uom.java.seagle.v2.ws.rest.metric.model.RESTMetricValue;
import gr.uom.java.seagle.v2.ws.rest.metric.model.RESTProject;
import gr.uom.java.seagle.v2.ws.rest.metric.model.RESTVersion;
import gr.uom.java.seagle.v2.ws.rest.project.AnalysisRequestorService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author elvis
 */
@Stateless
@Path("/metric")
public class MetricService extends AbstractRestService implements
        IMetricService {

    @EJB
    MetricManager metricManager;

    @EJB
    ProjectFacade projectFacade;

    @EJB
    MetricCategoryFacade categories;

    @EJB
    ProjectVersionMetricFacade versionMetricFacade;

    @EJB
    NodeVersionMetricFacade nodeVersionMetricFacade;

    @EJB
    ProjectMetricFacade projectMetricFacade;

    @EJB
    VersionFacade versionFacade;

    @EJB
    ProjectManager projectManager;

    private static final Logger logger = Logger.getLogger(MetricService.class.getName());

    @PUT
    @Path("/project/{name}/{mnemonic}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public Response registerMetricToProjectByName(
            @DefaultValue(DEFAULT_PURL) @PathParam("name") String name,
            @DefaultValue(DEFAULT_PURL) @PathParam("mnemonic") String mnemonic) {

        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (name.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("name path must be specified");
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.BAD_REQUEST).entity(errMsg).build();
        }
        // Look up the project in db
        Project project = findProjectByName(name);
        if (project == null) {
            String errMsg = "Project not found with name: " + name;
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.NOT_FOUND).entity(errMsg).build();
        }
        return registerMetric(project, mnemonic);
    }

    private Response registerMetric(Project project, String mnemonic) {

        // If mnemonic is not specified
        // register all metrics to project
        if (mnemonic.endsWith(DEFAULT_PURL)) {
            metricManager.registerAllMetricsToProject(project);
        } else {
            // Look up the metric if it was not found just
            // return a 404 response
            Metric metric = metricManager.getMetricByMnemonic(mnemonic);
            if (metric == null) {
                String errMsg = "Metric not found with mnemonic: " + mnemonic;
                logger.log(Level.INFO, errMsg);
                return Response.status(Response.Status.NOT_FOUND).entity(errMsg).build();
            }
            metricManager.registerMetricToProject(project, metric);
        }
        return Response.ok().build();
    }

    @PUT
    @Path("/project-url/{mnemonic}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public Response registerMetricToProjectByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl,
            @DefaultValue(DEFAULT_PURL) @PathParam("mnemonic") String mnemonic) {

        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (purl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("purl parameter must be specified");
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.NOT_FOUND).entity(errMsg).build();
        }
        // Look up the project in db
        Project project = findProjectByUrl(purl);
        if (project == null) {
            String errMsg = "Project not found with url: " + purl;
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.NOT_FOUND).entity(errMsg).build();
        }
        return registerMetric(project, mnemonic);
    }

    @PUT
    @Path("/project-all/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public Response registerMetricToProjectByName(
            @DefaultValue(DEFAULT_PURL) @PathParam("name") String name,
            @QueryParam("metric") List<String> metrics) {

        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (name.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("name path must be specified");
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.BAD_REQUEST).entity(errMsg).build();
        }
        // Look up the project in db
        Project project = findProjectByName(name);
        if (project == null) {
            String errMsg = "Project not found with name: " + name;
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.NOT_FOUND).entity(errMsg).build();
        }
        return registerMetrics(project, metrics);
    }

    private Response registerMetrics(Project project, List<String> mnemonics) {

        if (mnemonics == null || mnemonics.isEmpty()) {
            return Response.ok().build();
        }
        // Look up the metric if it was not found just
        // return a 404 response
        List<Metric> metrics = new ArrayList<>();
        for (String mnemonic : mnemonics) {
            Metric metric = metricManager.getMetricByMnemonic(mnemonic);
            if (metric == null) {
                String errMsg = "Metric with mnemonic: " + mnemonic + " not found";
                logger.log(Level.INFO, errMsg);
            }
            metrics.add(metric);
        }
        if (!metrics.isEmpty()) {
            metricManager.registerMetricToProject(project, metrics);
        }
        return Response.ok().build();
    }

    @PUT
    @Path("/project-all-url")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public Response registerMetricToProjectByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl,
            @QueryParam("metric") List<String> metrics) {
        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (purl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("purl parameter must be specified");
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.BAD_REQUEST).entity(errMsg).build();
        }
        // Look up the project in db
        Project project = findProjectByUrl(purl);
        if (project == null) {
            String errMsg = "Project not found with url: " + purl;
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.NOT_FOUND).entity(errMsg).build();
        }
        return registerMetrics(project, metrics);
    }

    @DELETE
    @Path("/project/{name}/{mnemonic}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public Response removeMetricFromProjectByBame(
            @DefaultValue(DEFAULT_PURL) @PathParam("name") String name,
            @DefaultValue(DEFAULT_PURL) @PathParam("mnemonic") String mnemonic) {

        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (name.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("name path must be specified");
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.BAD_REQUEST).entity(errMsg).build();
        }
        // Look up the project in db
        Project project = findProjectByName(name);
        if (project == null) {
            String errMsg = "Project not found with name: " + name;
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.NOT_FOUND).entity(errMsg).build();
        }
        return removeMetric(project, mnemonic);
    }

    private Response removeMetric(Project project, String mnemonic) {

        // If mnemonic is not specified
        // register all metrics to project
        if (mnemonic.endsWith(DEFAULT_PURL)) {
            metricManager.removeAllMetricsFromProject(project);
        } else {
            // Look up the metric if it was not found just
            // return a 404 response
            Metric metric = metricManager.getMetricByMnemonic(mnemonic);
            if (metric == null) {
                String errMsg = "Metric not found with mnemonic: " + mnemonic;
                logger.log(Level.INFO, errMsg);
                return Response.status(Response.Status.NOT_FOUND).entity(errMsg).build();
            }
            metricManager.removeMetricFromProject(project, metric);
        }
        return Response.ok().build();
    }

    @DELETE
    @Path("/project-url/{mnemonic}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public Response removeMetricFromProjectByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl,
            @DefaultValue(DEFAULT_PURL) @PathParam("mnemonic") String mnemonic) {

        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (purl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("purl parameter must be specified");
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.BAD_REQUEST).entity(errMsg).build();
        }
        // Look up the project in db
        Project project = findProjectByUrl(purl);
        if (project == null) {
            String errMsg = "Project not found with url: " + purl;
            logger.log(Level.INFO, errMsg);
            return Response.status(Response.Status.NOT_FOUND).entity(errMsg).build();
        }
        return removeMetric(project, mnemonic);
    }

    private Response removeMetrics(Project project, List<String> mnemonics) {

        // If no mnemonics are specified we consider as all the metrics
        // should be removed from this project.
        if (mnemonics == null || mnemonics.isEmpty()) {
            metricManager.removeAllMetricsFromProject(project);
            return Response.ok().build();
        }
        // Look up the metric if it was not found just
        // return a 404 response
        List<Metric> metrics = new ArrayList<>();
        for (String mnemonic : mnemonics) {
            Metric metric = metricManager.getMetricByMnemonic(mnemonic);
            if (metric == null) {
                notFoundException("Metric not found with mnemonic:" + mnemonic);
            }
            metrics.add(metric);
        }
        if (!metrics.isEmpty()) {
            metricManager.removeMetricFromProject(project, metrics);
        }
        return Response.ok().build();
    }

    @DELETE
    @Path("/project-all/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public Response removeMetricFromProjectByBame(
            @DefaultValue(DEFAULT_PURL) @PathParam("name") String name,
            @QueryParam("metric") List<String> metrics) {
        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (name.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("name path must be specified");
            illegalRequest(errMsg);
        }

        // Look up the project in db
        Project project = findProjectByName(name);
        if (project == null) {
            notFoundException("Project not found with name: " + name);
        }
        return removeMetrics(project, metrics);
    }

    @DELETE
    @Path("/project-all-url")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public Response removeMetricFromProjectByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl,
            @QueryParam("metric") List<String> metrics) {
        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (purl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("purl parameter must be specified");
            illegalRequest(errMsg);
        }
        // Look up the project in db
        Project project = findProjectByUrl(purl);
        if (project == null) {
            notFoundException("Project not found with url: " + purl);
        }
        return removeMetrics(project, metrics);
    }

    @DELETE
    @Path("/project-url/{category}")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public Response removeMetricsOfCategoryFromProjectByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl,
            @DefaultValue(DEFAULT_METRIC) @PathParam("category") String category) {

        if (purl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("purl parameter must be specified");
            illegalRequest(errMsg);
        }
        if (category.equals(DEFAULT_METRIC)) {
            String msg = "You should specify a metric category first";
            return Response.notModified(msg).build();
        }
        
        Collection<Metric> allMetrics = metricManager.getMetrics();
        List<String> metricsToRemove = new ArrayList<>();
        for (Metric m : allMetrics) {
            if (m.getCategory().getCategory().equals(category)) {
                metricsToRemove.add(m.getMnemonic());
            }
        }
        if (metricsToRemove.isEmpty()) {
            String msg = "No metric found from category " + category + ".";
            return Response.notModified(msg).build();
        }

        
        Project project = findProjectByUrl(purl);
        if (project == null) {
            notFoundException("Project not found with url: " + purl);
        }
        return removeMetrics(project, metricsToRemove);
    }

    @GET
    @Path("/classes/project")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getProjectClassesByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl,
            @DefaultValue(DEFAULT_VERSION) @QueryParam("versionID") String versionID) {

        if (purl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("purl path must be specified");
            illegalRequest(errMsg);
        }
        if (versionID.equals(DEFAULT_VERSION)) {
            String errMsg = getErrorResponseForProject("A version ID must be specified");
            illegalRequest(errMsg);
        }

        Project project = projectManager.findByUrl(purl);
        if (project == null) {
            notFoundException("Project not found with url: " + purl);
        }
        return getAllClassesForAProject(project, versionID);
    }

    @GET
    @Path("/project/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getProjectMetricsByName(
            @DefaultValue(DEFAULT_PURL) @PathParam("name") String name) {
        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (name.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("name path must be specified");
            illegalRequest(errMsg);
        }
        // Look up the project in db
        Project project = findProjectByName(name);
        if (project == null) {
            notFoundException("Project not found with name: " + name);
        }
        return getMetrics(project);
    }

    @GET
    @Path("/values/project/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getProjectMetricValuesByName(
            @DefaultValue(DEFAULT_PURL) @PathParam("name") String name) {
        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (name.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("name path must be specified");
            illegalRequest(errMsg);
        }
        // Look up the project in db
        Project project = findProjectByName(name);
        if (project == null) {
            notFoundException("Project not found with name: " + name);
        }
        return getAllMetricsAndValuesForAProject(project);
    }

    @GET
    @Path("/values/project")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getProjectMetricValuesByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl) {

        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (purl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("purl path must be specified");
            illegalRequest(errMsg);
        }
        // Look up the project in db

        Project project = projectManager.findByUrl(purl);
        if (project == null) {
            notFoundException("Project not found with url: " + purl);
        }
        return getAllMetricsAndValuesForAProject(project);
    }

    @GET
    @Path("/single/values/project")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getProjectSpecificMetricValuesByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl,
            @DefaultValue(DEFAULT_METRIC) @QueryParam("metricMnemonic") String metricMnemonic) {

        if (purl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("purl path must be specified");
            illegalRequest(errMsg);
        }
        if (purl.equals(DEFAULT_METRIC)) {
            String errMsg = getErrorResponseForProject("metricMnemonic must be specified");
            illegalRequest(errMsg);
        }
        Project project = projectManager.findByUrl(purl);
        if (project == null) {
            notFoundException("Project not found with url: " + purl);
        }
        return getSpecificMetricValuesForAProject(project, metricMnemonic);
    }

    @GET
    @Path("/single/values/project/forVersion")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getProjectSpecificMetricValuesForSpecificVersionByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl,
            @DefaultValue(DEFAULT_METRIC) @QueryParam("metricMnemonic") String metricMnemonic,
            @DefaultValue(DEFAULT_VERSION) @QueryParam("versionID") String versionID) {

        if (purl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("purl path must be specified");
            illegalRequest(errMsg);
        }
        if (metricMnemonic.equals(DEFAULT_METRIC)) {
            String errMsg = getErrorResponseForProject("metricMnemonic must be specified");
            illegalRequest(errMsg);
        }
        if (versionID.equals(DEFAULT_VERSION)) {
            String errMsg = getErrorResponseForProject("A version ID must be specified");
            illegalRequest(errMsg);
        }
        Project project = projectManager.findByUrl(purl);
        if (project == null) {
            notFoundException("Project not found with url: " + purl);
        }
        return getSpecificMetricValuesForAProjectAndForAVersion(project, metricMnemonic, versionID);
    }

    private Response getSpecificMetricValuesForAProjectAndForAVersion(Project project,
            String metricMnemonic, String versionID) {
        RESTProject restProject = createRestProject(project);
        // Get project versions and for each version get its metrics
        for (Version version : versionFacade.findByProject(project)) {
            if (version.getCommitID().equals(versionID)) {
                addNodeMetric(version, restProject, metricMnemonic);
            }
        }
        return Response.ok(restProject, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/single/values/project/forClass")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getProjectSpecificMetricValuesForSpecificClassByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl,
            @DefaultValue(DEFAULT_METRIC) @QueryParam("metricMnemonic") String metricMnemonic,
            @DefaultValue(DEFAULT_CLASS_NAME) @QueryParam("className") String className) {

        if (purl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("purl path must be specified");
            illegalRequest(errMsg);
        }
        if (metricMnemonic.equals(DEFAULT_METRIC)) {
            String errMsg = getErrorResponseForProject("metricMnemonic must be specified");
            illegalRequest(errMsg);
        }
        if (className.equals(DEFAULT_CLASS_NAME)) {
            String errMsg = getErrorResponseForProject("A fully qualified class name must be specified");
            illegalRequest(errMsg);
        }
        Project project = projectManager.findByUrl(purl);
        if (project == null) {
            notFoundException("Project not found with url: " + purl);
        }
        return getSpecificMetricValuesForAProjectAndForAClass(project, metricMnemonic, className);
    }

    @GET
    @Path("/values/forClass")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getAllMetricValuesForSpecificClass(
            @DefaultValue(DEFAULT_VERSION) @QueryParam("versionID") String versionID,
            @DefaultValue(DEFAULT_CLASS_NAME) @QueryParam("className") String className) {

        if (versionID.equals(DEFAULT_VERSION)) {
            String errMsg = getErrorResponseForProject("versionID must be specified");
            illegalRequest(errMsg);
        }
        if (className.equals(DEFAULT_CLASS_NAME)) {
            String errMsg = getErrorResponseForProject("A fully qualified class name must be specified");
            illegalRequest(errMsg);
        }
        List<Version> versions = versionFacade.findByCommitID(versionID);
        if (versions.isEmpty()) {
            notFoundException("No project found with version hash code: " + versionID);
        }
        return getAllMetricValuesForAClassForAVersion(className, versions.get(0));
    }

    private Response getAllMetricValuesForAClassForAVersion(String className, Version version) {
        RESTProject restProject = createRestProject(version.getProject());
        Collection<NodeVersionMetric> nodeVersionMetrics = version.getNodeVersionMetrics();
        RESTVersion restVersion = new RESTVersion();
        restVersion.setName(version.getName());
        restProject.addVersion(restVersion);
        for (NodeVersionMetric nvm : nodeVersionMetrics) {
            if (nvm.getNode().getName().equals(className)) {
                RESTMetricValue rmv = new RESTMetricValue(
                        nvm.getMetricValue().getMetric().getMnemonic(),
                        nvm.getMetricValue().getValue(),
                        nvm.getNode().getName());
                restVersion.addMetric(rmv);
            }
        }
        return Response.ok(restProject, MediaType.APPLICATION_JSON).build();
    }

    private Response getSpecificMetricValuesForAProjectAndForAClass(Project project,
            String metricMnemonic, String className) {
        RESTProject restProject = createRestProject(project);
        // Get project versions and for each version get its metrics
        for (Version version : versionFacade.findByProject(project)) {
            addNodeMetricForClass(version, restProject, metricMnemonic, className);
        }
        return Response.ok(restProject, MediaType.APPLICATION_JSON).build();
    }

    private void addNodeMetricForClass(Version version, RESTProject restProject,
            String metricMnemonic, String className) {
        RESTVersion restVersion = new RESTVersion(version.getName(), null);
        restProject.addVersion(restVersion);
        // Set node version metrics 
        for (NodeVersionMetric nvm : nodeVersionMetricFacade.findByVersion(version)) {
            MetricValue value = nvm.getMetricValue();
            String nodeName = nvm.getNode().getName();
            String mnemonic = value.getMetric().getMnemonic();
            if (mnemonic.equals(metricMnemonic) && nodeName.equalsIgnoreCase(className)) {
                RESTMetricValue mv = new RESTMetricValue(mnemonic, value.getValue(), nodeName);
                restVersion.addMetric(mv);
            }
        }
    }

    private Response getMetrics(Project project) {
        Collection<Metric> metrics = project.getRegisteredMetrics();
        return Response.ok(metrics, MediaType.APPLICATION_JSON).build();
    }

    private Response getAllMetricsAndValuesForAProject(Project project) {
        RESTProject restProject = createRestProject(project);
        // Get project versions and for each version get its metrics
        for (Version version : versionFacade.findByProject(project)) {
            addAllMetrics(version, restProject);
        }
        return Response.ok(restProject, MediaType.APPLICATION_JSON).build();
    }

    private Response getSpecificMetricValuesForAProject(Project project, String metricMnemonic) {
        RESTProject restProject = createRestProject(project);
        // Get project versions and for each version get its metrics
        for (Version version : versionFacade.findByProject(project)) {
            addMetric(version, restProject, metricMnemonic);
        }
        return Response.ok(restProject, MediaType.APPLICATION_JSON).build();
    }

    private void addAllMetrics(Version version, RESTProject restProject) {
        RESTVersion restVersion = new RESTVersion(version.getName(), null);
        restProject.addVersion(restVersion);
        // Set project version metrics (such as repo metrics)
        for (ProjectVersionMetric pvm : versionMetricFacade.findByVersion(version)) {
            MetricValue value = pvm.getMetricValue();
            String mnemonic = value.getMetric().getMnemonic();
            RESTMetricValue mv = new RESTMetricValue(mnemonic, value.getValue());
            restVersion.addMetric(mv);
        }
    }

    private Response getAllClassesForAProject(Project project, String versionID) {
        RESTProject restProject = createRestProject(project);
        List<Version> version = versionFacade.findByCommitID(project, versionID);
        for (NodeVersionMetric nvm : nodeVersionMetricFacade.findByVersion(version.get(0))) {
            MetricValue value = nvm.getMetricValue();
            String nodeName = nvm.getNode().getName();
            String mnemonic = value.getMetric().getMnemonic();
            if (mnemonic.equals(NodeAge.MNEMONIC)) {
                RESTMetricValue mv = new RESTMetricValue(mnemonic, value.getValue(), nodeName);
                restProject.addMetric(mv);
            }
        }
        return Response.ok(restProject, MediaType.APPLICATION_JSON).build();
    }

    private void addNodeMetric(Version version, RESTProject restProject, String metricMnemonic) {
        RESTVersion restVersion = new RESTVersion(version.getName(), null);
        restProject.addVersion(restVersion);
        // Set node version metrics 
        for (NodeVersionMetric nvm : nodeVersionMetricFacade.findByVersion(version)) {
            MetricValue value = nvm.getMetricValue();
            String nodeName = nvm.getNode().getName();
            String mnemonic = value.getMetric().getMnemonic();
            if (mnemonic.equals(metricMnemonic)) {
                RESTMetricValue mv = new RESTMetricValue(mnemonic, value.getValue(), nodeName);
                restVersion.addMetric(mv);
            }
        }
    }

    private void addMetric(Version version, RESTProject restProject, String metricMnemonic) {
        RESTVersion restVersion = new RESTVersion(version.getName(), null);
        restProject.addVersion(restVersion);
        // Set project version metrics (such as repo metrics)
        for (ProjectVersionMetric pvm : versionMetricFacade.findByVersion(version)) {
            MetricValue value = pvm.getMetricValue();
            String mnemonic = value.getMetric().getMnemonic();
            if (mnemonic.equals(metricMnemonic)) {
                RESTMetricValue mv = new RESTMetricValue(mnemonic, value.getValue());
                restVersion.addMetric(mv);
            }
        }
    }

    private RESTProject createRestProject(Project project) {
        // Create a rest project to return to client
        RESTProject restProject = new RESTProject(project.getName(),
                project.getRemoteRepoPath(), null, null);
        // Get project metrics first
        for (ProjectMetric pm : projectMetricFacade.findByProject(project)) {
            MetricValue value = pm.getMetricValue();
            String mnemonic = value.getMetric().getMnemonic();
            RESTMetricValue mv = new RESTMetricValue(mnemonic, value.getValue());
            restProject.addMetric(mv);
        }
        return restProject;
    }

    @GET
    @Path("/project-url")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getProjectMetricsByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String url) {

        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (url.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("'purl' parameter must be specified");
            illegalRequest(errMsg);
        }
        // Look up the project in db
        Project project = findProjectByUrl(url);
        if (project == null) {
            notFoundException("Project not found with url: " + url);
        }
        return getMetrics(project);
    }

    @GET
    @Path("/category/{mnemonic}")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getMetricCategories(
            @DefaultValue(DEFAULT_PURL) @PathParam("mnemonic") String metricMnemonic) {

        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (metricMnemonic.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("mnemonic path must be specified");
            illegalRequest(errMsg);
        }
        Metric metric = metricManager.getMetricByMnemonic(metricMnemonic);
        if (metric == null) {
            notFoundException("Metric not found with mnemonic: " + metricMnemonic);
        }
        MetricCategory category = metric.getCategory();
        if (category == null) {
            notFoundException("Category not found for metric with mnemonic: "
                    + metricMnemonic);
        }
        return Response.ok(category, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/language/{mnemonic}")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getMetricLanguages(
            @DefaultValue(DEFAULT_PURL) @PathParam("mnemonic") String metricMnemonic) {
        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (metricMnemonic.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("'mnemonic' path must be specified");
            illegalRequest(errMsg);
        }
        Metric metric = metricManager.getMetricByMnemonic(metricMnemonic);
        if (metric == null) {
            notFoundException("Metric not found with mnemonic: " + metricMnemonic);
        }
        Collection<ProgrammingLanguage> languages = metricManager
                .getMetricLanguages(metricMnemonic);
        if (languages == null) {
            languages = new ArrayList<ProgrammingLanguage>();
        }
        return Response.ok(languages, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getMetrics() {
        Collection<Metric> metrics = metricManager.getMetrics();
        if (metrics == null) {
            metrics = new ArrayList<Metric>();
        }
        return Response.ok(metrics, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/category")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getCategories() {
        Collection<MetricCategory> categories = metricManager.getCategories();
        if (categories == null) {
            categories = new ArrayList<MetricCategory>();
        }

        return Response.ok(categories, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/language")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getLanguages() {
        Collection<ProgrammingLanguage> languages = metricManager.getLanguages();
        if (languages == null) {
            languages = new ArrayList<ProgrammingLanguage>();
        }

        return Response.ok(languages, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/evolution")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getProjectMetricEvolution(
            @DefaultValue(DEFAULT_METRIC) @QueryParam("metricMnemonic") String metricMnemonic,
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl) {

        if (metricMnemonic.equals(DEFAULT_METRIC)) {
            String errMsg = getErrorResponseForProject("a metric mnemonic must be specified");
            illegalRequest(errMsg);
        }
        if (purl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("purl path must be specified");
            illegalRequest(errMsg);
        }

        Project project = projectManager.findByUrl(purl);
        if (project == null) {
            notFoundException("Project not found with url: " + purl);
        }
        Map<Version, MetricValue> metricEvolution = getMetricEvolution(metricMnemonic, project);
        RESTProject restProject = createRestProjectFromMetricValues(metricEvolution);

        return Response.ok(restProject, MediaType.APPLICATION_JSON).build();
    }

    private RESTProject createRestProjectFromMetricValues(Map<Version, MetricValue> versionToValuesMap) {
        RESTProject restProject = new RESTProject();

        for (Version version : versionToValuesMap.keySet()) {
            MetricValue metricValue = versionToValuesMap.get(version);
            RESTVersion restVersion = new RESTVersion(version.getName());
            restVersion.addMetric(new RESTMetricValue(metricValue.getMetric().getMnemonic(), metricValue.getValue()));
            restProject.addVersion(restVersion);
        }
        return restProject;
    }

    public Map<Version, MetricValue> getMetricEvolution(String metricMnemonic, Project project) {
        Map<Version, MetricValue> metricValues = new HashMap<>();
        Collection<Version> versions = project.getVersions();
        for (Version version : versions) {
            Collection<ProjectVersionMetric> projectVersionMetrics = version.getProjectVersionMetrics();
            for (ProjectVersionMetric prVerMetric : projectVersionMetrics) {
                if (prVerMetric.getMetricValue().getMetric().getMnemonic().equals(metricMnemonic)) {
                    metricValues.put(version, prVerMetric.getMetricValue());
                }
            }
        }
        return metricValues;
    }

}
