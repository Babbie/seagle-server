package gr.uom.java.seagle.v2.analysis.graph.javaGraph;

import gr.uom.java.seagle.v2.analysis.graph.EdgeCreationStrategy;
import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.java.seagle.v2.analysis.graph.GraphFactory;
import gr.uom.java.seagle.v2.analysis.graph.SoftwareGraph;
import gr.uom.java.seagle.v2.analysis.project.evolution.JavaProject;
import java.util.Set;

/**
 *
 * @author Theodore Chaikalis
 */
public class JavaGraphFactory implements GraphFactory {

    private final SystemObject systemObject;
    private final JavaProject javaProject;

    public JavaGraphFactory(SystemObject systemObject, JavaProject javaProject) {
        this.systemObject = systemObject;
        this.javaProject = javaProject;
    }

    /*
     * Returns the classes in ClassObject List as Graph vertices with NO edges between them.
     */
    @Override
    public SoftwareGraph getSystemGraph(EdgeCreationStrategy edgeCreationStrategy) {
        JavaClassGraph systemGraph = new JavaClassGraph();
        Set<ClassObject> classObjects = systemObject.getClassObjects();
        for (ClassObject classObject : classObjects) {
            String packageName = classObject.getPackageName();
            JavaPackage classPackage = systemGraph.getPackage(packageName);
            if (classPackage == null) {
                classPackage = new JavaPackage(packageName);
                systemGraph.addPackage(classPackage);
            }
            JavaNode node = new JavaNode(classObject.getName(), classPackage);
            node.setAbstract(classObject.isAbstract());
            node.setInterface(classObject.isInterface());
            String classPath = javaProject.getPathOfClass(classObject.getName());
            if(classPath == null){
                throw new RuntimeException("classPath is null. Cannot resolve Classpath from JavaProject");
            }
            node.setSourceFilePath(classPath);
            systemGraph.addVertex(node);
        }
        for (ClassObject classObject : classObjects) {
            JavaNode aNode = systemGraph.getNode(classObject.getName());
            if (classObject.getSuperclass() != null) {
                ClassObject superClass = systemObject.getClassObject(classObject.getSuperclass().toString());
                if (superClass != null) {
                    try {
                        JavaNode parentNode = systemGraph.getNode(superClass.getName());
                        if (parentNode != null) {
                            aNode.setParent(parentNode);
                        }
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
        edgeCreationStrategy.createEdges(systemGraph, systemObject);
        return systemGraph;
    }

}
