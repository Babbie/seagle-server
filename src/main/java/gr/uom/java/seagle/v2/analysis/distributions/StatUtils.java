package gr.uom.java.seagle.v2.analysis.distributions;

import java.util.Map;

/**
 *
 * @author Theodore Chaikalis
 */
public class StatUtils {

    public static void updateFrequencyMap(Map<Integer, Integer> map, int key) {
        if (!map.containsKey(key)) {
            map.put(key, 1);
        } else {
            map.put(key, map.get(key) + 1);
        }
    }

}
