package gr.uom.java.seagle.v2.db.persistence;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 * An internal entity used as a composed primary key for
 * {@link NodeVersionMetric}.
 * <p>
 * @author Elvis Ligu
 */
@Embeddable
public class NodeVersionMetricPK implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8470223034003567876L;


    private Long node;

    private Long version;

    private Long metricValue;

    public NodeVersionMetricPK() {
    }


    public Long getNode() {
        return node;
    }

    public Long getVersion() {
        return version;
    }

    public Long getMetricValue() {
        return metricValue;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += node.hashCode();
        hash += version.hashCode();
        hash += metricValue.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof NodeVersionMetricPK)) {
            return false;
        }
        NodeVersionMetricPK other = (NodeVersionMetricPK) object;
        return this.node.equals(other.node) && this.version.equals(other.version) && this.metricValue.equals(other.metricValue);
    }

    @Override
    public String toString() {
        return "gr.uom.java.seagle.db.persistence.v2.NodeVersionMetricPK[ nodeId=" + node + ", versionId=" + version + " ]";
    }

}
