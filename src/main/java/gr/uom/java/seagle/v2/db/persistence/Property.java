/**
 * 
 */
package gr.uom.java.seagle.v2.db.persistence;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Elvis Ligu
 */
@Entity
@Table(name = "property")
@NamedQueries({
   @NamedQuery(name = "Property.findByIdVal", query = "SELECT p.value FROM Property p WHERE p.id = :id"),
   @NamedQuery(name = "Property.findById", query = "SELECT p FROM Property p WHERE p.id = :id"),
   @NamedQuery(name = "Property.findAll", query = "SELECT p FROM Property p"),
   @NamedQuery(name = "Property.findByName", query = "SELECT p FROM Property p WHERE p.name = :name"),
   @NamedQuery(name = "Property.findByDomain", query = "SELECT p FROM Property p WHERE p.domain = :domain"),
   @NamedQuery(name = "Property.deleteByDomain", query = "DELETE FROM Property p WHERE p.domain = :domain"),
   @NamedQuery(name = "Property.findByDomainAndName", query = "SELECT p FROM Property p WHERE p.domain = :domain AND p.name = :name"),
   @NamedQuery(name = "Property.findByDomainAndNameVal", query = "SELECT p.value FROM Property p WHERE p.domain = :domain AND p.name = :name"),
   @NamedQuery(name = "Property.findByDomainAndPrefixName", query = "SELECT p FROM Property p WHERE p.domain = :domain AND p.name LIKE :prefix"),
    })
public class Property implements Serializable {

   /**
    * 
    */
   private static final long serialVersionUID = 2694820885006935359L;

   @Basic(optional = false)
   private String domain;
   
   @Basic(optional = false)
   @Column(length = 4096)
   private String name;
   
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 4096)
   @Column(name = "value")
   private String value;
   
   @Id
   @Column(name = "id", length = 64)
   String id;
   
   /**
    * @return the id
    */
   public String getId() {
      return id;
   }
   
   /**
    * Do not set this, it will be generated.
    * @param id the id to set
    */
   public void setId(String id) {
      this.id = id;
   }
   
   /**
    * 
    */
   public Property() {
   }

   
   public String getDomain() {
      return domain;
   }


   public void setDomain(String domain) {
      this.domain = domain;
   }


   public String getName() {
      return name;
   }


   public void setName(String name) {
      this.name = name;
   }


   public String getValue() {
      return value;
   }


   public void setValue(String value) {
      this.value = value;
   }

   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((domain == null) ? 0 : domain.hashCode());
      result = prime * result + ((name == null) ? 0 : name.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      Property other = (Property) obj;
      if (domain == null) {
         if (other.domain != null)
            return false;
      } else if (!domain.equals(other.domain))
         return false;
      if (name == null) {
         if (other.name != null)
            return false;
      } else if (!name.equals(other.name))
         return false;
      return true;
   }

   @Override
   public String toString() {
      return "Property [domain=" + domain + ", name=" + name + "]";
   }
}
