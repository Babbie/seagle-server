package gr.uom.java.seagle.v2.analysis.dpd;

import gr.uom.se.util.event.DefaultEvent;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventType;

/**
 *
 * @author Daniel Feitosa
 */
public enum DPDEventType implements EventType {
    /**
     * Request to execute DPD for a project.
     */
    DPD_ANALYSIS_REQUESTED,
    
    /**
     * Inform that DPD has finished running for a project.
     */
    DPD_ANALYSIS_ENDED,
    
    /**
     * Request to delete all DPD data (DB and files) generated for a project.
     */
    DPD_CLEAN_REQUESTED,
    
    /**
     * Inform that DPD data (DB and files) has been deleted.
     */
    DPD_CLEAN_ENDED;

    public Event newEvent(EventInfo info) {
        return new DefaultEvent(this, info);
    }
}
