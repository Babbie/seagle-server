package gr.uom.java.ast;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jdt.core.dom.TypeDeclaration;

public class ClassObject {

    private String name;
    private List<ConstructorObject> constructorList;
    private List<MethodObject> methodList;
    private List<FieldObject> fieldList;
    private TypeObject superclass;
    private List<TypeObject> interfaceList;
    private boolean _abstract;
    private boolean _interface;
    private boolean _static;
    private Access access;
    private TypeDeclaration typeDeclaration;
    private int linesOfCode;

    public ClassObject() {
        this.constructorList = new ArrayList<ConstructorObject>();
        this.methodList = new ArrayList<MethodObject>();
        this.interfaceList = new ArrayList<TypeObject>();
        this.fieldList = new ArrayList<FieldObject>();
        this._abstract = false;
        this._interface = false;
        this._static = false;
        this.access = Access.NONE;
        linesOfCode = 0;
    }

    public void setTypeDeclaration(TypeDeclaration typeDeclaration) {
        this.typeDeclaration = typeDeclaration;
        //this.typeDeclaration = ASTInformationGenerator.generateASTInformation(typeDeclaration);
    }

    public TypeDeclaration getTypeDeclaration() {
        return this.typeDeclaration;
    }

    public boolean isFriend(String className) {
        if (superclass != null) {
            if (superclass.getClassType().equals(className)) {
                return true;
            }
        }
        /*for(TypeObject interfaceType : interfaceList) {
         if(interfaceType.getClassType().equals(className))
         return true;
         }*/
        for (FieldObject field : fieldList) {
            TypeObject fieldType = field.getType();
            if (checkFriendship(fieldType, className)) {
                return true;
            }
        }
        for (ConstructorObject constructor : constructorList) {
            ListIterator<ParameterObject> parameterIterator = constructor.getParameterListIterator();
            while (parameterIterator.hasNext()) {
                ParameterObject parameter = parameterIterator.next();
                TypeObject parameterType = parameter.getType();
                if (checkFriendship(parameterType, className)) {
                    return true;
                }
            }
            for (CreationObject creation : constructor.getCreations()) {
                TypeObject creationType = creation.getType();
                if (checkFriendship(creationType, className)) {
                    return true;
                }
            }
        }
        for (MethodObject method : methodList) {
            TypeObject returnType = method.getReturnType();
            if (checkFriendship(returnType, className)) {
                return true;
            }
            ListIterator<ParameterObject> parameterIterator = method.getParameterListIterator();
            while (parameterIterator.hasNext()) {
                ParameterObject parameter = parameterIterator.next();
                TypeObject parameterType = parameter.getType();
                if (checkFriendship(parameterType, className)) {
                    return true;
                }
            }
            for (CreationObject creation : method.getCreations()) {
                TypeObject creationType = creation.getType();
                if (checkFriendship(creationType, className)) {
                    return true;
                }
            }
        }

        /*if(superclass != null) {
         ClassObject superclassObject = ASTReader.getSystemObject().getClassObject(superclass.getClassType());
         if(superclassObject != null){
         boolean result = superclassObject.isFriend(className);
         return result;
         }
         }*/
        return false;
    }

    public String isFriend2(String className) {
        if (superclass != null) {
            if (superclass.getClassType().equals(className)) {
                return "extends";
            }
        }
        /*for(TypeObject interfaceType : interfaceList) {
         if(interfaceType.getClassType().equals(className))
         return true;
         }*/
        for (FieldObject field : fieldList) {
            TypeObject fieldType = field.getType();
            if (checkFriendship(fieldType, className)) {
                return "field";
            }
        }
        for (ConstructorObject constructor : constructorList) {
            ListIterator<ParameterObject> parameterIterator = constructor.getParameterListIterator();
            while (parameterIterator.hasNext()) {
                ParameterObject parameter = parameterIterator.next();
                TypeObject parameterType = parameter.getType();
                if (checkFriendship(parameterType, className)) {
                    return "parameter, constructor";
                }
            }
            for (CreationObject creation : constructor.getCreations()) {
                TypeObject creationType = creation.getType();
                if (checkFriendship(creationType, className)) {
                    return "creation, constructor";
                }
            }
        }
        for (MethodObject method : methodList) {
            TypeObject returnType = method.getReturnType();
            if (checkFriendship(returnType, className)) {
                return "return Type of method";
            }
            ListIterator<ParameterObject> parameterIterator = method.getParameterListIterator();
            while (parameterIterator.hasNext()) {
                ParameterObject parameter = parameterIterator.next();
                TypeObject parameterType = parameter.getType();
                if (checkFriendship(parameterType, className)) {
                    return "parameter in method";
                }
            }
            for (CreationObject creation : method.getCreations()) {
                TypeObject creationType = creation.getType();
                if (checkFriendship(creationType, className)) {
                    return "creation inside method";
                }
            }
        }

        /*if(superclass != null) {
         ClassObject superclassObject = ASTReader.getSystemObject().getClassObject(superclass.getClassType());
         if(superclassObject != null){
         boolean result = superclassObject.isFriend(className);
         return result;
         }
         }*/
        return null;
    }

    public String isFriend3(String className) {
        StringBuilder sb = new StringBuilder();
        if (superclass != null) {
            if (superclass.getClassType().equals(className)) {
                sb.append("extends  ");
            }
        }
        /*for(TypeObject interfaceType : interfaceList) {
         if(interfaceType.getClassType().equals(className))
         return true;
         }*/
        for (FieldObject field : fieldList) {
            TypeObject fieldType = field.getType();
            if (checkFriendship(fieldType, className)) {
                sb.append("field  ");
            }
        }
        for (ConstructorObject constructor : constructorList) {
            ListIterator<ParameterObject> parameterIterator = constructor.getParameterListIterator();
            while (parameterIterator.hasNext()) {
                ParameterObject parameter = parameterIterator.next();
                TypeObject parameterType = parameter.getType();
                if (checkFriendship(parameterType, className)) {
                    sb.append("parameter, constructor  ");
                }
            }
            for (CreationObject creation : constructor.getCreations()) {
                TypeObject creationType = creation.getType();
                if (checkFriendship(creationType, className)) {
                    sb.append("creation, constructor  ");
                }
            }
        }
        for (MethodObject method : methodList) {
            TypeObject returnType = method.getReturnType();
            if (checkFriendship(returnType, className)) {
                sb.append("return Type of method  ");
            }
            ListIterator<ParameterObject> parameterIterator = method.getParameterListIterator();
            while (parameterIterator.hasNext()) {
                ParameterObject parameter = parameterIterator.next();
                TypeObject parameterType = parameter.getType();
                if (checkFriendship(parameterType, className)) {
                    sb.append("parameter in method  ");
                }
            }
            for (CreationObject creation : method.getCreations()) {
                TypeObject creationType = creation.getType();
                if (checkFriendship(creationType, className)) {
                    sb.append("creation inside method ");
                }
            }
        }

        /*if(superclass != null) {
         ClassObject superclassObject = ASTReader.getSystemObject().getClassObject(superclass.getClassType());
         if(superclassObject != null){
         boolean result = superclassObject.isFriend(className);
         return result;
         }
         }*/
        if (sb.toString().length() != 0) {
            return sb.toString();
        } else {
            return null;
        }
    }

    private boolean checkFriendship(TypeObject type, String className) {
        try {
            if (type.getClassType().equals(className)) {
                return true;
            }
            if (type.getGenericType() != null && type.getGenericType().contains(className)) {
                return true;
            }

        } catch (Exception e) {
            System.out.println("" + e);
        }
        return false;
    }

    public boolean containsMethodWithTestAnnotation() {
        for (MethodObject method : methodList) {
            if (method.hasTestAnnotation()) {
                return true;
            }
        }
        return false;
    }

    public MethodObject getMethod(MethodInvocationObject mio) {
        ListIterator<MethodObject> mi = getMethodIterator();
        while (mi.hasNext()) {
            MethodObject mo = mi.next();
            if (mo.equals(mio)) {
                return mo;
            }
        }
        return null;
    }

    public MethodObject getMethod(SuperMethodInvocationObject smio) {
        ListIterator<MethodObject> mi = getMethodIterator();
        while (mi.hasNext()) {
            MethodObject mo = mi.next();
            if (mo.equals(smio)) {
                return mo;
            }
        }
        return null;
    }

    public boolean hasFieldType(String className) {
        ListIterator<FieldObject> fi = getFieldIterator();
        while (fi.hasNext()) {
            FieldObject fo = fi.next();
            if (fo.getType().getClassType().equals(className)) {
                return true;
            }
        }
        return false;
    }

    public void setAccess(Access access) {
        this.access = access;
    }

    public Access getAccess() {
        return access;
    }

    public void setSuperclass(TypeObject superclass) {
        this.superclass = superclass;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean addMethod(MethodObject method) {
        return methodList.add(method);
    }

    public boolean addInterface(TypeObject i) {
        return interfaceList.add(i);
    }

    public boolean addConstructor(ConstructorObject c) {
        return constructorList.add(c);
    }

    public boolean addField(FieldObject f) {
        return fieldList.add(f);
    }

    public ListIterator<ConstructorObject> getConstructorIterator() {
        return constructorList.listIterator();
    }

    public ListIterator<MethodObject> getMethodIterator() {
        return methodList.listIterator();
    }

    public ListIterator<TypeObject> getInterfaceIterator() {
        return interfaceList.listIterator();
    }

    public ListIterator<TypeObject> getSuperclassIterator() {
        List<TypeObject> superclassList = new ArrayList<TypeObject>(interfaceList);
        superclassList.add(superclass);
        return superclassList.listIterator();
    }

    public ListIterator<FieldObject> getFieldIterator() {
        return fieldList.listIterator();
    }

    public int getNumberOfFields() {
        return fieldList.size();
    }

    public Set<FieldObject> getFieldsAccessedInsideMethod(MethodObject method) {
        Set<FieldObject> fields = new LinkedHashSet<FieldObject>();
        for (FieldInstructionObject fieldInstruction : method.getFieldInstructions()) {
            for (FieldObject field : fieldList) {
                if (field.equals(fieldInstruction)) {
                    if (!fields.contains(field)) {
                        fields.add(field);
                    }
                    break;
                }
            }
        }
        return fields;
    }

    public String getName() {
        return name;
    }

    public TypeObject getSuperclass() {
        return superclass;
    }

    public void setAbstract(boolean abstr) {
        this._abstract = abstr;
    }

    public boolean isAbstract() {
        return this._abstract;
    }

    public void setInterface(boolean i) {
        this._interface = i;
    }

    public boolean isInterface() {
        return this._interface;
    }

    public boolean isStatic() {
        return _static;
    }

    public void setStatic(boolean s) {
        _static = s;
    }

    public boolean implementsInterface(String i) {
        return interfaceList.contains(i);
    }

    public int getNumberOfMethods() {
        return methodList.size();
    }

    public int getLinesOfCode() {
        return linesOfCode;
    }

    public void setLinesOfCode(int linesOfCode) {
        this.linesOfCode = linesOfCode;
    }

    public List<MethodObject> getAccessorMethods() {
        List<MethodObject> accessors = new ArrayList<>();
        for (MethodObject method : methodList) {
            if (method.isStatic() == false && method.getMethodBody() != null) {
                FieldInstructionObject fieldInstruction = method.isGetter();
                if (fieldInstruction != null) {
                    accessors.add(method);
                }
                if (fieldInstruction == null) {
                    fieldInstruction = method.isSetter();
                    if (fieldInstruction != null) {
                        accessors.add(method);
                    }
                }
                if (fieldInstruction == null) {
                    fieldInstruction = method.isCollectionAdder();
                    if (fieldInstruction != null) {
                        accessors.add(method);
                    }
                }
            }
        }
        return accessors;
    }

    public List<MethodObject> getNonAccessorMethods() {
        List<MethodObject> nonAccessorMethods = new ArrayList<>();
        Set<MethodObject> accessorSet = new HashSet<>(getAccessorMethods());
        for (MethodObject method : methodList) {
            if (method.isStatic() == false && method.getMethodBody() != null) {
                if (!accessorSet.contains(method)) {
                    nonAccessorMethods.add(method);
                }
            }
        }
        return nonAccessorMethods;
    }

    public int getNumberOfPublicMembers() {
        return getNumberOfPublicMethods() + getNumberOfPublicAttributes();
    }

    public int getNumberOfPublicMethods() {
        int publicMethods = 0;
        for (MethodObject method : methodList) {
            if (method.getAccess().equals(Access.PUBLIC)) {
                publicMethods++;
            }
        }
        return publicMethods;
    }

    public int getNumberOfPublicAttributes() {
        int publicAttributes = 0;
        for (FieldObject field : fieldList) {
            if (field.getAccess().equals(Access.PUBLIC)) {
                publicAttributes++;
            }
        }
        return publicAttributes;
    }

    public int getNumberOfMethodsThatAccessAField() {
        int totalFieldAccesses = 0;
        for (FieldObject field : getNonStaticFields()) {
            int mA = 0;  //mA is the number of methods that access a variable (attribute)
            for (MethodObject method : getNonAccessorMethods()) {
                boolean found = false;
                List<FieldInstructionObject> fieldInstructions = method.getFieldInstructions();
                for (FieldInstructionObject fieldInstruction : fieldInstructions) {
                    if (field.equals(fieldInstruction)) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    mA++;
                } else {
                    List<MethodInvocationObject> methodInvocations = method.getMethodInvocations();
                    for (MethodInvocationObject methodInvocation : methodInvocations) {
                        if (methodInvocation.getOriginClassName().equals(getName())) {
                            MethodObject methodObject = getMethod(methodInvocation);
                            if (getAccessorMethods().contains(methodObject)) {
                                List<FieldInstructionObject> accessorFieldInstructions = methodObject.getFieldInstructions();
                                FieldInstructionObject accessorFieldInstruction = accessorFieldInstructions.get(0);
                                if (field.equals(accessorFieldInstruction)) {
                                    mA++;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            totalFieldAccesses += mA;
        }
        return totalFieldAccesses;
    }

    public List<FieldObject> getNonStaticFields() {
        List<FieldObject> fields = new ArrayList<>();
        ListIterator<FieldObject> fieldIterator = this.getFieldIterator();
        while (fieldIterator.hasNext()) {
            FieldObject field = fieldIterator.next();
            if (!field.isStatic()) {
                fields.add(field);
            }
        }
        return fields;
    }

    public List<ClassObject> getClassesOfOutNeighbourhood(SystemObject system) {
        List<ClassObject> externalNeighbours = new ArrayList<>();
        ListIterator<ClassObject> otherClasses = system.getClassListIterator();
        while (otherClasses.hasNext()) {
            ClassObject otherClass = otherClasses.next();
            if (!otherClass.name.equals(this.name)) {
                if (this.isFriend(otherClass.name)) {
                    externalNeighbours.add(otherClass);
                }
            }
        }
        return externalNeighbours;
    }

    public List<FieldObject> getFieldsThatReferenceForeignClasses(SystemObject system) {
        List<ClassObject> classesOfOutNeighbourhood = getClassesOfOutNeighbourhood(system);
        List<FieldObject> fieldsThatReferenceForeignClasses = new ArrayList<>();
        for (FieldObject field : fieldList) {
            if (field.getType().getClassType().equals(this.name)) {
                continue;
            }
            for (ClassObject co : classesOfOutNeighbourhood) {
                if (field.getType().getClassType().equals(co.name)) {
                    fieldsThatReferenceForeignClasses.add(field);
                }
            }
        }
        return fieldsThatReferenceForeignClasses;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (!access.equals(Access.NONE)) {
            sb.append(access.toString()).append(" ");
        }
        if (_static) {
            sb.append("static").append(" ");
        }
        if (_interface) {
            sb.append("interface").append(" ");
        } else if (_abstract) {
            sb.append("abstract class").append(" ");
        } else {
            sb.append("class").append(" ");
        }
        sb.append(name).append(" ");
        sb.append("extends ").append(superclass);
        if (!interfaceList.isEmpty()) {
            sb.append(" ").append("implements ");
            for (int i = 0; i < interfaceList.size() - 1; i++) {
                sb.append(interfaceList.get(i)).append(", ");
            }
            sb.append(interfaceList.get(interfaceList.size() - 1));
        }
        return sb.toString();
    }

    public String getPackageName() {
        if (name.lastIndexOf(".") != -1) {
            String possibleName = this.name.substring(0, name.lastIndexOf("."));
            Pattern p = Pattern.compile("[A-Z]");
            Matcher m = p.matcher(possibleName);
            if (m.find() && possibleName.contains(".")) {
                possibleName = possibleName.substring(0, possibleName.lastIndexOf("."));
            }
            return possibleName;
        }
        return "defaultPackage";
    }

    public Iterable<MethodObject> getMethodList() {
        return methodList;
    }

    public List<ConstructorObject> getConstructorList() {
        return constructorList;
    }

    private List<ConstructorObject> getAllMethodsPlusConstructors() {
        List<ConstructorObject> constructorObjects = getConstructorList();
        for (MethodObject method : getMethodList()) {
            constructorObjects.add(method.getConstructorObject());
        }
        return constructorObjects;
    }


    

}
