/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.analysis.graph.evolution;

import static org.junit.Assert.assertNotNull;
import gr.uom.java.seagle.v2.analysis.AnalysisEventType;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.ws.rest.project.AnalysisRequestor;
import gr.uom.se.util.event.DefaultEvent;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.vcs.VCSBranch;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.analysis.version.provider.BranchTagProvider;
import gr.uom.se.vcs.analysis.version.provider.ConnectedTagVersionNameProvider;
import gr.uom.se.vcs.analysis.version.provider.TagProvider;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;

import java.util.Collection;
import java.util.Date;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.junit.Test;

/**
 *
 * @author teo
 */
public class EvolutionAnalysisTest extends AbstractGraphMetricTest {

   private static final Logger logger = Logger
         .getLogger(EvolutionAnalysisTest.class.getName());

   @EJB
   AnalysisRequestor analysisRequestorService;

   public EvolutionAnalysisTest() {
      super();
   }

   @Test
   public void testEvolutionCalculation() throws VCSRepositoryException {
      Collection<Metric> previousMetrics = metricManager.getMetrics();
      try {
         // Clone the project first
         projectManager.clone(demoUrl);
         Project project = projectManager.findByUrl(demoUrl);
         assertNotNull(project);
         // Remove all metrics so if there is a problem to other metrics
         // this test case will be not affected
         metricManager.removeAllMetricsFromProject(project);

         // Before triggering the event make sure to register only the
         // metric we are testing so it will accept the event, otherwise
         // the listener will take no event at all
         // I.E String mnemonic = "my_metric";
         // Metric mnemonic = metricManager.getMetricByMnemonic(mnemonic);
         // metricManager.registerMetricToProject(project, metric);
         // now we are ready to trigger the event so only our metric will get
         // the
         // metric START event.

         // Now trigger artificial event
         trigerArtificialEvent();

         // This is unnecessary unless we have other tests that relies on
         // registered metrics
         metricManager.registerMetricToProject(project, previousMetrics);
      } finally {
         cleanUp();
      }
   }

   private void trigerArtificialEvent() throws VCSRepositoryException {
      AnalysisRequestInfo info = new AnalysisRequestInfo("teo@haik.com",
            demoUrl);
      Event event = new DefaultEvent(AnalysisEventType.ANALYSIS_REQUESTED,
            new EventInfo(info, new Date()));
      eventManager.trigger(event);
   }
}
