/**
 *
 */
package gr.uom.java.seagle.v2.metric.repo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import gr.uom.java.seagle.v2.AbstractSeagleTest;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisEventType;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.ProjectMetric;
import gr.uom.java.seagle.v2.db.persistence.Property;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectMetricFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.PropertyFacade;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.metric.MetricManager;
import gr.uom.java.seagle.v2.metric.MetricRunInfo;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.se.util.context.ContextManager;
import gr.uom.se.util.event.DefaultEvent;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.module.ModuleContext;
import gr.uom.se.vcs.VCSBranch;
import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.analysis.version.provider.BranchTagProvider;
import gr.uom.se.vcs.analysis.version.provider.ConnectedTagVersionNameProvider;
import gr.uom.se.vcs.analysis.version.provider.ConnectedVersionProvider;
import gr.uom.se.vcs.analysis.version.provider.TagProvider;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.concurrent.ManagedExecutorService;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Elvis Ligu
 */
public class CommitCounterTaskTest extends AbstractSeagleTest {

    public static final String purl1
            = "https://github.com/fernandezpablo85/scribe-java.git";
    private static final Logger logger = Logger
            .getLogger(CommitCounterTaskTest.class.getName());

    @EJB
    ProjectManager projectManager;

    @EJB
    MetricManager metricManager;

    @EJB
    EventManager eventManager;

    @EJB
    PropertyFacade properties;

    @EJB
    SeagleManager seagleManager;

    @EJB
    ProjectMetricFacade projectMetricFacade;

    @Resource
    ManagedExecutorService service;

    @Deployment
    public static JavaArchive deploy() {
        return AbstractSeagleTest.deployment();
    }

    @Before
    public void beforeTest() throws InterruptedException, ExecutionException {
        cleanUp();
        wakeUpExecutor();
    }

    private void cleanUp() {
        projectManager.delete(purl1);
        assertTrue(!projectManager.exists(purl1));
        Project project = projectManager.findByUrl(purl1);
        assertNull(project);
    }

    public void wakeUpExecutor() throws InterruptedException, ExecutionException {
      // This should be called as a workaround
        // to wake up the executor to execute the Consumer threads of
        // task scheduler!!! (shit this is arquillian & GF))

        service.submit(new Runnable() {

            @Override
            public void run() {
                try {
                    logger.log(Level.INFO, "waking up thread pool");
                    Thread.sleep(10);
                    logger.log(Level.INFO, "thread pool woke up");
                } catch (InterruptedException e) {
                    logger.log(Level.SEVERE,
                            "Could not wake up executor");
                }
            }
        }).get();
    }

    @Test
    public void testExecutionOfCommitCounter() throws VCSRepositoryException,
            InterruptedException {
        Collection<Metric> previousMetrics = metricManager.getMetrics();

        // download a project
        projectManager.clone(purl1);

        try {
            // Remove all metrics from project
            Project project = projectManager.findByUrl(purl1);
            assertNotNull(project);
            metricManager.removeAllMetricsFromProject(project);

            // Register only commit counter metric
            String mnemonic = RepoMetricEnum.COM_PROJ.getMnemonic();
            Metric metric = metricManager.getMetricByMnemonic(mnemonic);
            assertNotNull(metric);
            metricManager.registerMetricToProject(project, metric);

            // Register the listeners
            TestAnalysisRequestListener aListener = new TestAnalysisRequestListener(
                    seagleManager, eventManager);
            TestMetricListener mListener = new TestMetricListener(seagleManager,
                    eventManager);
            mListener.register();
            aListener.register();

            // Now trigger the fake event
            trigerArtificialEvent();

            // Now test that event was triggered
            assertEquals(1, aListener.requests.size());
            AnalysisRequestInfo rinfo = aListener.requests.iterator().next();
            assertEquals(purl1, rinfo.getRemoteProjectUrl());

            // Test that metric START was received
            assertEquals(1, mListener.starts.size());
            MetricRunInfo msinfo = mListener.starts.iterator().next();

            // Test that metric start was sent only to commit counter
            assertEquals(1, msinfo.mnemonics().size());
            String mm = msinfo.mnemonics().iterator().next();
            assertEquals(mnemonic, mm);

         // Now wait until metric finishes its work
            // Metric is running in a thread
            while (aListener.completions.isEmpty()) {
                Thread.sleep(10);
            }
         // At this point metric has finished its work
            // now we should check the completion
            assertEquals(1, aListener.completions.size());
            AnalysisRequestInfo cinfo = aListener.completions.iterator().next();
            assertEquals(purl1, rinfo.getRemoteProjectUrl());
            // Request info should be the same as completion info
            assertEquals(rinfo, cinfo);

         // Now we should check that we received metric end for commit counter
            // Test that metric END was received
            assertEquals(1, mListener.ends.size());
            MetricRunInfo meinfo = mListener.ends.iterator().next();
            assertEquals(1, meinfo.mnemonics().size());
            mm = meinfo.mnemonics().iterator().next();
            assertEquals(mnemonic, mm);

            // Now we should check the db for computed metric
            List<ProjectMetric> pMetrics = projectMetricFacade
                    .findByProjectAndMetric(project, metric);
         // There should be only one metric of this type because
            // commit counter will update this metric if this is in db
            assertEquals(1, pMetrics.size());
            ProjectMetric pm = pMetrics.get(0);
            logger.log(Level.INFO, "counted commits {1} for {0}"
                    + purl1, pm.getMetricValue().getValue());

         // Now we should check the db for metric properties
            // they should be ok
            testBranchPropertiesOK();

        } finally {
            Project project = projectManager.findByUrl(purl1);
            assertNotNull(project);
            metricManager.registerMetricToProject(project, previousMetrics);
            cleanUp();
        }
    }

    private void testBranchPropertiesOK() {
        // Get all default branch properties
        Collection<Property> bProperties = ProjectBranchPropertyUpdater
                .getBranchProperties(properties, purl1);
      // For each branch property we should check the db if
        // it has a branch property for our metric
        String domain = purl1;
        for (Property bProperty : bProperties) {
            String bName = bProperty.getName();
            // Branch property name is like 'BRANCH.bid'
            String name = RepoMetricEnum.COM_PROJ.getPrefixedPropertyName(
                    bName);

            String value = bProperty.getValue();
            // Find the metric branch property here
            Property mbProperty = properties.getPropertyByName(domain, name);
            assertNotNull(mbProperty);
            assertEquals(value, mbProperty.getValue());
        }
    }

    private void trigerArtificialEvent() throws VCSRepositoryException {
      // Now we should create an artificial event
        // Start first the list of commit versions
        VCSRepository repo = projectManager.getRepository(purl1);
        ConnectedVersionProvider versionProvider = selectVersionProviderOfTheBranchWithMaxNumberOfTags(repo);
        try {
            for (VCSCommit c : versionProvider) {
                projectManager.checkout(repo, c);
            }
        } finally {
            if (repo != null) {
                repo.close();
            }
        }

        AnalysisRequestInfo info
                = new AnalysisRequestInfo("elvis@ligu.com", purl1);
        Event event = new DefaultEvent(
                AnalysisEventType.ANALYSIS_REQUESTED,
                new EventInfo(info, new Date()));
        eventManager.trigger(event);
    }

   public ConnectedVersionProvider selectVersionProviderOfTheBranchWithMaxNumberOfTags(VCSRepository repo) throws VCSRepositoryException {
        Collection<VCSBranch> branches = repo.getBranches();
        ConnectedVersionProvider maxVersionProvider = null;
        int maxVersionCount = 0;

        ContextManager cm = seagleManager.getManager(ContextManager.class);

        ModuleContext context = cm.lookupContext(repo, ModuleContext.class);
        ConnectedVersionProvider versionProvider = context.load(ConnectedVersionProvider.class);

        for (VCSBranch branch : branches) {
            TagProvider tagProvider = new BranchTagProvider(repo, branch.getName());

            int currentVersionCount = versionProvider.getCommits().size();

            if (currentVersionCount > maxVersionCount) {
                maxVersionProvider = versionProvider;
                maxVersionCount = currentVersionCount;
            }
        }
        return maxVersionProvider;
    }
}
