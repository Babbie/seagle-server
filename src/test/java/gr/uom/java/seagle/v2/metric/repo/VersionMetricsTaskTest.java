/**
 * 
 */
package gr.uom.java.seagle.v2.metric.repo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import gr.uom.java.seagle.v2.AbstractSeagleTest;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisEventType;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.MetricValue;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.ProjectVersionMetric;
import gr.uom.java.seagle.v2.db.persistence.Version;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectMetricFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectVersionMetricFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.PropertyFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.VersionFacade;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.metric.MetricManager;
import gr.uom.java.seagle.v2.metric.MetricRunInfo;
import gr.uom.java.seagle.v2.metric.imp.AbstractMetricHandler.MetricSuffixProperty;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;
import gr.uom.java.seagle.v2.project.ProjectContext;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.se.util.context.ContextManager;
import gr.uom.se.util.event.DefaultEvent;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.analysis.version.provider.ConnectedVersionProvider;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Elvis Ligu
 */
@TransactionManagement(TransactionManagementType.BEAN)
public class VersionMetricsTaskTest extends AbstractSeagleTest {

   public static final String purl1 = "https://github.com/fernandezpablo85/scribe-java.git";
   private static final Logger logger = Logger
         .getLogger(VersionMetricsTaskTest.class.getName());

   @EJB
   ProjectManager projectManager;

   @EJB
   MetricManager metricManager;

   @EJB
   EventManager eventManager;

   @EJB
   PropertyFacade properties;

   @EJB
   SeagleManager seagleManager;

   @EJB
   ProjectMetricFacade projectMetricFacade;

   @EJB
   ProjectVersionMetricFacade versionMetricFacade;

   @EJB
   VersionFacade versionFacade;

   @Resource
   ManagedExecutorService service;

   @Resource
   UserTransaction tx;

   @Deployment
   public static JavaArchive deploy() {
      return AbstractSeagleTest.deployment();
   }

   @Before
   public void beforeTest() throws InterruptedException, ExecutionException, NotSupportedException, SystemException, SecurityException, IllegalStateException, RollbackException, HeuristicMixedException, HeuristicRollbackException {
      cleanUp();
      wakeUpExecutor();
      tx.begin();
      projectManager.clone(purl1);
      tx.commit();
   }

   private void cleanUp() {
      projectManager.delete(purl1);
      assertTrue(!projectManager.exists(purl1));
      Project project = projectManager.findByUrl(purl1);
      assertNull(project);
   }

   public void wakeUpExecutor() throws InterruptedException, ExecutionException {
      // This should be called as a workaround
      // to wake up the executor to execute the Consumer threads of
      // task scheduler!!! (shit this is arquillian & GF))

      service.submit(new Runnable() {

         @Override
         public void run() {
            try {
               logger.log(Level.INFO, "waking up thread pool");
               Thread.sleep(10);
               logger.log(Level.INFO, "thread pool woke up");
            } catch (InterruptedException e) {
               logger.log(Level.SEVERE, "Could not wake up executor");
            }
         }
      }).get();
   }

   @Test
   public void testVersionMetricExecutions() throws VCSRepositoryException,
         InterruptedException {
      // Register the listeners
      TestAnalysisRequestListener aListener = new TestAnalysisRequestListener(
            seagleManager, eventManager);
      TestMetricListener mListener = new TestMetricListener(seagleManager,
            eventManager);
      mListener.register();
      aListener.register();

      ContextManager cm = seagleManager.getManager(ContextManager.class);
      assertNotNull(cm);
      try (VCSRepository repo = projectManager.getRepository(purl1)) {
         // Load version provider
         ProjectContext context = cm.lookupContext(repo, ProjectContext.class);
         assertNotNull(context);
         ConnectedVersionProvider versionProvider = context
               .load(ConnectedVersionProvider.class);
         assertNotNull(versionProvider);
         versionProvider.init();

         // Get the mnemonics that should be executed
         List<String> mnemonics = new ArrayList<String>();
         CommitProcessorFactory factory = new CommitProcessorFactory(
               seagleManager, versionProvider, purl1);

         Project project = projectManager.findByUrl(purl1);
         // Remove all metrics from project
         metricManager.removeAllMetricsFromProject(project);
         List<Metric> metrics = new ArrayList<>();
         // Collect metrics to be executed, and register only those to this
         // project, ensuring that only our metrics will be executed
         factory.init();
         for (ExecutableMetric m : factory.getSupportingMetrics()) {
            mnemonics.add(m.getMnemonic());
            Metric metric = metricManager.getMetricByMnemonic(m.getMnemonic());
            assertNotNull(metric);
            metricManager.registerMetricToProject(project, metric);
            metrics.add(metric);
         }
         
         // Start executing
         triggerArtificialEvent();

         // Check that the metrics are executed
         // Check that the request was received
         assertEquals(aListener.requests.size(), 1);
         // Now wait until metric finishes its work
         // Metric is running in a thread, if we are not waiting
         // the metrics to finish we will get an error, because
         // arquillian will try to shutdown the container while
         // threads are running, without waiting for them!!!
         while (aListener.completions.isEmpty()) {
            Thread.sleep(10);
         }
         assertEquals(aListener.completions.size(), 1);
         AnalysisRequestInfo info = aListener.completions.iterator().next();
         assertNotNull(info);

         // Check for each version that a metric has been executed
         for (VCSCommit vcommit : versionProvider) {
            // Check that all metrics are executed
            for (Metric metric : metrics) {
               testMetricExecution(project, metric, vcommit);
            }
         }

         testMetricEvents(mListener, mnemonics);
      }
      // Clean up
      cleanUp();
   }

   private void testMetricEvents(TestMetricListener metricListener,
         Collection<String> mnemonics) {
      Set<String> startMnemonics = new HashSet<String>();
      for (MetricRunInfo info : metricListener.starts) {
         startMnemonics.addAll(info.mnemonics());
      }
      assertTrue(mnemonics.containsAll(startMnemonics));

      Set<String> endMnemonics = new HashSet<String>();
      for (MetricRunInfo info : metricListener.starts) {
         endMnemonics.addAll(info.mnemonics());
      }
      assertTrue(mnemonics.containsAll(endMnemonics));
      assertEquals(startMnemonics, endMnemonics);
   }

   private void triggerArtificialEvent() {
      AnalysisRequestInfo info = new AnalysisRequestInfo("elvis@ligu.com",
            purl1);
      Event event = new DefaultEvent(AnalysisEventType.ANALYSIS_REQUESTED,
            new EventInfo(info, new Date()));
      eventManager.trigger(event);
   }

   private void testMetricExecution(Project project, Metric metric,
         VCSCommit vcommit) {
      assertNotNull(metric);
      versionFacade.findAll();
      List<Version> v = versionFacade.findByCommitID(project, vcommit.getID());
      assertEquals(v.size(), 1);
      Version version = v.get(0);

      List<ProjectVersionMetric> pvms = versionMetricFacade
            .findByVersionAndMetric(version, metric);
      if(pvms.size() == 0) {
         return; // The version is not calculated here!!!
      }
      assertEquals(pvms.size(), 1);
      ProjectVersionMetric versionMetric = pvms.get(0);

      MetricValue value = versionMetric.getMetricValue();
      assertNotNull(value);

      // Now check that metric value execution property has been created
      MetricMetadataChecker checker = MetricMetadataChecker.getInstance();
      assertFalse(checker.areNewVersions(seagleManager, purl1,
            metric.getMnemonic()));

      // Check the last computed date
      Date date = getLastComputed(properties, metric.getMnemonic(), purl1);
      assertNotNull(date);
   }

   static final Date getLastComputed(PropertyFacade properties, String metric,
         String purl) {
      String domain = purl;
      String name = MetricSuffixProperty.COMPUTED.getPropertyName(metric);
      String value = properties.get(domain, name);
      if (value == null) {
         return null;
      }
      try {
         return PropertyFacade.toDate(value);
      } catch (ParseException e) {
         throw new RuntimeException("property " + name + " for domain " + purl
               + " should had a Date string");
      }
   }
}
