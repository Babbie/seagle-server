/**
 * 
 */
package gr.uom.java.seagle.v2.db.persistence.controllers;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import gr.uom.java.seagle.v2.AbstractSeagleTest;
import gr.uom.java.seagle.v2.db.persistence.Property;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;

/**
 * @author Elvis Ligu
 */
public class PropertyFacadeTest extends AbstractSeagleTest {

   @EJB
   PropertyFacade properties;

   /**
    * Create the archive to be deployed in the container.
    *
    * Method used by Arquillian environment.
    *
    * @return
    */
   @Deployment
   public static JavaArchive createArchiveAndDeploy() {
      JavaArchive jar = ShrinkWrap
            .create(JavaArchive.class)
            .addPackage(
                  gr.uom.java.seagle.v2.db.persistence.ActionType.class
                        .getPackage())
            .addPackage(
                  gr.uom.java.seagle.v2.db.persistence.controllers.AbstractFacade.class
                        .getPackage())
            .addAsManifestResource("test-persistence.xml", "persistence.xml")
            .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
      return jar;
   }

   @Test
   public void testCreation() {
      String domain = "?domain?";
      String name = "?name?";
      String strVal = "12";

      Property property = createAndTest(domain, name, strVal);
      // Delete the property
      properties.remove(property);
      // Check for removal
      assertNull(properties.get(domain, name));
   }

   private Property createAndTest(String domain, String name, String strVal) {
      // Create the property
      properties.create(domain, name, strVal);
      // Query and check
      Property property = properties.getPropertyByName(domain, name);
      assertNotNull(property);
      assertEquals(domain, property.getDomain());
      assertEquals(name, property.getName());
      assertEquals(strVal, property.getValue());
      return property;
   }

   @Test
   public void testGetSimple() throws ParseException {
      String domain = "?domain?";
      String name = "?name?";
      String strVal = "[12, 13, 14]";

      Property property = createAndTest(domain, name, strVal);

      // Query the value as string
      assertEquals(strVal, properties.get(domain, name));

      // Query the value as int[]
      int[] expected = new int[] { 12, 13, 14 };
      assertArrayEquals(expected, properties.get(domain, name, int[].class));

      // Get all properties
      List<Property> props = properties.getDomainProperties(domain);
      assertEquals(1, props.size());
      assertEquals(property, props.get(0));

      // Update the value and query it again
      double[][] doubles = new double[][] { { 1.2, 3.4 }, { 2.01 } };
      properties.edit(domain, name, doubles);
      // Now query and test
      double[][] result = properties.get(domain, name, double[][].class);
      assertArrayEquals(doubles, result);

      // Try to store a date as long
      Date date = new Date();
      long millis = date.getTime();
      properties.edit(domain, name, millis);
      // Now query the value
      long exp = properties.get(domain, name, long.class);
      assertEquals(millis, exp);
      assertEquals(date, new Date(exp));

      // Now test the date serialization
      Date[][][] dates = new Date[2][3][1];
      for (int i = 0; i < dates.length; i++) {
         for (int j = 0; j < dates[i].length; j++) {
            for (int k = 0; k < dates[i][j].length; k++) {
               dates[i][j][k] = new Date();
            }
         }
      }
      // Serialize the dates using utility methods
      String serial = PropertyFacade.toString(dates);
      // Make the update
      properties.edit(domain, name, serial);
      // Query and check the value, by getting it as strings
      String[][][] dserial = properties.get(domain, name, String[][][].class);
      // Deserialize string array to date array (max 3D arrays are allowed)
      assertArrayEquals(dates, PropertyFacade.toDates(dserial));

      // Delete the property
      properties.remove(property);
      // Check for removal
      assertNull(properties.get(domain, name));
   }

   @Test
   public void testPropertyDeletionByUpdateNull() {
      String domain = "?domain?";
      String name = "?name?";
      String strVal = "[12, 13, 14]";

      Property property = createAndTest(domain, name, strVal);

      properties.edit(domain, name, null);
      // Check for removal
      assertNull(properties.get(domain, name));
      assertFalse(properties.contains(property));

      // Delete the property
      properties.remove(property);
      // Check for removal
      assertNull(properties.get(domain, name));
   }

   @Test
   public void testGetByPrefix() {
      String domain = "?domain?";
      String prefix = "prop.";
      String after = "ks$%@&& ^$";
      String name = prefix + after;
      String strVal = "[12, 13, 14]";

      Property property = createAndTest(domain, name, strVal);

      // Get by prefix and check
      List<Property> props = properties.getDomainByPrefix(domain, prefix);
      assertEquals(1, props.size());
      property = props.get(0);
      assertEquals(name, property.getName());
      assertEquals(domain, property.getDomain());
      assertEquals(strVal, property.getValue());

      String pName = property.getName();
      String pAfter = pName.substring(prefix.length());
      assertEquals(after, pAfter);

      // Get by prefix again but now specify as prefix the whole name
      props = properties.getDomainByPrefix(domain, name);
      property = props.get(0);
      assertEquals(name, property.getName());
      assertEquals(domain, property.getDomain());
      assertEquals(strVal, property.getValue());

      pName = property.getName();
      pAfter = pName.substring(prefix.length());
      assertEquals(after, pAfter);

      // Delete the property
      properties.remove(property);
      // Check for removal
      assertNull(properties.get(domain, name));
   }
}
