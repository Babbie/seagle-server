package gr.uom.java.seagle.v2.event;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import gr.uom.java.seagle.v2.AbstractSeagleTest;
import gr.uom.se.util.event.DefaultEvent;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.event.EventQueue;
import gr.uom.se.util.event.EventType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author teo
 */
public class EventManagerTest extends AbstractSeagleTest {

   @EJB
   EventManager eventManager;

   @Deployment
   public static JavaArchive deploy() {
       return AbstractSeagleTest.deployment();
   }

   private static enum Type implements EventType {
      E1, E2;

      public Event create(Info info) {
         return new DefaultEvent(this, new EventInfo(info, new Date()));
      }
   }

   private static class Info {
      @SuppressWarnings("unused")
      final String msg;

      public Info(String msg) {
         this.msg = msg;
      }
   }

   private static class Listener implements gr.uom.se.util.event.EventListener {
      List<Event> received = new ArrayList<>();

      @Override
      public void respondToEvent(Event event) {
         received.add(event);
      }
   }

   public EventManagerTest() {
   }

   @Test
   public void testInit() throws Exception {
      EventQueue eventQueue = eventManager.getEventQueue();
      Assert.assertNotNull(eventQueue);
   }

   @Test
   public void testSendReceiveEvent() {

      Info i1 = new Info("hello");
      Info i2 = new Info("world");
      Event e1 = Type.E1.create(i1);
      Event e2 = Type.E2.create(i2);

      Listener listener = new Listener();
      eventManager.addListener(listener);

      eventManager.trigger(e1);
      eventManager.trigger(e2);

      assertEquals(2, listener.received.size());
      assertTrue(listener.received.contains(e1));
      assertTrue(listener.received.contains(e2));

      listener.received.clear();
      eventManager.removeListener(listener);

      eventManager.trigger(e1);
      assertTrue(listener.received.isEmpty());

      eventManager.addListener(Type.E1, listener);

      eventManager.trigger(e2);
      assertTrue(listener.received.isEmpty());
      eventManager.trigger(e1);
      assertEquals(1, listener.received.size());
      assertTrue(listener.received.contains(e1));
   }
}
