package gr.uom.java.seagle.v2;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;

/**
 *
 * @author Theodore Chaikalis
 */
public class ArquillianArchive {

    public static JavaArchive get() {
            
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addPackage(gr.uom.java.ast.ASTInformation.class.getPackage())
                .addPackage(gr.uom.java.ast.util.ExpressionExtractor.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.analysis.AnalysisEventType.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.analysis.distributions.StatUtils.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.analysis.graph.AbstractEdge.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.analysis.graph.EdgeCreationStrategy.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.analysis.graph.evolution.AbstractGraphMetricTest.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.analysis.graph.javaGraph.JavaASTFileReader.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.analysis.metrics.aggregation.MetricAggregationStrategy.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.analysis.project.evolution.SourceCodeEvolutionAnalysis.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.analysis.graph.javaGraph.ASTReader.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.ws.rest.project.AnalysisRequestor.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.SeagleManager.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.db.persistence.ActionType.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.db.persistence.controllers.AbstractFacade.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.event.EventManager.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.metric.Language.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.metric.annotation.Metric.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.metric.repo.RepoMetricEnum.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.policy.FilePolicy.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.project.EventListener.class.getPackage())

                .addPackage(gr.uom.java.seagle.v2.project.nameResolver.RepositoryInfoResolver.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.ws.rest.project.VersionRequestorService.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.scheduler.SchedulerConfig.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.ws.rs.config.ApplicationConfig.class.getPackage())
                .addAsManifestResource("test-persistence.xml", "persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
        return jar;
    }

}
