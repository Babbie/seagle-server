/**
 * 
 */
package gr.uom.java.seagle.v2.db.persistence.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import gr.uom.java.seagle.v2.db.persistence.Developer;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.MetricCategory;
import gr.uom.java.seagle.v2.db.persistence.MetricValue;
import gr.uom.java.seagle.v2.db.persistence.Node;
import gr.uom.java.seagle.v2.db.persistence.NodeVersionMetric;
import gr.uom.java.seagle.v2.db.persistence.ProgrammingLanguage;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.ProjectInfo;
import gr.uom.java.seagle.v2.db.persistence.ProjectTimeline;
import gr.uom.java.seagle.v2.db.persistence.ProjectVersionMetric;
import gr.uom.java.seagle.v2.db.persistence.ProjectVersionMetricPK;
import gr.uom.java.seagle.v2.db.persistence.Version;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author Elvis Ligu
 */
@RunWith(Arquillian.class)
@UsingDataSet("datasets/check-cascade-samples.xml")
public class CheckCascadeTest {

   @EJB
   private ProjectFacade projectService;

   @EJB
   private VersionFacade versionService;

   @EJB
   private ProjectInfoFacade projectInfoService;

   @EJB
   private NodeFacade nodeService;

   @EJB
   private ProjectVersionMetricFacade projectVersionMetricService;

   @EJB
   private NodeVersionMetricFacade nodeVersionMetricService;

   @EJB
   private MetricCategoryFacade metricCategoryService;

   @EJB
   private MetricFacade metricService;

   @EJB
   private MetricValueFacade metricValueService;

   @EJB
   private ProjectTimelineFacade projectTimelineService;

   @EJB
   private DeveloperFacade developerService;

   @EJB
   private ProgrammingLanguageFacade languageService;

   /**
    * Create the archive to be deployed in the container.
    *
    * Method used by Arquillian environment.
    *
    * @return
    */
   @Deployment
   public static JavaArchive createArchiveAndDeploy() {
      JavaArchive jar = ShrinkWrap
            .create(JavaArchive.class)

            .addPackage(
                  gr.uom.java.seagle.v2.db.persistence.ActionType.class
                        .getPackage())
            .addPackage(
                  gr.uom.java.seagle.v2.db.persistence.controllers.AbstractFacade.class
                        .getPackage())
            .addAsManifestResource("test-persistence.xml", "persistence.xml")
            .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
      return jar;
   }

   @Test
   public void testRemovalOfProjects() {
      removeProjects();
      removeMetricCategories();
   }

   @Test
   public void testRemovalOfVersion() {
      removeProjects();
      removeMetricCategories();
   }

   /**
    * If a project is removed it should remove all other entities related to
    * project.
    */
   private void removeProjects() {
      List<Project> projects = projectService.findAll();
      assertFalse(projects.isEmpty());
      for (Project project : projects) {
         projectService.remove(project);
         assertProjectRemoved(project);
      }
   }

   /**
    * If a category is removed it should remove all other entities related to
    * this, such as metrics, metric values and so on.
    */
   private void removeMetricCategories() {
      List<MetricCategory> categories = metricCategoryService.findAll();
      assertFalse(categories.isEmpty());
      for (MetricCategory category : categories) {
         metricCategoryService.remove(category);
         assertMetricCategoryRemoved(category);
      }
   }


   /**
    * Assert that the project is removed.
    * 
    * @param project
    */
   private void assertProjectRemoved(Project project) {
      // The project entity should not be in the db
      assertNotNull(project);
      Long id = project.getId();
      assertEntityRemoved(id, projectService);

      // The project info related to the given project
      // should not be in the db
      assertProjectInfoRemoved(project.getProjectInfo());

      // The project timelines related to the given project
      // should not be in the db

      // Assert that all project timelines of the given project
      // are removed (from entity)
      Collection<ProjectTimeline> timelines = project.getTimeLines();
      for (ProjectTimeline t : timelines) {
         assertTimelineRemoved(t);
      }

      // Assert that all project timelines of the given project
      // are removed (from manager)
      timelines = projectTimelineService.findByProjectURL(project
            .getRemoteRepoPath());
      for (ProjectTimeline t : timelines) {
         assertTimelineRemoved(t);
      }

      // Assert that all owners was removed (not from DB).
      assertProjectOwnersRemoved(project);

      // Assert that project languages are removed (not from DB).
      assertProjectLanguageRemoved(project);

      // Assert that project metrics are removed
      assertProjectMetricsRemoved(project);

      // Assert that all versions of the given project
      // are removed (from entity)
      Collection<Version> versions = project.getVersions();
      for (Version v : versions) {
         assertVersionRemoved(v);
      }

      // Assert that all versions of the given project
      // are removed (from manager)
      versions = versionService.findByProject(project);
      for (Version v : versions) {
         assertVersionRemoved(v);
      }
   }

   /**
    * Assert that the given project info was removed.
    */
   private void assertProjectInfoRemoved(ProjectInfo info) {
      assertNotNull(info);
      Long id = info.getId();
      assertEntityRemoved(id, projectInfoService);
   }

   /**
    * Assert that project timeline was removed.
    * 
    * @param timeline
    */
   private void assertTimelineRemoved(ProjectTimeline timeline) {
      assertNotNull(timeline);
      Long id = timeline.getId();
      assertEntityRemoved(id, projectTimelineService);
   }

   /**
    * Assert that the version is removed. When a version is removed all entities
    * related to it must be removed to. For example version nodes must be
    * removed, project version metrics, and node version metrics must be removed
    * too.
    * 
    * @param version
    */
   private void assertVersionRemoved(Version version) {
      Long id = version.getId();
      assertEntityRemoved(id, versionService);

      // Assert that all project version metrics
      // are removed
      assertProjectVersionMetricsRemoved(version);

      // Assert that all node version metrics
      // are removed
      assertNodeVersionMetricsRemoved(version);
   }

   /**
    * Assert that all metrics computed for the given version will be removed.
    * 
    * @param version
    */
   private void assertProjectVersionMetricsRemoved(Version version) {
      assertNotNull(version);
      Collection<ProjectVersionMetric> metrics = projectVersionMetricService
            .findByVersion(version);
      assertTrue(metrics.isEmpty());

      for (ProjectVersionMetric pvm : version.getProjectVersionMetrics()) {
         MetricValue value = pvm.getMetricValue();
         assertMetricValueRemoved(value);
         
      }
   }

   /**
    * Assert that all node version metrics are removed.
    * 
    * @param version
    */
   private void assertNodeVersionMetricsRemoved(Version version) {
      assertNotNull(version);
      Collection<NodeVersionMetric> metrics = nodeVersionMetricService
            .findByVersion(version);
      assertTrue(metrics.isEmpty());

      for (NodeVersionMetric nvm : version.getNodeVersionMetrics()) {
         MetricValue value = nvm.getMetricValue();
         assertMetricValueRemoved(value);
      }
   }

   private void assertEntityRemoved(Object id, AbstractFacade<?> service) {
      assertNotNull(id);
      assertNull(service.findById(id));
   }

   /**
    * Assert that this project owners are removed.
    * 
    * @param dev
    */
   private void assertProjectOwnersRemoved(Project project) {
      assertNotNull(project);
      Collection<Developer> owners = developerService
            .findProjectOwnersByUrl(project.getRemoteRepoPath());
      assertTrue(owners.isEmpty());
   }

   /**
    * Assert that this project languages are removed from this project.
    */
   private void assertProjectLanguageRemoved(Project project) {
      assertNotNull(project);
      Collection<ProgrammingLanguage> langs = languageService
            .findByProject(project);
      assertTrue(langs.isEmpty());
   }

   /**
    * Assert that all registered metrics of the given project are removed from
    * this project.
    * 
    * @param projec
    */
   private void assertProjectMetricsRemoved(Project project) {
      assertNotNull(project);
      Collection<Metric> metrics = metricService.getRegisteredMetrics(project);
      assertTrue(metrics.isEmpty());
   }

   /**
    * Assert that metric category is removed, and all entities related to this.
    * 
    * @param category
    */
   private void assertMetricCategoryRemoved(MetricCategory category) {
      assertNotNull(category);
      Long id = category.getId();
      assertEntityRemoved(id, metricCategoryService);

      // Assert that all metrics are removed
      Collection<Metric> metrics = category.getMetrics();
      for (Metric metric : metrics) {
         assertMetricRemoved(metric);
      }

      // Assert that all metrics are removed
      metrics = metricService.findByCategory(category);
      for (Metric metric : metrics) {
         assertMetricRemoved(metric);
      }
   }

   /**
    * Assert that this metric is removed and all its relatd entities.
    * 
    * @param metric
    */
   private void assertMetricRemoved(Metric metric) {
      assertNotNull(metric);
      Long id = metric.getId();
      assertEntityRemoved(id, metricService);

      // Assert that all values are removed
      assertMetricValuesForMetricRemoved(metric);

      // Assert that all languages that apply to this
      // metric are removed from this metric
      assertLanguageAppliedForMetricRemoved(metric);

   }

   /**
    * Assert that all values for a given metric are removed.
    * 
    * @param metric
    */
   private void assertMetricValuesForMetricRemoved(Metric metric) {
      assertNotNull(metric);
      Collection<MetricValue> values = metricValueService.findByMetric(metric);
      assertTrue(values.isEmpty());
   }

   /**
    * Assert that all languages are removed from the given metric.
    * 
    * @param metric
    */
   private void assertLanguageAppliedForMetricRemoved(Metric metric) {
      assertNotNull(metric);
      Collection<ProgrammingLanguage> langs = metricService.getLanguages(metric
            .getMnemonic());
      assertTrue(langs.isEmpty());
   }

   /**
    * Assert that metric value is removed.
    * 
    * @param value
    */
   private void assertMetricValueRemoved(MetricValue value) {
      assertNotNull(value);
      Long id = value.getId();
      assertEntityRemoved(id, metricValueService);
   }

   /**
    * Assert that node is removed.
    * 
    * @param node
    */
   @SuppressWarnings("unused")
   private void assertNodeRemoved(Node node) {
      assertNotNull(node);
      Long id = node.getId();
      assertEntityRemoved(id, nodeService);
   }
}
