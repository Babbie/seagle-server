package gr.uom.java.seagle.v2.db.persistence.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import gr.uom.java.seagle.v2.db.persistence.Project;

import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ShouldMatchDataSet;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Test case for {@link ProjectFacade} class.
 * <p>
 * Tests are ran within a container using Arquillian. The type of the
 * environment (container, DBMS) is specified under arquillian.xml configuration
 * file. A sample dataset managed by Arquillian (DBUnit) is provided, and the db
 * is cleaned and populated again after each run.
 * 
 * @author Elvis Ligu
 */
@RunWith(Arquillian.class)
@UsingDataSet("datasets/project-facade-samples.xml")
public class ProjectFacadeTest {

   /**
    * Contains the number of projects within DB.
    * <p>
    */
   public static final int NUMBER_OF_PROJECTS_IN_DATASET = 3;
   /**
    * A list with the ids of projects contained in DB.
    * <p>
    */
   public static final Integer[] PROJECT_IDS = new Integer[] { 10, 20, 30 };

   @Inject
   private ProjectFacade projectService;

   /**
    * Create the archive to be deployed in the container.
    *
    * Method used by Arquillian environment.
    *
    * @return
    */
   @Deployment
   public static JavaArchive createArchiveAndDeploy() {
       JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                
                .addPackage(gr.uom.java.seagle.v2.db.persistence.ActionType.class.getPackage())
                .addPackage(gr.uom.java.seagle.v2.db.persistence.controllers.AbstractFacade.class.getPackage())
                .addAsManifestResource("test-persistence.xml", "persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
        return jar;
   }

   /**
    * Test the {@link ProjectFacade#count()} method.
    * <p>
    * The number of returned query should be the same as the number of the
    * projects in DBUnit sample.
    */
   @Test
   public void testCount() {
      assertEquals(projectService.count(), NUMBER_OF_PROJECTS_IN_DATASET);
   }

   /**
    * Test the {@link ProjectFacade#findAll()} method.
    * <p>
    * There should be as many projects as indicated by
    * {@link #NUMBER_OF_PROJECTS_IN_DATASET} property. Each returned project's
    * {@code id} will be checked if it is contained within {@link #PROJECT_IDS}
    * list.
    */
   @Test
   public void testFindAll() {

      List<Project> projects = projectService.findAll();

      // Check the number of projects
      assertEquals(projects.size(), NUMBER_OF_PROJECTS_IN_DATASET);

      // Check for each retrieved project to be in project ids array
      for (Project p : projects) {
         assertTrue(Arrays.asList(PROJECT_IDS).contains(p.getId()));
      }
   }

   /**
    * Test the {@link ProjectFacade#findRange(int[])} method.
    * <p>
    * For simplifying testing, the range is from 0 to 0 and the query will
    * return only one project.
    */
   @Test
   public void testFindRange() {

      // There should be two projects in this range
      List<Project> projects = projectService.findRange(new int[] { 0, 0 });

      // Check the number of projects
      assertEquals(projects.size(), 1);
   }

   /**
    * Test the {@link ProjectFacade#findById(java.lang.Object)} method.
    * <p>
    * There will be as many queries as the {@code ids} contained within
    * {@link #PROJECT_IDS} list.
    */
   @Test
   public void testFindById() {
      for (Integer id : PROJECT_IDS) {
         Project pr = projectService.findById(id);
         assertNotNull(pr);
         assertEquals(id, pr.getId());
      }
   }

   /**
    * Test the {@link ProjectFacade#findByName(java.lang.String)} method.
    * <p>
    * Only one project should be returned (if any) with a given name, because
    * the name should be unique within projects table.
    * <p>
    * <b>There is a strange problem running this test. In some situations this
    * test may fail, probably because the DBMS doesn't guarantee the isolation
    * level. The reason is that, in another test within this class we are
    * editing the same project that we are querying here, and the returned
    * project doesn't have the same name as the one we used to query for. Mostly
    * the problem occurs where there is another test failure/exception when
    * running the tests.</b>
    */
   @Test
   public void findByName() {
      String name = "Java Project";
      List<Project> projects = projectService.findByName(name);
      // There must be only one project with the given name,
      // the name is unique for a project.
      assertEquals(1, projects.size());

      // WARNING: the assertion here may not pass,
      // because of the concurrent changes made to project Java Project.
      // The behaviour is unspecified and yet to be resolved!!!
      Project project = projects.get(0);
      assertEquals(name, project.getName());
   }

   /**
    * Test the {@link ProjectFacade#create(java.lang.Object)} method.
    * <p>
    * After the inserting of a new test project the database contents will be
    * tested against a sample xml by DBUnit which reflects the newly created
    * test.
    */
   @Test
   @ShouldMatchDataSet(value = "project-facade-create.xml", excludeColumns = { "id" })
   public void testCreate() {

      // Create a sample project
      Project vbProject = new Project();
      vbProject.setName("VB Project");
      vbProject.setDescription("A test Visual Basic project");
      vbProject.setRemoteRepoPath("http://example.com/vbproject.git");
      vbProject.setImagePath("path/to/vbproject/image");

      // Persist the project
      projectService.create(vbProject);
      // Flush the changes so the newly created project
      // will retrieve a value to its id
      projectService.flushChanges();
      // Check if entity was created
      assertTrue(projectService.contains(vbProject));

      // The id should not be null
      assertNotNull(vbProject.getId());

      // Find the project by its id and check if the returned is the same
      // as the newly created
      Project expected = projectService.findById(vbProject.getId());
      assertEquals(vbProject, expected);
   }

   /**
    * Test the {@link ProjectFacade#remove(java.lang.Object)} method.
    * <p>
    * After the delete the project should not be available in db for query.
    */
   @Test
   public void testRemove() {
      Integer id = 10;
      Project foundProject = projectService.findById(id);
      assertNotNull(foundProject);

      projectService.remove(foundProject);
      assertNull(projectService.findById(id));
      assertFalse(projectService.contains(foundProject));
   }

   /**
    * Test the {@link ProjectFacade#edit(java.lang.Object)} method.
    * <p>
    */
   @Test
   public void testEdit() {
      Integer id = 10;
      String newName = "Java Project (Edited)";
      Project foundProject = projectService.findById(id);
      assertNotNull(foundProject);

      String oldName = foundProject.getName();

      foundProject.setName(newName);
      projectService.edit(foundProject);

      foundProject = projectService.findById(id);
      assertNotNull(foundProject);
      assertEquals(newName, foundProject.getName());

      // Return the project to its last state
      foundProject.setName(oldName);
      projectService.edit(foundProject);
   }

   /**
    * Integrity check for the project's name NOT NULL constraint. Also this
    * method check for the uniqueness of the project name.
    * <p>
    * Null project names are not allowed. Project name should be unique.
    */
   @Test
   public void testNullNames() {
      // 1 - When a project is persisted the
      // name must not be null
      Project project = new Project();
      project.setRemoteRepoPath("http://example.com/project.git");
      boolean thrown = false;
      try {
         projectService.create(project);
      } catch (Exception pe) {
         thrown = true;
      }
      // An exception must be thrown when attempt to
      // create a project with null name
      assertTrue(thrown);

      // 2 - The name should be unique.
      thrown = false;
      project.setName("Java Project");
      try {
         projectService.create(project);
      } catch (Exception pe) {
         thrown = true;
      }
      // An exception must be thrown when attempt to
      // create a project with a name already contained in db
      assertTrue(thrown);

      // 3 - Setting the name of a project to null is not allowed
      project.setName(null);
      thrown = false;
      try {
         projectService.edit(project);
      } catch (Exception ep) {
         thrown = true;
      }
      assertTrue(thrown);

      // 4 - Setting the name of a project to other project's
      // name is not allowed
      project.setName("Java Project");
      thrown = false;
      try {
         projectService.edit(project);
      } catch (Exception ep) {
         thrown = true;
      }
      assertTrue(thrown);
   }

   /**
    * Test the {@link ProjectFacade#findCreatedBefore(java.util.Date)} method.
    * <p>
    */
   @Test
   public void testFindCreatedBefore() {
      // Test for projects to found
      Date date = new GregorianCalendar(2005, 9, 12).getTime();
      List<Project> projects = projectService.findCreatedBefore(date);
      assertFalse(projects.isEmpty());

      for (Project p : projects) {
         assertTrue(p.getProjectInfo().getDateCreated().before(date));
      }

      // Test for an empty result
      date = new GregorianCalendar(1900, 0, 1).getTime();
      projects = projectService.findCreatedBefore(date);
      assertTrue(projects.isEmpty());
   }

   /**
    * Test the {@link ProjectFacade#findCreatedAfter(java.util.Date)} method.
    * <p>
    */
   @Test
   public void testFindCreatedAfter() {
      // Test for projects to found
      Date date = new GregorianCalendar(2005, 9, 12).getTime();
      List<Project> projects = projectService.findCreatedAfter(date);
      assertFalse(projects.isEmpty());

      for (Project p : projects) {
         assertTrue(p.getProjectInfo().getDateCreated().after(date));
      }

      // Test for an empty result
      date = new GregorianCalendar(2020, 0, 1).getTime();
      projects = projectService.findCreatedAfter(date);
      assertTrue(projects.isEmpty());
   }

   /**
    * Test the {@link ProjectFacade#findInsertedBefore(java.util.Date)} method.
    * <p>
    */
   @Test
   public void testFindInsertedBefore() {
      // Test for projects to found
      Date date = new GregorianCalendar(2014, 0, 14, 3, 0).getTime();
      List<Project> projects = projectService.findInsertedBefore(date);
      assertFalse(projects.isEmpty());

      for (Project p : projects) {
         assertTrue(p.getProjectInfo().getDateInserted().before(date));
      }

      // Test for an empty result
      date = new GregorianCalendar(1900, 0, 1).getTime();
      projects = projectService.findInsertedBefore(date);
      assertTrue(projects.isEmpty());
   }

   /**
    * Test the {@link ProjectFacade#findInsertedAfter(java.util.Date)} method.
    * <p>
    */
   @Test
   public void testFindInsertedAfter() {
      // Test for projects to found
      Date date = new GregorianCalendar(2014, 0, 14, 3, 0).getTime();
      List<Project> projects = projectService.findInsertedAfter(date);
      assertFalse(projects.isEmpty());

      for (Project p : projects) {
         assertTrue(p.getProjectInfo().getDateInserted().after(date));
      }

      // Test for an empty result
      date = new GregorianCalendar(2020, 0, 1).getTime();
      projects = projectService.findInsertedAfter(date);
      assertTrue(projects.isEmpty());
   }

   /**
    * Test the {@link ProjectFacade#findUpdatedBefore(java.util.Date)} method.
    * <p>
    */
   @Test
   public void testFindUpdatedBefore() {
      // Test for projects to found
      Date date = new GregorianCalendar(2014, 10, 3, 9, 10).getTime();
      List<Project> projects = projectService.findUpdatedBefore(date);
      assertFalse(projects.isEmpty());

      for (Project p : projects) {
         assertTrue(p.getProjectInfo().getLastUpdate().before(date));
      }

      // Test for an empty result
      date = new GregorianCalendar(1900, 0, 1).getTime();
      projects = projectService.findUpdatedBefore(date);
      assertTrue(projects.isEmpty());
   }

   /**
    * Test the {@link ProjectFacade#findUpdatedAfter(java.util.Date)} method.
    * <p>
    */
   @Test
   public void testFindUpdatedAfter() {
      // Test for projects to found
      Date date = new GregorianCalendar(2014, 10, 3, 9, 10).getTime();
      List<Project> projects = projectService.findUpdatedAfter(date);
      assertFalse(projects.isEmpty());

      for (Project p : projects) {
         assertTrue(p.getProjectInfo().getLastUpdate().after(date));
      }

      // Test for an empty result
      date = new GregorianCalendar(2020, 0, 1).getTime();
      projects = projectService.findUpdatedAfter(date);
      assertTrue(projects.isEmpty());
   }

   /**
    * Test the
    * {@link ProjectFacade#findCreatedWithin(java.util.Date, java.util.Date)
     * }
    * method.
    * <p>
    */
   @Test
   public void testFindCreatedWithin() {
      // Test for projects to found
      Date start = new GregorianCalendar(2001, 0, 1).getTime();
      Date end = new GregorianCalendar(2005, 11, 31).getTime();

      List<Project> projects = projectService.findCreatedWithin(start, end);
      assertFalse(projects.isEmpty());

      for (Project p : projects) {
         Date date = p.getProjectInfo().getDateCreated();
         assertTrue(date.after(start) && date.before(end));
      }

      // Test for empty result
      start = new GregorianCalendar(2000, 0, 1).getTime();
      end = new GregorianCalendar(2001, 0, 1).getTime();

      projects = projectService.findCreatedWithin(start, end);
      assertTrue(projects.isEmpty());
   }

   /**
    * Test the
    * {@link ProjectFacade#findUpdatedWithin(java.util.Date, java.util.Date)
     * }
    * method.
    * <p>
    */
   @Test
   public void testFindUpdatedWithin() {
      // Test for projects to found
      Date start = new GregorianCalendar(2014, 10, 3, 4, 0).getTime();
      Date end = new GregorianCalendar(2014, 10, 3, 19, 0).getTime();

      List<Project> projects = projectService.findUpdatedWithin(start, end);
      assertFalse(projects.isEmpty());

      for (Project p : projects) {
         Date date = p.getProjectInfo().getLastUpdate();
         assertTrue(date.after(start) && date.before(end));
      }

      // Test for empty result
      start = new GregorianCalendar(2000, 0, 1).getTime();
      end = new GregorianCalendar(2001, 0, 1).getTime();

      projects = projectService.findUpdatedWithin(start, end);
      assertTrue(projects.isEmpty());
   }

   /**
    * Test the
    * {@link ProjectFacade#findInsertedWithin(java.util.Date, java.util.Date)
     * }
    * method.
    * <p>
    */
   @Test
   public void testFindInsertedWithin() {
      // Test for projects to found
      Date start = new GregorianCalendar(2014, 0, 14, 2, 0).getTime();
      Date end = new GregorianCalendar(2014, 0, 14, 3, 0).getTime();

      List<Project> projects = projectService.findInsertedWithin(start, end);
      assertFalse(projects.isEmpty());

      for (Project p : projects) {
         Date date = p.getProjectInfo().getDateInserted();
         assertTrue(date.after(start) && date.before(end));
      }

      // Test for empty result
      start = new GregorianCalendar(2000, 0, 1).getTime();
      end = new GregorianCalendar(2001, 0, 1).getTime();

      projects = projectService.findInsertedWithin(start, end);
      assertTrue(projects.isEmpty());
   }
}
