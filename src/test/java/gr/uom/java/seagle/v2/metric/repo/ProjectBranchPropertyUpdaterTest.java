/**
 * 
 */
package gr.uom.java.seagle.v2.metric.repo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import gr.uom.java.seagle.v2.AbstractSeagleTest;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.Property;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.PropertyFacade;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.se.vcs.VCSBranch;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Elvis Ligu
 */
public class ProjectBranchPropertyUpdaterTest extends AbstractSeagleTest {

   public static final String purl1 = "https://github.com/fernandezpablo85/scribe-java.git";
   private static final Logger logger = Logger.getLogger(ProjectBranchPropertyUpdaterTest.class.getName());
   
   @EJB
   SeagleManager seagleManager;

   @EJB
   ProjectManager projectManager;

   @EJB
   ProjectFacade projectFacade;

   @EJB
   PropertyFacade properties;

   @Deployment
   public static JavaArchive deploy() {
      return AbstractSeagleTest.deployment();
   }

   @Before
   public void beforeTest() {
      projectManager.delete(purl1);
      assertTrue(!projectManager.exists(purl1));
      Project project = projectManager.findByUrl(purl1);
      assertNull(project);
   }

   @Test
   public void testBranchPropertiesCreation() throws VCSRepositoryException {
      projectManager.clone(purl1);

      try {
         testProperties();
      } finally {
         projectManager.delete(purl1);
      }
   }

   private void testProperties() throws VCSRepositoryException {
      String domain = purl1;
      long millis = System.currentTimeMillis();
      try (VCSRepository repo = projectManager.getRepository(purl1)) {
         Collection<VCSBranch> branches = repo.getBranches();
         for (VCSBranch branch : branches) {
            String bid = branch.getID();
            String name = ProjectBranchPropertyUpdater
                  .getBranchPropertyName(bid);
            Property bProperty = properties.getPropertyByName(domain, name);
            assertNotNull(bProperty);
            String value = branch.getHead().getID();
            assertEquals(value, bProperty.getValue());
         }
      } finally {
         logger.info("Properties check time (ms): " + (System.currentTimeMillis() - millis));
      }
   }

   @Test
   public void testBranchPropertiesDeleted() {
      projectManager.clone(purl1);
      projectManager.delete(purl1);
      String domain = purl1;
      Collection<Property> bProperties = properties.getDomainProperties(domain);
      assertTrue(bProperties.isEmpty());
   }

   // We can't test the update because the project should be updated
   // however we can initiate an update and check that it doesn't throw
   // an exception
   @Test
   public void testFakeUpdate() throws VCSRepositoryException {
      projectManager.clone(purl1);
      try {
         testProperties();
         projectManager.update(purl1);
         testProperties();
      } finally {
         projectManager.delete(purl1);
      }
   }
}
